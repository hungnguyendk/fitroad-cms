<?php

namespace api\modules\doc\controllers;

use Yii;
use yii\web\Controller;
use yii\web\View as View;


class GuideController extends Controller
{
    public $layout = '//doc';
    
    public function actionIndex()
    {
       
        $view = new \yii\web\View();
        
        $view->title = 'Fit Road Document';
        
        $this->setView($view) ;
        
    
        return $this->render('index');
    }
}

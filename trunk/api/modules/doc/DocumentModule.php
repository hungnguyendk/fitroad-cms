<?php

namespace api\modules\doc;

class DocumentModule extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\doc\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

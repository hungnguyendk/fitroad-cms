<?php 
//use api\assets\DocumentAsset;
use yii\helpers\Html;
use yii\helpers\BaseHtml;
use yii\widgets\ActiveForm;
//DocumentAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html >
<head>
 	<meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo Html::encode($this->title); ?></title>
 	<?php $this->head() ?>
 	 <link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16" />
  <link href='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/css/typography.css' media='screen' rel='stylesheet' type='text/css'/>
  <link href='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/css/reset.css' media='screen' rel='stylesheet' type='text/css'/>
  <link href='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/css/screen.css' media='screen' rel='stylesheet' type='text/css'/>
  <link href='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/css/reset.css' media='print' rel='stylesheet' type='text/css'/>
  <link href='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/css/print.css' media='print' rel='stylesheet' type='text/css'/>
</head>

<body class="swagger-section">
<?php $this->beginBody() ?>

<?php echo $content;?>
 <?php $this->endBody() ?>
 <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/lib/jquery-1.8.0.min.js' type='text/javascript'></script>
  <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/lib/jquery.slideto.min.js' type='text/javascript'></script>
  <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/lib/jquery.wiggle.min.js' type='text/javascript'></script>
  <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/lib/jquery.ba-bbq.min.js' type='text/javascript'></script>
  <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/lib/handlebars-2.0.0.js' type='text/javascript'></script>
  <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/lib/underscore-min.js' type='text/javascript'></script>
  <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/lib/backbone-min.js' type='text/javascript'></script>
  <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/swagger-ui.js' type='text/javascript'></script>
  <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/lib/highlight.7.3.pack.js' type='text/javascript'></script>
  <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/lib/marked.js' type='text/javascript'></script>
  <script src='<?php echo Yii::$app->homeUrl;?>assets/c40826f2/lib/swagger-oauth.js' type='text/javascript'></script>
   <script type="text/javascript">
    $(function () {
      var url = window.location.search.match(/url=([^&]+)/);
      if (url && url.length > 1) {
        url = decodeURIComponent(url[1]);
      } else {
        url = "<?php echo Yii::$app->homeUrl;?>fitroad.json";
      }
      window.swaggerUi = new SwaggerUi({
        url: url,
        dom_id: "swagger-ui-container",
        supportedSubmitMethods: ['get', 'post', 'put', 'delete', 'patch'],
        onComplete: function(swaggerApi, swaggerUi){
          if(typeof initOAuth == "function") {
            initOAuth({
              clientId: "your-client-id",
              realm: "your-realms",
              appName: "your-app-name"
            });
          }

          $('pre code').each(function(i, e) {
            hljs.highlightBlock(e)
          });

          addApiKeyAuthorization();
        },
        onFailure: function(data) {
          log("Unable to Load SwaggerUI");
        },
        docExpansion: "none",
        apisSorter: "alpha",
        showRequestHeaders: false
      });

      function addApiKeyAuthorization(){
        var key = encodeURIComponent($('#input_apiKey')[0].value);
        if(key && key.trim() != "") {
            var apiKeyAuth = new SwaggerClient.ApiKeyAuthorization("api_key", key, "query");
            window.swaggerUi.api.clientAuthorizations.add("api_key", apiKeyAuth);
            log("added key " + key);
        }
      }

      $('#input_apiKey').change(addApiKeyAuthorization);

      // if you have an apiKey you would like to pre-populate on the page for demonstration purposes...
      /*
        var apiKey = "myApiKeyXXXX123456789";
        $('#input_apiKey').val(apiKey);
      */

      window.swaggerUi.load();

      function log() {
        if ('console' in window) {
          console.log.apply(console, arguments);
        }
      }
  });
  </script>
</body>
</html>
<?php $this->endPage() ?>

<?php
return [
    'adminEmail' => 'admin@simple.com',


    'sizeThumbImg'			               =>	200,
    'sizeNormalImg'		                   =>  400,
    'thumbName'                  => 'thumb-',
    'normalName'                 => 'normal-'
];

<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'en_EN',
    'sourceLanguage' => 'en',

    'modules' => [
        'v1' => [
            'basePath' => '@api/versions/v1',
            'class' => 'api\versions\v1\RestModule',
        ],
        'doc' => [
            'class' => 'api\modules\doc\DocumentModule',
        ],
    ],
//    'controllerNamespace' => 'api\versions\v1\controllers',
//     'controllerMap' => [
//             [
//                     'user' => [
//                             'class' => 'api\versions\v1\controllers\UserController',
//                             'enableCsrfValidation' => false,
//                     ],
//             ],
//     ],
    'components' => [
        'assetManager' => [
                'appendTimestamp' => true,
        ],
//        'i18n' => [
//        'translations' => [
//            'app*' => [
//                'class' => 'yii\i18n\PhpMessageSource',
//                //'basePath' => '@app/messages',
//                //'sourceLanguage' => 'en-US',
//                'fileMap' => [
//                    'app' => 'app.php',
//                    'app/error' => 'error.php',
//                    ],
//                ],
//            ],
//        ],
        'urlManager' => [
                'enablePrettyUrl' => true,
                'enableStrictParsing' => true,

                'showScriptName' => false,
                //'suffix' => '.json',
                'rules' => [    
                        'class' => 'yii\rest\UrlRule',  
                     //   'controller' => ['contact/index'],
                        '<module:v1>/<controller:[\w\-]+>' => 'v1/<controller>/index',
                        '<module:v1>/<controller:[\w\-]+>/<action:[\w\-]+>' => 'v1/<controller>/<action>',
                         'doc/guide/index'      => 'doc/guide/index',
                        //'<controller:\w+>/<action:\w+>'         => '<controller>/<action>',
                        
                ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                        
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info','error', 'warning'],
                    'categories' => ['Users'],
                    'logFile' => '@api/runtime/logs/Users/requests'.date("Ymd").'.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 20,
                ],

            ],
        ],
    ],
    'params' => $params,
];

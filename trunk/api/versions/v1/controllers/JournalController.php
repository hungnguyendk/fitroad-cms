<?php
/**
 * User: sangnguyen on  4/16/16 at 08:40
 * File name: JournalController.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace api\versions\v1\controllers;


use api\commons\helpers\ApiHelpers;
use api\commons\models\entities\FitRoadGalleryImage;
use api\commons\models\entities\FitRoadUser;
use api\components\RestController;

class JournalController extends RestController{
    public function actionUpload(){
        $this->requireAuthToken();
        $data = [];
        $uploadFile = $this->uploadAttachmentFile( \Yii::getAlias('@publicImagesActivities'));
        $ids = [];
        if ($uploadFile) {

            if(is_array($uploadFile->output) && !empty($uploadFile->output)){

                foreach($uploadFile->output as $_item){

                    $imageModel = new FitRoadGalleryImage();

                    $galleryParam = [
                        'food_id' => null,
                        'user_id' => $this->user->id,
                        'url' => $_item,
                        'status' => \api\commons\models\entities\FitRoadGalleryImage::STATUS_ACTIVE,
                        'type' => \api\commons\models\entities\FitRoadGalleryImage::IMAGE_TYPE_ACTIVITY,
                        'is_current' => \api\commons\models\entities\FitRoadGalleryImage::IS_CURRENT];

                    $imageModel->attributes = $galleryParam;

                    if (!$imageModel->save()) {
                        $businessMsg = ApiHelpers::getFormMessageError($imageModel);
                        ($businessMsg) ? $this->warning = ['businessError' => ['msg' => $businessMsg, 'action' => 'upload_journal_image']] : null;
                    }else{

                        $ids['id'] =$imageModel->id;

                        $data[] =$ids;
                    }
                }
            }
            $this->sendResponse(['images'=>$data]);
        }
    }

    public function actionGetList(){
        $this->requireAuthToken();
        $data = [];
        $response = [];
        $params['recordPerpage']      = (int) (isset($this->request->pagination->recordPerpage)
            && $this->request->pagination->recordPerpage !=0)?
            $this->request->pagination->recordPerpage:20;

        $params['page']     = (int) (isset($this->request->pagination->currentPage)
            && $this->request->pagination->currentPage !=0)?
            $this->request->pagination->currentPage - 1 :0;

        $date =  isset ($this->request->date) ? $this->request->date :
            $this->sendResponse($this->builtErrorCode(400,
                \Yii::t('api/error','The date can not blank!')));

        $journal = new FitRoadGalleryImage();

        $params['queryAndOnCondition'] = [
            'dateRequest'=>$date,
            'status'=>1,
            'type'=>FitRoadGalleryImage::IMAGE_TYPE_ACTIVITY,
            'user_id'=>$this->user->id
        ];

        $provider = $journal->getList($params);
        $results = $provider->getModels();

        if($results){
            foreach($results as $_item){
                $data[] = $_item->getDataSelf();
            }
        }
        $response['pagination'] = $this->outputObjectPaging($provider);
        $response['journals'] = $data;


        $this->sendResponse($response);
    }
}
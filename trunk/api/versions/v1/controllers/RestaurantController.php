<?php
/**
 * User: sangnguyen on  4/15/16 at 09:09
 * File name: RestaurantController.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace api\versions\v1\controllers;

use api\commons\models\entities\FitRoadRestaurant;
use api\commons\models\entities\FitRoadRestaurantVisitReport;
use api\components\RestController;
use joshtronic\GooglePlaces;
use yii\helpers\ArrayHelper;

Class RestaurantController extends RestController{
    var  $googlePlace =  null;
    var  $nextPageToken = null;
    var $dataResponseGoogle = [];
    public function actionGetLocalList(){

        $models = FitRoadRestaurant::findAll(['status'=>1]);
        $data= [];

        if($models){
            foreach($models as $_item){
                $data[] = $_item->getDataSelf();
            }
        }
        $this->sendResponse($data);
    }

    public function getDataGoogleSearchPlace($lat,$long,$distance,$nextPageToken,$type=['restaurant','food']){
        //$this->googlePlace->location = array(10.7559492, 106.69074769999997);
        $placeIds = [];

        $this->googlePlace = new GooglePlaces('AIzaSyBkasIPKFOEKKUKeKwxmTfCaEg4G6cWYO0');
        $this->googlePlace->sleep = 0;

        $this->googlePlace->location = [$lat, $long];
        $this->googlePlace->radius   = ($distance)?$distance:15000;
        $this->googlePlace->types    = $type; // Requires keyword, name or types

        $this->googlePlace->rankby   = 'distance';

        if($nextPageToken){
            $this->googlePlace->pagetoken = $nextPageToken;
        }

        $page1 = $this->googlePlace->nearbySearch();

        $results    =   json_encode($page1['results']);

        $results = json_decode($results);

        $this->nextPageToken = (isset($page1['next_page_token']))?$page1['next_page_token']:'';

        if( is_array($results) && !empty($results)){
            $i= 0;
            foreach($results as $_item){

                $googleDataRestaurant = [
                    'name'=>$_item->name,
                    'is_local'=>0,
                    'place_id'=>isset($_item->place_id)?$_item->place_id:null,
                    'identifier'=>isset($_item->id)?$_item->id:null,
                    'description'=>null,
                    //                    'phone	'=>isset($details->international_phone_number)?$details->international_phone_number:null,
                    'fax'=>null,
                    //                    'website'=>isset($details->website)?$details->website:null,
                    'address'=>isset($_item->vicinity)?$_item->vicinity:null,
                    'lat'=>isset($_item->geometry->location->lat)?(string)$_item->geometry->location->lat:null,
                    'long'=>isset($_item->geometry->location->lng)?(string)$_item->geometry->location->lng:null,
                    'icon'=>isset($_item->icon)?$_item->icon:null,
                    'type'=>(isset($_item->type) && !empty($_item->type))?json_encode($_item->type):null,
                    'google_data'=>json_encode([
                        'id'=>isset($_item->id)?$_item->id:null,
                        'openingHours'=>(isset($_item->opening_hours) && $_item->opening_hours->open_now)?'Open now':null,
                        'placeId'=>isset($_item->place_id)?$_item->place_id:null,
                        'reference'=>isset($_item->reference)?$_item->reference:null,
                        'scope'=>'Local',
                        'originScope'=>'Google',
                        'priceLevel'=>isset($_item->price_level)?$_item->price_level:null,
                        'photo'=>isset($page1['results'][$i]['photos'])?$page1['results'][$i]['photos']:[],
                        'vicinity'=>isset($_item->vicinity)?$_item->vicinity:null,
                        'formattedAddress'=>isset($_item->formattedAddress)?$_item->photo->formatted_address:null,
                        //'map_url'=>isset($details->url)?$details->url:null,
                        //'utc_offset'=>isset($details->utc_offset)?$details->utc_offset:null,
                        //'formatted_phone_number'=>isset($details->formatted_phone_number)?$details->formatted_phone_number:null,
                        //'international_phone_number'=>isset($details->international_phone_number)?$details->international_phone_number:null,
                        //'address_components'=>isset($details->address_components)?$details->address_components:null,
                        'rating'=>isset($_item->rating)?$_item->rating:null,
                        //'reviews'=>isset($details->reviews)?$details->reviews:null
                    ])
                ];

                $googleRestaurant = [
                    "isVip"=> boolval(0),
                    "createdAt"=>'',
                    "images"=> [],
                    "calories"=>(int)0,
                    'name'=>$_item->name,
                    "description"=>'',
                    "phone"=> '',
                    'address'=>isset($_item->vicinity)?$_item->vicinity:'',
                    "website"=> '',
                    "openTime"=> '',
                    "closeTime"=> '',
                    'lat'=>isset($_item->geometry->location->lat)?(string)$_item->geometry->location->lat:'',
                    'lng'=>isset($_item->geometry->location->lng)?(string)$_item->geometry->location->lng:'',
                    "scope"=> 'Google',
                    "originScope"=>'Google',
                    "id"=>(string)$_item->id,
                    "placeId"=>(string)$_item->place_id,
                    "identifier"=>($_item->id)?(string)$_item->id:'',
                    "openingHours"=>(isset($_item->opening_hours) && $_item->opening_hours->open_now)?'Open now':'',
                    "ggPhotoSettings"=>isset($page1['results'][$i]['photos'])?$page1['results'][$i]['photos']:[],
                    "urlAvatarThumb"=>'',
                    "urlAvatarOrigin"=>'',
                ];

                $this->dataResponseGoogle[] = $googleRestaurant;

                $i++;
                $placeIds[] = isset($_item->place_id)?$_item->place_id:null;

                $restaurant = new FitRoadRestaurant();
                $restaurant->attributes = $googleDataRestaurant;

                try{
                    $restaurant->save(FALSE);
                }catch(\yii\db\Exception $e){
                    $rs = FitRoadRestaurant::findOne(['place_id'=>$_item->place_id]);
                    if($rs){
                        $rs->attributes = $googleDataRestaurant;
                        $rs->save(FALSE);
                    }

                    \Yii::error('Error \'s name: '.$e->getName(), 'Save data from google');
                    \Yii::error('Error \'s message: '.$e->getMessage(), 'Save data from google');
                }
                $i++;
            }
        }
        return $placeIds;
    }

    public function actionGetList(){

        //$this->requireAuthToken();
        $data = [];
        $placeIds =[];
        $response = [];
        $provider = null;

        $lat = isset ($this->request->lat) ? $this->request->lat : null;
        $long = isset ($this->request->long) ? $this->request->long : null;
        $calories = isset ($this->request->calories) ? $this->request->calories : null;
        $distance = isset ($this->request->distance) ? $this->request->distance : null;
        $nextPageToken = isset ($this->request->nextPageToken) ? $this->request->nextPageToken : null;
        $calories = isset ($this->request->calories) ? $this->request->calories : null;

        $params['recordPerpage']      = (int) (isset($this->request->pagination->recordPerpage)
            && $this->request->pagination->recordPerpage !=0)?
            $this->request->pagination->recordPerpage:20;

        $params['page']     = (int) (isset($this->request->pagination->currentPage)
            && $this->request->pagination->currentPage !=0)?
            $this->request->pagination->currentPage - 1 :0;

        if($calories && !empty($calories)){

                $restaurant = new FitRoadRestaurant();
                $params['queryAndOnCondition'] = [
                    'calories'=>$calories,
                    FitRoadRestaurant::tableName().'.status'=>1
                ];

                $provider = $restaurant->getList($params,$lat,$long);
                $results = $provider->getModels();
        }else{
            $placeIds = $this->getDataGoogleSearchPlace($lat,$long,$distance,$nextPageToken);

            $res = new FitRoadRestaurant();

            $localRestaurant = $res->getRestaurantsAroundOnSystem($lat,$long);

            $results = array_merge($this->dataResponseGoogle,$localRestaurant);

        }

        if($results){
            foreach($results as $_item){
                if(is_object($_item)){
                    $data[] = $_item->getDataSelf();
                }else{
                    $data[] = $_item;
                }

            }
        }

        if(!$calories && empty($calories)){
            $dateIndex = ArrayHelper::index($data, 'identifier');

            $data = array_values($dateIndex);
        }

        $response['pagination'] = $this->outputObjectPaging($provider);
        $response['restaurants'] = $data;
        $response['nextPageToken'] = (string)$this->nextPageToken;

       $this->sendResponse($response);

    }

    public function actionClick(){
        $this->requireAuthToken();
        $restaurantId =  isset ($this->request->restaurantId) ? $this->request->restaurantId : null;

        $restaurant = FitRoadRestaurant::findOne(['id'=>(int)$restaurantId,'status'=>1]);

        if(!$restaurant){
            $this->sendResponse($this->builtErrorCode(400,\Yii::t('api/error','Restaurant do not exits!')));
        }

        $visitData = [
            'user_id'=>$this->user->id,
            'restaurant_id'=>$restaurant->id,
            'request_ip'=> \Yii::$app->request->getUserIP(),
            'device_version'=>\Yii::$app->request->userAgent,
        ];

        $visit = new FitRoadRestaurantVisitReport();
        $visit->attributes = $visitData;

        if(!$visit->save()){
            $this->getFormError($visit);
        }

        $this->sendResponse(['restaurant'=>$restaurant->getDataSelf()]);

    }

}
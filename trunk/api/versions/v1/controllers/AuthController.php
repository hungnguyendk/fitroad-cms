<?php
/**
 * User: sangnguyen
 * Date: 9/29/15
 * Time: 14:24
 * File name: UserController.php
 * Project name: Fit Road
 */
namespace api\versions\v1\controllers;

use api\commons\forms\SentTokenRestPasswordForm;
use api\commons\forms\SignUpForm;
use api\commons\helpers\ApiHelpers;
use api\commons\models\entities\FitRoadGalleryImage;
use api\commons\models\entities\FitRoadUser;

use api\commons\models\repositories\FitRoadUserRepository;
use Yii;


Class AuthController extends \api\components\RestController{

    /**
     * @var: User Login
     */
    public function actionVerify(){
       // $this->requireDeviceToken();
        $form = new \api\commons\forms\SignInForm();
        $form->email = isset ($this->request->email) ? $this->request->email : null;
        $form->password = isset ($this->request->password) ? $this->request->password : null;
//        $deviceToken    = isset ($this->request->deviceToken) ? $this->request->deviceToken : null;
//        $deviceType     = isset ($this->request->deviceType) ? $this->request->deviceType : null ;
//        $osType         = isset ($this->request->deviceOs) ? $this->request->deviceOs : null; ;
        if($form->validate()){
            $form->login();
            $userIndentity = Yii::$app->user->getIdentity();

            $this->user = $userIndentity;
            $tokenParams = [
                'user_id'      =>  $userIndentity->id,
                'auth_token'    =>  Yii::$app->security->generateRandomString(),
//                'device_token'  =>  $deviceToken,
//                'device_type'   =>  $deviceType,
//                'os_type'       =>  $osType
            ];

            $userSelf  = FitRoadUser::findOne(['id'=>$userIndentity->id]);
            $token = $userSelf->verifyAuthKey($tokenParams);

            if(!$token){
                $this->sendResponse(false,$this->builtErrorCode(100));
            }

            $this->outPutUser($userSelf,$token);

        }else{
            $this->getFormError($form);
        }
    }

    public function actionSignUp(){
        $businessMsg = null;
        $isUpdate  = FALSE;
        $token = null;

        if (isset($this->request->authentication->authToken) && !empty($this->request->authentication->authToken)) {
            $this->requireAuthToken();
            $form = new SignUpForm(['scenario'=>'update']);
            $form->user = $this->user;
            $isUpdate = TRUE;
        }else{
            $form = new SignUpForm(['scenario'=>'register']);
        }
//        echo '<pre>';
//        print_r($form->getScenario());
//        echo '</pre>';
//        die();

        $form->email = isset ($this->request->email) ? $this->request->email : null;
        $form->password = isset ($this->request->password) ? $this->request->password : null;
        $form->age = isset ($this->request->age) ? $this->request->age : null;
        $form->gender = isset ($this->request->gender) ? (string)$this->request->gender : null;
        $form->height = isset ($this->request->height) ? (string)$this->request->height : null;
        $form->weight = isset ($this->request->weight) ? (string)$this->request->weight : null;
        $form->activity = isset ($this->request->activity) ? $this->request->activity : null;
        $form->lastName = isset ($this->request->lastName) ? $this->request->lastName : null;
        $form->firstName = isset ($this->request->firstName) ? $this->request->firstName : null;


        if($form->validate() && $form->verify()){

            if($isUpdate === FALSE){
                $this->user = $form->user;

                $tokenParams = [
                    'user_id'      =>  $this->user->id,
                    'auth_token'    =>  Yii::$app->security->generateRandomString(),
                    //                'device_token'  =>  $deviceToken,
                    //                'device_type'   =>  $deviceType,
                    //                'os_type'       =>  $osType
                ];
                $token = $this->user->verifyAuthKey($tokenParams);
            }

            $uploadFile = $this->uploadAttachmentFile( Yii::getAlias('@publicImagesAvatars'));

            if ($uploadFile) {

                FitRoadGalleryImage::updateDoNotCurrentAllImage($this->user->id);

                $imageModel = new FitRoadGalleryImage();

                $galleryParam = [
                    'food_id'=>null,
                    'user_id'=>$this->user->id,
                    'url'=>$uploadFile->output[0],
                    'status'=> \api\commons\models\entities\FitRoadGalleryImage::STATUS_ACTIVE,
                    'type'=> \api\commons\models\entities\FitRoadGalleryImage::IMAGE_TYPE_AVATAR,
                    'is_current'=>\api\commons\models\entities\FitRoadGalleryImage::IS_CURRENT
                ];

                $imageModel->attributes = $galleryParam;

                if(!$imageModel->save()){
                    $businessMsg = ApiHelpers::getFormMessageError($imageModel);
                    ($businessMsg)?$this->warning = [
                        'businessError'=>[
                            'msg'=>$businessMsg,
                            'action'=>'upload_avatar'
                        ]]:null;
                }
//                try{
//                    $imageModel->save();
//                }catch (\yii\db\Exception $e){
//                   $businessMsg = ApiHelpers::getFormMessageError($imageModel);
//                }
            }

            if(!$token && $isUpdate === FALSE){
                $this->sendResponse(false,$this->builtErrorCode(100));
            }

            $this->outPutUser($this->user,$token);

        }else{
            $this->getFormError($form);
        }
    }

    public function actionSignOut(){
        $authToken = $this->requireAuthToken();
        Yii::$app->user->logout();
        try{
            $authToken->delete();
            $this->sendResponse();
        }
        catch (\yii\db\Exception $e){
            Yii::error('Error \'s name: '.$e->getName(), 'Users');
            Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
            Yii::error('Error Delete authetication token of user '.Yii::$app->user->id, 'Users');
        }
    }
    /**
     * @var : Password reset request
     */
    public function actionPasswordResetRequest(){

        $form = new SentTokenRestPasswordForm();
        $form->isBackend = FALSE;
        $form->email = isset ($this->request->email) ? $this->request->email : null;
        if($form->validate()){
            $send = $form->sendEmail();
            if($send == true)
                $this->sendResponse(['msgClient'=>Yii::t('app','Please check mail.')]);
            else
                $this->sendResponse(false,$this->builtErrorCode(94));
        }else{
            $this->getFormError($form);
        }
    }


}
<?php
/**
 * User: sangnguyen on  4/15/16 at 22:51
 * File name: FoodController.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace api\versions\v1\controllers;


use api\commons\helpers\ApiHelpers;
use api\commons\models\entities\FitRoadFood;
use api\commons\models\entities\FitRoadGalleryImage;
use api\commons\models\entities\FitRoadMealLog;
use api\commons\models\entities\FitRoadRestaurant;
use api\components\RestController;
use yii\data\ActiveDataProvider;

class FoodController extends RestController{
    public function actionGetList(){
        $this->requireAuthToken();

        $data= [];

        $restaurantId =  isset ($this->request->restaurantId) ? $this->request->restaurantId : null;
        $type = isset ($this->request->type) ? $this->request->type : null;

        if(!in_array($type,[1,2,null])){
            $this->sendResponse($this->builtErrorCode(400,\Yii::t('api/error','Type not support!')));
        }

        $restaurant = FitRoadRestaurant::findOne(['id'=>(int)$restaurantId,'status'=>1]);

        if(!$restaurant){
            $this->sendResponse($this->builtErrorCode(400,\Yii::t('api/error','Restaurant do not exits!')));
        }

        $params['recordPerpage']      = (int) (isset($this->request->pagination->recordPerpage)
            && $this->request->pagination->recordPerpage !=0)?
            $this->request->pagination->recordPerpage:20;

        $params['page']     = (int) (isset($this->request->pagination->currentPage)
            && $this->request->pagination->currentPage !=0)?
            $this->request->pagination->currentPage - 1 :0;

        $models = $restaurant->getFitRoadFoods();

        if($type && !empty($type) && $type != 0){
            $models->andOnCondition(['type'=>(int)$type]);
        }

        $modelProvider = new ActiveDataProvider([
            'query' => $models,
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]],
            'pagination' => [
                'pageSize'  =>  $params['recordPerpage'],
                'page'      =>  $params['page']
            ]
        ]);

        $result = $modelProvider->getModels();


        if($result){
            foreach($result as $_item){
                $data['foods'][] = $_item->getDataSelf();
            }
        }
        $data['pagination']  = $this->outputObjectPaging($modelProvider);

        $this->sendResponse($data);
    }

    public function actionEating(){
        $this->requireAuthToken();  

        $calories =  isset ($this->request->calories) ? $this->request->calories : null;
        $quantity =  isset ($this->request->quantity) ? $this->request->quantity : null;
        $typeOfMeal = isset ($this->request->typeOfMeal) ? $this->request->typeOfMeal : null;
        $foodId = isset ($this->request->foodId) ? $this->request->foodId : null;

        $foodName = isset ($this->request->foodName) ? $this->request->foodName : null;
        $description = isset ($this->request->description) ? $this->request->description : null;
        $date  = isset ($this->request->date) ? $this->request->date : null;

        $isManual = isset ($this->request->isManual) ?(int)$this->request->isManual : FALSE;

        if(!in_array($typeOfMeal,[1,2,3,4])){
            $this->sendResponse($this->builtErrorCode(400,\Yii::t('api/error','This type of meal do not support!')));
        }

        if($typeOfMeal == FitRoadMealLog::TYPE_MANUAL){
            $isManual = TRUE;
        }

        $food = FitRoadFood::findOne(['id'=>(int)$foodId,'status'=>1]);

        if(!$food && $isManual === FALSE){
            $this->sendResponse($this->builtErrorCode(400,\Yii::t('api/error','Food do not exits!')));
        }

        $dataFoodMealLog = [
            'food_id'=>($food)?(int)$food->id:null,
            'user_id'=>(int)$this->user->id,
            'meal_date'=> ($date && $date != 0) ? (int)$date:null,
            'type'=>(int)$typeOfMeal,
            'calories'=>($food)?(int)$food->calories:$calories,
            'name'=>($food)?(string)$food->name:$foodName,
            'description'=>($food)?(string)$food->description:$description,
            'quantity'=>(int)$quantity,
            'is_manual'=>$isManual
        ];

        $mealLog = new FitRoadMealLog();
        $mealLog->attributes = $dataFoodMealLog;

        if(!$mealLog->save()){
            $this->getFormError($mealLog);
        }
        $this->sendResponse(['meal'=>['id'=>$mealLog->id]]);
    }

    public function actionMealLogs(){
        $this->requireAuthToken();
        $data = [];

        $params['recordPerpage']      = (int) (isset($this->request->pagination->recordPerpage)
            && $this->request->pagination->recordPerpage !=0)?
            $this->request->pagination->recordPerpage:100;

        $params['page']     = (int) (isset($this->request->pagination->currentPage)
            && $this->request->pagination->currentPage !=0)?
            $this->request->pagination->currentPage - 1 :0;

        $params['queryAndOnCondition']['dateRequest'] =  isset ($this->request->date) ? $this->request->date :
            $this->sendResponse($this->builtErrorCode(400,
                \Yii::t('api/error','The date can not blank!')));

        $params['queryAndOnCondition']['user_id'] = $this->user->id;
      
        $provider =  (new FitRoadMealLog())->getList($params);

        $results = $provider->getModels();

        if($results){
            foreach($results as $_item){
                $data[] = $_item->getDataSelf();
            }
        }

        //$response['pagination'] = $this->outputObjectPaging($provider);
        $response['meals'] = $data;
        $this->sendResponse($response);
    }

    public function actionDeleteMealLog(){
        $this->requireAuthToken();
        $id      = (int) (isset($this->request->id)
            && $this->request->id !=0)?
            $this->request->id:  $this->sendResponse($this->builtErrorCode(400,\Yii::t('api/error','Meal log id can not blank!')));


        $model = FitRoadMealLog::findOne(['id'=>$id]);

        if(!$model){
            $this->sendResponse($this->builtErrorCode(400,\Yii::t('api/error','Meal log not found!')));
        }

        if($model->delete()){
            $this->sendResponse();
        }

    }
}
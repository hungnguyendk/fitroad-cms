<?php

namespace api\commons\models\queries;

/**
 * This is the ActiveQuery class for [[\api\commons\models\entities\FitRoadUser]].
 *
 * @see \api\commons\models\entities\FitRoadUser
 */
class FitRoadUserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \api\commons\models\entities\FitRoadUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\entities\FitRoadUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace api\commons\models\queries;

/**
 * This is the ActiveQuery class for [[\api\commons\models\entities\FitRoadRestaurant]].
 *
 * @see \api\commons\models\entities\FitRoadRestaurant
 */
class FitRoadRestaurantQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \api\commons\models\entities\FitRoadRestaurant[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\entities\FitRoadRestaurant|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace api\commons\models\queries;

/**
 * This is the ActiveQuery class for [[\api\commons\models\entities\FitRoadMealLog]].
 *
 * @see \api\commons\models\entities\FitRoadMealLog
 */
class FitRoadMealLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \api\commons\models\entities\FitRoadMealLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\entities\FitRoadMealLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php
/**
 * User: sangnguyen on  4/15/16 at 10:02
 * File name: FitRoadRestaurantRepository.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace api\commons\models\repositories;



use api\commons\helpers\ApiHelpers;

use api\commons\models\entities\FitRoadFood;
use api\commons\models\entities\FitRoadRestaurant;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\SqlDataProvider;
use yii\db\ActiveRecord;

Abstract class FitRoadRestaurantRepository extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    public function getRestaurantsAroundOnSystem($lat,$lng,$placeIds=[],$distance=20000){
        $res = [];
        $query = FitRoadRestaurant::find();

        $res = $query->addSelect(['*','( 3959 * acos (cos ( radians('.$lat.') )* cos( radians( `lat` ) )* cos( radians( `long` ) - radians('.$lng.') )+ sin ( radians('.$lat.') )* sin( radians( `lat` ) ))) AS distance'])
           // ->andOnCondition(['not in','place_id',$placeIds])
            ->having('distance < 30')
            ->orderBy('distance')
            ->limit(20)
            ->all();

        return $res;
    }

    public function getList($params=[],$lat=null,$lng=null){

        $query = FitRoadRestaurant::find();
        if($lat !== null && $lng !== null){
            $query->select(FitRoadRestaurant::tableName().'.*')
                ->addSelect(['( 3959 * acos (cos ( radians('.$lat.') )* cos( radians( `lat` ) )* cos( radians( `long` ) - radians('.$lng.') )+ sin ( radians('.$lat.') )* sin( radians( `lat` ) ))) AS distance'])
                // ->andOnCondition(['not in','place_id',$placeIds])
                ->having('distance < 6')
                ->orderBy('distance');
        }


        if(isset($params['queryAndOnCondition'])
            && is_array($params['queryAndOnCondition'])
            &&!empty($params['queryAndOnCondition']))
        {

            if(isset($params['queryAndOnCondition']['calories'])){
                $query->leftJoin(FitRoadFood::tableName(),
                    FitRoadFood::tableName().'.restaurant_id = '.FitRoadRestaurant::tableName().'.id'
                );
                $query->andOnCondition(['between',
                    FitRoadFood::tableName().'.calories',
                    (int)$params['queryAndOnCondition']['calories']-50,
                    (int)$params['queryAndOnCondition']['calories']+50]);
            }
            unset($params['queryAndOnCondition']['calories']);
            $query->andOnCondition($params['queryAndOnCondition']);
        }
//        $query ->limit(40);
//        return $query;

        $modelProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
          //  'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]],
            'pagination' => [
                'pageSize'  =>  $params['recordPerpage'],
                'page'      =>  $params['page']
            ]
        ]);

        return $modelProvider;
    }

    public function getDataSelf(){
        try{
            (!empty($this->google_data) && is_object(json_decode($this->google_data)))
                ?$googleData=json_decode($this->google_data):$googleData=null;
            return $data = [
                  "isVip"=> ($this->is_vip == 1)?TRUE:FALSE,
                  "createdAt"=>(int)$this->created_at,
                  "images"=> [],
                  "calories"=>(int)$this->calories,
                  "name"=> (string)$this->name,
                  "description"=> (string)$this->description,
                  "phone"=> (string)$this->phone,
                  "address"=> (string)$this->address,
                  "website"=> (string)$this->website,
                  "openTime"=> '',
                  "closeTime"=> '',
                  "lat"=> (string)$this->lat,
                  "lng"=> (string)$this->long,
                  "scope"=> 'Local',
                  "originScope"=>(string)($googleData && isset($googleData->originScop))?$googleData->originScope:null,
                  "id"=>(string)$this->id,
                  "placeId"=>(string)$this->place_id,
                  "identifier"=>(string)$this->identifier,
                  "openingHours"=>(string)($googleData && !empty($googleData->openingHours))?(string)$googleData->openingHours:'',
                  "ggPhotoSettings"=>isset($googleData->photo)?$googleData->photo:[],
                  "urlAvatarThumb"=>isset($this->avatar)?ApiHelpers::builtUrlImages(\Yii::$app->params['restaurantsFinder'],$this->avatar,\Yii::$app->params['thumbName']):null,
                  "urlAvatarOrigin"=>isset($this->avatar)?ApiHelpers::builtUrlImages(\Yii::$app->params['restaurantsFinder'],$this->avatar):null,
            ];
        }catch (\yii\db\Exception $e){
            \Yii::error('Error \'s name: '.$e->getName(), 'Users');
            \Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
            \Yii::error('Error get data self restaurant', 'Users');
        }
    }

}
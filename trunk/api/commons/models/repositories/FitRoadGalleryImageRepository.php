<?php
/**
 * User: sangnguyen on  4/13/16 at 00:05
 * File name: FitRoadGalleryImageRepository.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by AppsCyclone
 * All rights reserved
 */

namespace api\commons\models\repositories;

use api\commons\helpers\ApiHelpers;
use api\commons\models\entities\FitRoadGalleryImage;
use api\commons\models\entities\FixRoadGalleryImage;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

Abstract class FitRoadGalleryImageRepository extends ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    const IS_CURRENT = 1;
    CONST DO_NOT_CURRENT = 0;

    const IMAGE_TYPE_AVATAR = 'avatar';
    const IMAGE_TYPE_FOOD = 'food';
    const IMAGE_TYPE_ACTIVITY = 'activity';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    public function getList($params=[]){

        $query = FitRoadGalleryImage::find();
        if(isset($params['queryAndOnCondition'])
            && is_array($params['queryAndOnCondition'])
            &&!empty($params['queryAndOnCondition']))
        {

            if(isset($params['queryAndOnCondition']['dateRequest'])){
                $query->where("DATE_FORMAT( FROM_UNIXTIME( created_at ),'%D%M%Y') = DATE_FORMAT( FROM_UNIXTIME(:dateRequest ),'%D%M%Y')",
                    [
                        ':dateRequest'=>$params['queryAndOnCondition']['dateRequest']
                    ]);
            }

            unset($params['queryAndOnCondition']['dateRequest']);

            $query->andOnCondition($params['queryAndOnCondition']);
        }


        $modelProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]],
            'pagination' => [
                'pageSize'  =>  $params['recordPerpage'],
                'page'      =>  $params['page']
            ]
        ]);

        return $modelProvider;
    }

    public function getDataSelf($finder=null){
        if($finder == null){
            $finder =Yii::$app->params['activitiesFinder'];
        }

        try{
            return $data = [
                "urlImageThumb"=>(string)ApiHelpers::builtUrlImages($finder,
                    $this->url,Yii::$app->params['thumbName']),

                "urlImageOrigin"=>(string)ApiHelpers::builtUrlImages($finder,$this->url),

                "type"=>(string)$this->type,

                "createdAt"=>(int)$this->created_at,
                "id"=>(string)$this->id,
            ];
        }catch (\yii\db\Exception $e){
            \Yii::error('Error \'s name: '.$e->getName(), 'Users');
            \Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
            \Yii::error('Error get data self Gallery images', 'Users');
        }
    }

    static public function updateDoNotCurrentAllImage($user_id,$type='avatar'){

        return $model = Yii::$app->db->createCommand()->update(
            FitRoadGalleryImage::tableName(),[
                'is_current'=>0
            ],[
                'type'=>$type,
                'user_id'=>$user_id
            ])->execute();

    }
}
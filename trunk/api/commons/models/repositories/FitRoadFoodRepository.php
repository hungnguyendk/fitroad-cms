<?php
/**
 * User: sangnguyen on  4/15/16 at 23:07
 * File name: FitRoadFoodRepository.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace api\commons\models\repositories;


use api\commons\models\entities\FitRoadGalleryImage;
use yii\db\ActiveRecord;

Abstract class FitRoadFoodRepository extends ActiveRecord{
    CONST TYPE_SNACK = 2;
    CONST TYPE_BIGMEAL=1;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    public function getType(){
        switch($this->type){
            case self::TYPE_SNACK:
                return 'Snack';
            break;
            case self::TYPE_BIGMEAL:
                return 'Big Meal';
                break;
        }
    }

    public function getDataSelf(){
        $dataImage = [];
        $images = $this->getFitRoadGalleryImages()->andOnCondition([
            'status'=>1,
            'type'=>FitRoadGalleryImage::IMAGE_TYPE_FOOD
        ])->all();

        if($images && !empty($images)){
            foreach($images as $i){
                $dataImage[] = $i->getDataSelf(\Yii::$app->params['foodsFinder']);
            }
        }

        try{
            return $data = [
                  "images"=>$dataImage,
                  "restaurantId"=>(string)$this->restaurant->id,
                  "name"=> (string)$this->name,
                  "description"=>(string)$this->description,
                  "price"=> (int)$this->price,
                  "calories"=> (int)$this->calories,
                  "weight"=> (int)$this->weight,
                  "type"=> (string)$this->getType(),
                  "id"=>(string)$this->id,
                  "createdAt"=>(int)$this->created_at,
            ];
        }catch (\yii\db\Exception $e){
            \Yii::error('Error \'s name: '.$e->getName(), 'Users');
            \Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
            \Yii::error('Error get data self food', 'Users');
        }
    }
}
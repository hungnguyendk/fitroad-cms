<?php
/**
 * User: sangnguyen on  4/16/16 at 02:22
 * File name: FitRoadFoodMealLogRepository.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace api\commons\models\repositories;

use api\commons\models\entities\FitRoadFood;
use api\commons\models\entities\FitRoadMealLog;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

Abstract class FitRoadMealLogRepository extends ActiveRecord
{

    const  TYPE_SNACK = 3;
    const  TYPE_BREAKFAST= 1;
    const  TYPE_LUNCH = 2;
    const  TYPE_DINNER = 4;
    const  TYPE_MANUAL= 5;
    /**
     * @author Sang Nguyen
     * (non-PHPdoc)
     * @see \yii\base\Component::behaviors()
     */
    public function behaviors()
    {

        if($this->meal_date && !empty($this->meal_date)){
            return [
                'timestamp' => [
                    'class' => 'yii\behaviors\AttributeBehavior',
                    'attributes' => [
                        \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                        \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['created_at'],
                    ],
                    'value' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
                ],
            ];
        }else{
            return [
                'timestamp' => [
                    'class' => 'yii\behaviors\AttributeBehavior',
                    'attributes' => [
                        \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                        \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['meal_date'],
                        \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['created_at'],
                    ],
                    'value' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
                ],
            ];
        }

    }

    public function getList($params=[]){

        $query = FitRoadMealLog::find();
        if(isset($params['queryAndOnCondition'])
            && is_array($params['queryAndOnCondition'])
            &&!empty($params['queryAndOnCondition']))
        {

            if(isset($params['queryAndOnCondition']['calories'])){
                $query->andOnCondition(['between',
                    'calories',
                    (int)$params['queryAndOnCondition']['calories']-50,
                    (int)$params['queryAndOnCondition']['calories']+50]);
            }

            unset($params['queryAndOnCondition']['calories']);

            if(isset($params['queryAndOnCondition']['dateRequest'])){
                $query->where("DATE_FORMAT( FROM_UNIXTIME( meal_date ),'%D%M%Y') = DATE_FORMAT( FROM_UNIXTIME(:dateRequest ),'%D%M%Y')",
                    [
                        ':dateRequest'=>$params['queryAndOnCondition']['dateRequest']
                    ]);
            }

            unset($params['queryAndOnCondition']['dateRequest']);

            $query->andOnCondition($params['queryAndOnCondition']);
        }


        $modelProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]],
            'pagination' => [
                'pageSize'  =>  $params['recordPerpage'],
                'page'      =>  $params['page']
            ]
        ]);

        return $modelProvider;
    }


    public function getTypeOfMeal(){
        switch($this->type){
            case self::TYPE_SNACK:
                return 'Snack';
                break;
            case self::TYPE_BREAKFAST:
                return 'Breakfast';
                break;
            case self::TYPE_LUNCH:
                return 'Lunch';
                break;
            case self::TYPE_DINNER:
                return 'Dinner';
                break;
            case self::TYPE_MANUAL:
                return 'Manual';
                break;
        }
    }

    public function getDataSelf(){
        try{
            return $data = [
                "id"=>(string)$this->id,
                "food"=>($this->food)?$this->food->getDataSelf():'',
                "foodName"=>(string)$this->name,
                "mealDescription"=>(string)$this->description,
                "calories"=>(int)$this->calories,
                "mealDate"=>(int)$this->meal_date,
                "quantity"=>(int)$this->quantity,
                "typeOfMeal"=>(string)$this->getTypeOfMeal(),
                "isManual"=>boolval($this->is_manual),
                "createdAt"=>(int)$this->created_at,
            ];
        }catch (\yii\db\Exception $e){
            \Yii::error('Error \'s name: '.$e->getName(), 'Users');
            \Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
            \Yii::error('Error get data self meal logs', 'Users');
        }
    }
}
<?php
/**
 * User: sangnguyen on  4/16/16 at 03:33
 * File name: FitRoadRestaurantVisitReportRepository.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace api\commons\models\repositories;


use yii\db\ActiveRecord;

Abstract class FitRoadRestaurantVisitReportRepository extends ActiveRecord
{
    /**
     * @author Sang Nguyen
     * (non-PHPdoc)
     * @see \yii\base\Component::behaviors()
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\AttributeBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at' ],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['created_at'],
                ],
                'value' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
            ],
        ];
    }
}
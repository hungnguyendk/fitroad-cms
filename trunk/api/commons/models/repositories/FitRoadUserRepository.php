<?php
namespace api\commons\models\repositories;

use api\commons\helpers\ApiHelpers;
use api\commons\models\entities\FitRoadGalleryImage;
use yii;
use api\commons\models\entities\AuthenticationToken;

/**
 * User: sangnguyen on  4/12/16 at 20:13
 * File name: FitRoadUserRepositories.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by COMPANY
 * All rights reserved
 */
Abstract class FitRoadUserRepository extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    CONST FIX_ROAD_USER_ROOT = '0001';
    CONST FIX_ROAD_ADMIN = '0010';
    CONST FIX_ROAD_MANAGER = '0011';
    CONST FIX_ROAD_STAFF   = '4';
    CONST FIX_ROAD_CUSTOMER = '-1';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert){

        if(isset($this->password ) && !empty($this->password)){
            $this->password = \Yii::$app->security->generatePasswordHash($this->password);
        }else{
            unset($this['password']);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @author Sang Nguyen
     * @var Verify authentication token
     */
    public function verifyAuthKey($params = []){
        $token  = AuthenticationToken ::findOne([
            'user_id' => $params['user_id'],
        ]);
        if($token){
            $token->user_id = $params['user_id'];
            $token->auth_token = Yii::$app->security->generateRandomString();
            $token->save(false);
        }else{
            $token = new AuthenticationToken();
            $token->attributes = $params;
            $token->save(false);
        }
        if($token)
            return $token;
        return false;
    }
    /**
     * @return array
     */
    public function getDataSelfUser(){

        try{
            return $data = [
                "createdAt"=>(int)$this->created_at,
                "email"     =>  (string)$this->email,
                "firstName" =>  (string)$this->first_name,
                "lastName"  =>  (string)$this->last_name,
                "age"       =>  (int)$this->age,
                "gender"    =>  (string)$this->gender,
                "height"    =>  (string)$this->height,
                "weight"    =>  (string)$this->weight,
                "activity"  =>  (int)$this->point,
                'avatarUrl' =>  (string)$this->getAvatar()
            ];
        }catch (\yii\db\Exception $e){
            Yii::error('Error \'s name: '.$e->getName(), 'Users');
            Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
            Yii::error('Error get data self contact', 'Users');
        }
    }


    public function getAvatar(){
        $avatar = $this->getFitRoadGalleryImages()->andOnCondition([
            'type'=>FitRoadGalleryImage::IMAGE_TYPE_AVATAR,
            'is_current'=>FitRoadGalleryImage::IS_CURRENT,
            'status'=>FitRoadGalleryImage::STATUS_ACTIVE
        ])->one();

        if($avatar)
            return ApiHelpers::builtUrlImages(Yii::$app->params['avatarsFinder'],
                $avatar->url,Yii::$app->params['thumbName']);
        else
            return null;
    }
}
<?php

namespace api\commons\models\entities;

use Yii;

/**
 * This is the model class for table "fit_road_category".
 *
 * @property integer $id
 * @property integer $name
 * @property integer $status
 * @property integer $created_at
 * @property integer $restaurant_id
 *
 * @property FitRoadRestaurant $restaurant
 * @property FitRoadFood[] $fitRoadFoods
 */
class FitRoadCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'status', 'created_at', 'restaurant_id'], 'integer'],
            [['name'], 'unique'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadRestaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('api', 'ID'),
            'name' => Yii::t('api', 'Name'),
            'status' => Yii::t('api', 'Status'),
            'created_at' => Yii::t('api', 'Created At'),
            'restaurant_id' => Yii::t('api', 'Restaurant ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(FitRoadRestaurant::className(), ['id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadFoods()
    {
        return $this->hasMany(FitRoadFood::className(), ['category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\queries\FitRoadCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \api\commons\models\queries\FitRoadCategoryQuery(get_called_class());
    }
}

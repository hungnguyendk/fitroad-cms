<?php

namespace api\commons\models\entities;

use api\commons\models\repositories\FitRoadRestaurantRepository;
use Yii;

/**
 * This is the model class for table "fit_road_restaurant".
 *
 * @property integer $id
 * @property string $avatar
 * @property string $place_id
 * @property string $identifier
 * @property string $name
 * @property string $description
 * @property string $phone
 * @property string $fax
 * @property string $website
 * @property string $lat
 * @property string $long
 * @property string $address
 * @property string $icon
 * @property string $type
 * @property integer $status
 * @property string $google_data
 * @property integer $rating
 * @property integer $calories
 * @property integer $is_vip
 * @property integer $is_local
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property FitRoadCategory[] $fitRoadCategories
 * @property FitRoadFood[] $fitRoadFoods
 * @property FitRoadRestaurantHasManagedByUser[] $fitRoadRestaurantHasManagedByUsers
 * @property FitRoadRestaurantVisitReport[] $fitRoadRestaurantVisitReports
 */
class FitRoadRestaurant extends FitRoadRestaurantRepository
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_restaurant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description', 'google_data'], 'string'],
            [['status', 'rating', 'calories', 'is_vip', 'is_local', 'created_at', 'updated_at'], 'integer'],
            [['avatar', 'identifier', 'address', 'type'], 'string', 'max' => 255],
            [['place_id'], 'string', 'max' => 155],
            [['name', 'website'], 'string', 'max' => 115],
            [['phone'], 'string', 'max' => 15],
            [['fax'], 'string', 'max' => 25],
            [['lat', 'long'], 'string', 'max' => 45],
            [['icon'], 'string', 'max' => 225],
            [['place_id'], 'unique'],
            [['identifier'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('api', 'ID'),
            'avatar' => Yii::t('api', 'Avatar'),
            'place_id' => Yii::t('api', 'google place ID'),
            'identifier' => Yii::t('api', 'Identifier'),
            'name' => Yii::t('api', 'Name'),
            'description' => Yii::t('api', 'Description'),
            'phone' => Yii::t('api', 'Phone'),
            'fax' => Yii::t('api', 'Fax'),
            'website' => Yii::t('api', 'Website'),
            'lat' => Yii::t('api', 'Lat'),
            'long' => Yii::t('api', 'Long'),
            'address' => Yii::t('api', 'Address'),
            'icon' => Yii::t('api', 'Icon'),
            'type' => Yii::t('api', 'Type'),
            'status' => Yii::t('api', 'Status'),
            'google_data' => Yii::t('api', 'Google Data'),
            'rating' => Yii::t('api', 'Rating'),
            'calories' => Yii::t('api', 'Calories'),
            'is_vip' => Yii::t('api', 'Is Vip'),
            'is_local' => Yii::t('api', 'Is Local'),
            'created_at' => Yii::t('api', 'Created At'),
            'updated_at' => Yii::t('api', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadCategories()
    {
        return $this->hasMany(FitRoadCategory::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadFoods()
    {
        return $this->hasMany(FitRoadFood::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadRestaurantHasManagedByUsers()
    {
        return $this->hasMany(FitRoadRestaurantHasManagedByUser::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadRestaurantVisitReports()
    {
        return $this->hasMany(FitRoadRestaurantVisitReport::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\queries\FitRoadRestaurantQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \api\commons\models\queries\FitRoadRestaurantQuery(get_called_class());
    }
}

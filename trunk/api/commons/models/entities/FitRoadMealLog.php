<?php

namespace api\commons\models\entities;

use api\commons\models\repositories\FitRoadMealLogRepository;
use Yii;

/**
 * This is the model class for table "fit_road_meal_log".
 *
 * @property integer $id
 * @property integer $food_id
 * @property integer $meal_date
 * @property integer $user_id
 * @property integer $type
 * @property integer $calories
 * @property integer $is_maual
 * @property string $name
 * @property string $description
 * @property integer $quantity
 * @property integer $status
 * @property integer $created_at
 *
 * @property FitRoadUser $user
 * @property FitRoadFood $food
 */
class FitRoadMealLog extends FitRoadMealLogRepository
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_meal_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['food_id', 'meal_date', 'user_id', 'type', 'calories', 'is_manual', 'quantity', 'status', 'created_at'], 'integer'],
            [['user_id'], 'required'],
            [['name'], 'string', 'max' => 115],
            [['description'], 'string', 'max' => 555],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['food_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadFood::className(), 'targetAttribute' => ['food_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('api', 'ID'),
            'food_id' => Yii::t('api', 'Food ID'),
            'meal_date' => Yii::t('api', 'Meal Date'),
            'user_id' => Yii::t('api', 'User ID'),
            'type' => Yii::t('api', 'Type'),
            'calories' => Yii::t('api', 'Calories'),
            'is_manual' => Yii::t('api', 'Is Maual'),
            'name' => Yii::t('api', 'Name'),
            'description' => Yii::t('api', 'Description'),
            'quantity' => Yii::t('api', 'Quantity'),
            'status' => Yii::t('api', 'Status'),
            'created_at' => Yii::t('api', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(FitRoadUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFood()
    {
        return $this->hasOne(FitRoadFood::className(), ['id' => 'food_id']);
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\queries\FitRoadMealLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \api\commons\models\queries\FitRoadMealLogQuery(get_called_class());
    }
}

<?php

namespace api\commons\models\entities;

use api\commons\models\repositories\FitRoadGalleryImageRepository;
use Yii;

/**
 * This is the model class for table "fit_road_gallery_image".
 *
 * @property integer $id
 * @property integer $food_id
 * @property integer $user_id
 * @property string $url
 * @property integer $status
 * @property string $type
 * @property integer $is_current
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $message
 *
 * @property FitRoadUser $user
 * @property FitRoadFood $food
 */
class FitRoadGalleryImage extends FitRoadGalleryImageRepository
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_gallery_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['food_id', 'user_id', 'status', 'is_current', 'created_at', 'updated_at'], 'integer'],
            [['type'], 'required'],
            [['type'], 'string'],
            [['url'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 115],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['food_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadFood::className(), 'targetAttribute' => ['food_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('api', 'ID'),
            'food_id' => Yii::t('api', 'Food ID'),
            'user_id' => Yii::t('api', 'User ID'),
            'url' => Yii::t('api', 'Url'),
            'status' => Yii::t('api', 'Status'),
            'type' => Yii::t('api', 'Type'),
            'is_current' => Yii::t('api', 'Is Current'),
            'created_at' => Yii::t('api', 'Created At'),
            'updated_at' => Yii::t('api', 'Updated At'),
            'message' => Yii::t('api', 'Message'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(FitRoadUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFood()
    {
        return $this->hasOne(FitRoadFood::className(), ['id' => 'food_id']);
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\queries\FitRoadGalleryImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \api\commons\models\queries\FitRoadGalleryImageQuery(get_called_class());
    }
}

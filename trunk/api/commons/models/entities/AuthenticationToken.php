<?php

namespace api\commons\models\entities;

use Yii;

/**
 * This is the model class for table "authentication_token".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $auth_token
 * @property string $device_token
 * @property string $device_type
 * @property string $os_type
 * @property string $os_info
 * @property integer $last_accessed
 * @property string $last_location
 * @property string $created_date
 *
 * @property FitRoadUser $user
 */
class AuthenticationToken extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authentication_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'auth_token', 'created_date'], 'required'],
            [['user_id', 'last_accessed'], 'integer'],
            [['created_date'], 'safe'],
            [['auth_token', 'device_token', 'os_info', 'last_location'], 'string', 'max' => 255],
            [['device_type', 'os_type'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('api', 'ID'),
            'user_id' => Yii::t('api', 'User ID'),
            'auth_token' => Yii::t('api', 'Auth Token'),
            'device_token' => Yii::t('api', 'Device Token'),
            'device_type' => Yii::t('api', 'Device Type'),
            'os_type' => Yii::t('api', 'Os Type'),
            'os_info' => Yii::t('api', 'Os Info'),
            'last_accessed' => Yii::t('api', 'Last Accessed'),
            'last_location' => Yii::t('api', 'Last Location'),
            'created_date' => Yii::t('api', 'Created Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(FitRoadUser::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\queries\AuthenticationTokenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \api\commons\models\queries\AuthenticationTokenQuery(get_called_class());
    }
}

<?php

namespace api\commons\models\entities;

use api\commons\models\repositories\FitRoadRestaurantVisitReportRepository;
use Yii;

/**
 * This is the model class for table "fit_road_restaurant_visit_report".
 *
 * @property integer $id
 * @property integer $restaurant_id
 * @property integer $user_id
 * @property string $device_version
 * @property string $request_ip
 * @property integer $created_at
 *
 * @property FitRoadRestaurant $restaurant
 * @property FitRoadUser $user
 */
class FitRoadRestaurantVisitReport extends FitRoadRestaurantVisitReportRepository
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_restaurant_visit_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'user_id'], 'required'],
            [['restaurant_id', 'user_id', 'created_at'], 'integer'],
            [['request_ip'], 'string', 'max' => 45],
            [['device_version'], 'string', 'max' => 255],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadRestaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('api', 'ID'),
            'restaurant_id' => Yii::t('api', 'Restaurant ID'),
            'user_id' => Yii::t('api', 'User ID'),
            'device_version' => Yii::t('api', 'Device Version'),
            'request_ip' => Yii::t('api', 'Request Ip'),
            'created_at' => Yii::t('api', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(FitRoadRestaurant::className(), ['id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(FitRoadUser::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\queries\FitRoadRestaurantVisitReportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \api\commons\models\queries\FitRoadRestaurantVisitReportQuery(get_called_class());
    }
}

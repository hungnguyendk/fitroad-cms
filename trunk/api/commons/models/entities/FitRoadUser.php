<?php

namespace api\commons\models\entities;

use api\commons\models\repositories\FitRoadUserRepository;
use Yii;

/**
 * This is the model class for table "fit_road_user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $password
 * @property string $email
 * @property string $height
 * @property string $avatar
 * @property string $weight
 * @property string $gender
 * @property integer $age
 * @property string $phone_number
 * @property string $alias
 * @property integer $point
 * @property string $password_reset_token
 * @property string $time_zone
 * @property string $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthenticationToken[] $authenticationTokens
 * @property FitRoadGalleryImage[] $fitRoadGalleryImages
 * @property FitRoadMealLog[] $fitRoadMealLogs
 * @property FitRoadRestaurantHasManagedByUser[] $fitRoadRestaurantHasManagedByUsers
 * @property FitRoadRestaurantVisitReport[] $fitRoadRestaurantVisitReports
 * @property FitRoadUserActivity[] $fitRoadUserActivities
 */
class FitRoadUser extends FitRoadUserRepository
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email'], 'required'],
            [['age', 'point', 'status', 'created_at', 'updated_at'], 'integer'],
            [['role'], 'string'],
            [['first_name', 'last_name', 'email', 'alias', 'time_zone'], 'string', 'max' => 45],
            [['password','avatar'], 'string', 'max' => 255],
            [['height', 'weight'], 'string', 'max' => 11],
            [['gender'], 'string', 'max' => 4],
            [['phone_number'], 'string', 'max' => 15],
            [['password_reset_token'], 'string', 'max' => 115],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('api', 'ID'),
            'first_name' => Yii::t('api', 'First Name'),
            'last_name' => Yii::t('api', 'Last Name'),
            'avatar' => Yii::t('api', 'Avatar'),
            'password' => Yii::t('api', 'Password'),
            'email' => Yii::t('api', 'Email'),
            'height' => Yii::t('api', 'Height'),
            'weight' => Yii::t('api', 'Weight'),
            'gender' => Yii::t('api', 'Gender'),
            'age' => Yii::t('api', 'Age'),
            'phone_number' => Yii::t('api', 'Phone Number'),
            'alias' => Yii::t('api', 'Alias'),
            'point' => Yii::t('api', 'Point'),
            'password_reset_token' => Yii::t('api', 'Password Reset Token'),
            'time_zone' => Yii::t('api', 'Time Zone'),
            'role' => Yii::t('api', 'Role'),
            'status' => Yii::t('api', 'Status'),
            'created_at' => Yii::t('api', 'Created At'),
            'updated_at' => Yii::t('api', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthenticationTokens()
    {
        return $this->hasMany(AuthenticationToken::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadGalleryImages()
    {
        return $this->hasMany(FitRoadGalleryImage::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadMealLogs()
    {
        return $this->hasMany(FitRoadMealLog::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadRestaurantHasManagedByUsers()
    {
        return $this->hasMany(FitRoadRestaurantHasManagedByUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadRestaurantVisitReports()
    {
        return $this->hasMany(FitRoadRestaurantVisitReport::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadUserActivities()
    {
        return $this->hasMany(FitRoadUserActivity::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\queries\FitRoadUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \api\commons\models\queries\FitRoadUserQuery(get_called_class());
    }
}

<?php

namespace api\commons\models\entities;

use Yii;

/**
 * This is the model class for table "log_api".
 *
 * @property integer $id
 * @property string $method
 * @property string $request_url
 * @property string $request_string
 * @property string $reponse_string
 * @property integer $owner_id
 * @property string $duration
 * @property string $request_ip
 * @property string $device_type
 * @property string $device_version
 * @property double $created_at
 */
class LogApi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_api';
    }

    /**
     * @author Sang Nguyen
     * (non-PHPdoc)
     * @see \yii\base\Component::behaviors()
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\AttributeBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at' ],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['created_at'],
                ],
                'value' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['method', 'request_url', 'request_string', 'reponse_string', 'duration', 'request_ip'], 'required'],
            [['request_string', 'reponse_string'], 'string'],
            [['owner_id'], 'integer'],
            [['created_at'], 'number'],
            [['method'], 'string', 'max' => 10],
            [['request_url'], 'string', 'max' => 300],
            [['duration'], 'string', 'max' => 20],
            [['request_ip'], 'string', 'max' => 32],
            [['device_type'], 'string', 'max' => 64],
            [['device_version'], 'string', 'max' => 288],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('api', 'ID'),
            'method' => Yii::t('api', 'Method'),
            'request_url' => Yii::t('api', 'Request Url'),
            'request_string' => Yii::t('api', 'Request String'),
            'reponse_string' => Yii::t('api', 'Reponse String'),
            'owner_id' => Yii::t('api', 'Owner ID'),
            'duration' => Yii::t('api', 'Duration'),
            'request_ip' => Yii::t('api', 'Request Ip'),
            'device_type' => Yii::t('api', 'Device Type'),
            'device_version' => Yii::t('api', 'Device Version'),
            'created_at' => Yii::t('api', 'Created At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \api\commons\models\queries\LogApiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \api\commons\models\queries\LogApiQuery(get_called_class());
    }
    /**
     * @author SangNguyen
     * @param unknown $params
     */
    public function writeLog($params=array()){

        try {
            $this->attributes = $params;

            $this->save(false);

        } catch(\yii\db\Exception $e){
            Yii::error('Error \'s name: '.$e->getName(), 'Write log API');
            Yii::error('Error \'s message: '.$e->getMessage(), 'Write log API');
        }
    }
}

<?php
/**
 * User: sangnguyen
 * Date: 9/30/15
 * Time: 13:15
 * File name: SentTokenRestPasswordForm.php
 * Project name: cycmover
 */

namespace api\commons\forms;

use Yii;
use yii\base\Model;
use  api\commons\models\entities\FitRoadUser;

class SentTokenRestPasswordForm extends Model{
    public $email;
    public $isBackend=TRUE;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\api\commons\models\entities\FitRoadUser',
                'message' => Yii::t('api/error','The email address is not correct. Please try again')
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user FitRoadUser */
        $user = \common\models\UserIdentity::findOne([
            'email' => $this->email,
        ]);

        if($this->isBackend == TRUE){
            $linkRestPassword = Yii::$app->params['backendHost'].'/auth/authenticate/reset-password?token='.$user->password_reset_token;
        }else{
            $linkRestPassword = 'http://fitroad.ca/site/reset-password?token='.$user->password_reset_token;
        }

        if ($user) {
            if (!\common\models\UserIdentity::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save(false)) {

                $email = \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user,'linkRestPassword'=>$linkRestPassword])
                    ->setFrom(['support@fitroad.ca' =>  'FitRoad Support'])
                    ->setTo($this->email)
                    ->setSubject('Password reset for ' . \Yii::$app->name);

                try {
                    $email->send();
                } catch (\Swift_TransportException $e) {
                    Yii::error($e->getMessage());
                    return true;
                }

                /*return \Yii::$app->mailer->compose('EDM_resetPassword', ['model' => $this])
                    ->setFrom(['admin@cycmovers.com'=>\Yii::$app->params['sendMailQuotationFormDefaultName']])
                    ->setTo($this->email)
                    ->setSubject(Yii::t('app','CYC Mover LLP: Forgot password'))
                    ->setHtmlBody(Yii::t('app','<p>Dear {full_name},</p><p> you have requested for your password to be
                        reset. Please click on the following link to continue resetting your password,
                        please click <a href="{forgot_password_link}">here</a> to reset your password.</p>
                        <p>Thanks,</p>',[
                        'full_name'=>$user->first_name . ' ' . $user->last_name,
                        'forgot_password_link' => Yii::$app->params['backEndHostInfo']
                            .'/auth/default/forgot-password?reset_token='.$user->password_reset_token
                    ]))->send();*/
            }
            return true;
        }
        return FALSE;
    }
}
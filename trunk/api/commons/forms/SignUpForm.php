<?php
/**
 * User: sangnguyen on  11/04/15 at 22:18
 * File name: SignUpForm.php
 * Project name: Fit Road
 * Copyright (c) 2015 by COMPANY
 * All rights reserved
 */

namespace api\commons\forms;

use api\commons\models\entities\FitRoadUser;
use api\commons\helpers\ApiHelpers;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;


class SignUpForm extends Model{
    CONST SCENARIO_UPDATE       =   'update';
    CONST SCENARIO_REGISTER     =   'register';

    public $user;
    public $password;
    public $email;
    public $age;
    public $weight;
    public $height;
    public $activity;
    public $firstName;
    public $lastName;
    public $role = -1;
    public $phone_number;
    public $gender;
    /**
     * @inheritdoc
     */

    public function rules()
    {
        $parentRules = parent::rules();
        ($this->user)?$user_id=$this->user->id:$user_id=null;

        $currentRules =  [
            ['role', 'integer'],
            ['role', 'in','range' => ['0011','4','0010','-1'],'message' => Yii::t('backend/error', 'role not existed')],

            ['gender','filter','filter' => 'trim'],
            ['gender', 'in','range' => ['0','1'],'message' => Yii::t('backend/error', 'gender not existed')],

            ['phone_number', 'integer'],
            ['phone_number', 'match', 'pattern'=>'/^[1-9]{9,12}$/','message' => Yii::t('backend/error',
                'phone number validate')],

            ['lastName','filter','filter' => 'trim'],
            ['lastName', 'required', 'on'    =>  self::SCENARIO_REGISTER,],

            ['firstName','filter','filter' => 'trim'],
            ['firstName', 'required', 'on'    =>  self::SCENARIO_REGISTER,],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'on'    =>  self::SCENARIO_REGISTER],
            ['email', 'email'],

            [
                'email', 'unique', 'targetClass' => '\backend\commons\models\entities\FitRoadUser',
                //'filter'=> ['not in','id',[$this->id]],
                'on'    =>  self::SCENARIO_REGISTER,
                'message' => Yii::t('backend/error', 'email existed'),
            ],

            [
                'email', 'unique', 'targetClass' => '\backend\commons\models\entities\FitRoadUser',
                'filter'=> ['not in','id',[$user_id]],
                'on'    =>  self::SCENARIO_UPDATE,
                'message' => Yii::t('backend/error', 'email existed'),
            ],

//            ['password', 'required','on'=>self::SCENARIO_REGISTER],
//            ['password', 'required','on'=>self::SCENARIO_REGISTER],

            [['password'], 'required','on'=>self::SCENARIO_REGISTER],
            ['password','filter','filter' => 'trim'],


        ];
        return ArrayHelper::merge($parentRules,$currentRules);
    }
/*    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = ['username', 'password'];
        $scenarios[self::SCENARIO_REGISTER] = ['username', 'email', 'password'];
        return $scenarios;
    }*/

    public function verify(){
        $model = new FitRoadUser();

        if($this->user){
            $model = $this->user;
        }

//        $this->phone_number     =   $this->phone_number;
        $model->status      =   FitRoadUser::STATUS_ACTIVE;
        $model->last_name   =   $this->lastName;
        $model->first_name  =   $this->firstName;
        $model->age         =   $this->age;
        $model->gender      =   (isset($this->gender) && !empty(isset($this->gender)))?$this->gender:-1;
        $model->height      =   $this->height;
        $model->weight      =   $this->weight;
        $model->point       =   $this->activity;
        $model->password    =   $this->password;
        $model->email       =   $this->email;
        $model->role        =   FitRoadUser::FIX_ROAD_CUSTOMER;
//        $this->role             =   $this->role;
        if(!$model->save()){
            $this->addError('user',ApiHelpers::getFormMessageError($model));
            return FALSE;
        }
        return $this->user = $model;
    }
}
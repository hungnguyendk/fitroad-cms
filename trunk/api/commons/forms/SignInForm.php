<?php
/**
 * User: sangnguyen
 * Date: 9/30/15
 * Time: 08:43
 * File name: SingInForm.php
 * Project name: cycmover
 */

namespace api\commons\forms;

use Yii;
use yii\base\Model;
use common\models\UserIdentity;

/**
 * @property string $email
 * @property string $password

 * Class SingInForm
 * @package api\common\forms
 */
class SignInForm extends Model
{
    public $email;
    public $password;

    private $_user = false;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('api/error', 'login fail'));
            }elseif($user->status !== UserIdentity::STATUS_ACTIVE){
                $this->addError($attribute, Yii::t('api/error','user ban'));
                return false;
            }
        }
    }
    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return UserIdentity|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserIdentity::findByEmail($this->email)->one();
        }
        return $this->_user;
    }
}
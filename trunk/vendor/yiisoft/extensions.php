<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'kartik-v/yii2-widget-sidenav' => 
  array (
    'name' => 'kartik-v/yii2-widget-sidenav',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/sidenav' => $vendorDir . '/kartik-v/yii2-widget-sidenav',
    ),
  ),
  'moonlandsoft/yii2-tinymce' => 
  array (
    'name' => 'moonlandsoft/yii2-tinymce',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@moonland/tinymce' => $vendorDir . '/moonlandsoft/yii2-tinymce',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf',
    ),
  ),
  'c006/yii2-migration-utility' => 
  array (
    'name' => 'c006/yii2-migration-utility',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@c006/utility/migration' => $vendorDir . '/c006/yii2-migration-utility',
    ),
  ),
  'nterms/yii2-pagesize-widget' => 
  array (
    'name' => 'nterms/yii2-pagesize-widget',
    'version' => '2.0.1.0',
    'alias' => 
    array (
      '@nterms/pagesize' => $vendorDir . '/nterms/yii2-pagesize-widget',
    ),
  ),
  'codiverum/yii2-relation-search-filter' => 
  array (
    'name' => 'codiverum/yii2-relation-search-filter',
    'version' => '1.0.7.0',
    'alias' => 
    array (
      '@codiverum/relationSF' => $vendorDir . '/codiverum/yii2-relation-search-filter',
    ),
  ),
  'nickcv/yii2-mandrill' => 
  array (
    'name' => 'nickcv/yii2-mandrill',
    'version' => '1.6.1.0',
    'alias' => 
    array (
      '@nickcv/mandrill' => $vendorDir . '/nickcv/yii2-mandrill',
    ),
  ),
  'mdmsoft/yii2-admin' => 
  array (
    'name' => 'mdmsoft/yii2-admin',
    'version' => '2.6.0.0',
    'alias' => 
    array (
      '@mdm/admin' => $vendorDir . '/mdmsoft/yii2-admin',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.4.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
);

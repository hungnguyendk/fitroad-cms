<?php
use yii\widgets\Breadcrumbs;
?>
<div class="page-content-wrapper">
	<div class="page-content" style="overflow: scroll">
		<!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            <?php echo $this->title;?>
        </h3>
		<div class="page-bar">
      <?php
        echo Breadcrumbs::widget([
            'options' => [
                'class' => 'page-breadcrumb'
            ],
            'homeLink' => [
                'label' => Yii::t('yii', 'Dashboard'),
                'url' => Yii::$app->homeUrl,
                'template' => '<li><i class="fa fa-home"></i> {link}<i class="fa fa-angle-right"></i></li>'
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
        ])?>
        </div>
		<!-- END PAGE HEADER-->
        <?php echo $content?>
    </div>
</div>
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$this->registerJsFile(Yii::$app->homeUrl.'scripts/site/MyLogin.js',[ 'depends' => 'yii\web\YiiAsset']);
$this->registerCss(".input-icon > i {margin:14px 2px 4px 10px;}");
?>
<?php
$script = <<< JS
  Metronic.init(); // init metronic core components
  Layout.init(); // init current layout
  MyLogin.init();
JS;
$this->registerJs($script, \yii\web\View::POS_READY, $key = null);

?>
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="">
		<img src="<?php echo Yii::$app->params['urlAssetsImagesUploaded'] . "icon_cms.png"; ?>" style="margin: 0px auto;" alt="" class="img-responsive">
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'login-form']]); ?>
	<h3 class="form-title">Login to your account</h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
			<span>
			Enter any email and password. </span>
	</div>
	<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

	<?= $form->field($model, 'email',[
		'inputOptions' => [
			'placeholder' => $model->getAttributeLabel('email'),
		],
		'inputTemplate' => '<div class="input-icon"><i class="fa fa-user"></i>{input}</div>',
	]) ?>

	<?= $form->field($model, 'password',[
		'inputOptions' => [
			'placeholder' => $model->getAttributeLabel('password'),
		],
		'inputTemplate' => '<div class="input-icon"><i class="fa fa-lock"></i>{input}</div>',
	])->passwordInput() ?>

	<?= $form->field($model, 'rememberMe')->checkbox() ?>

	<div class="form-group">
		<?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block uppercase', 'name' => 'login-button']) ?>
	</div>
	<h4>Forgot your password ?</h4>
	<p>
		no worries, click <a href="javascript:;" id="forget-password">
			here </a>
		to reset your password.
	</p>
	<?php ActiveForm::end(); ?>
	<!--		<div class="form-group">-->
	<!--			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
	<!--			<label class="control-label visible-ie8 visible-ie9">Username</label>-->
	<!--			<div class="input-icon">-->
	<!--				<i class="fa fa-user"></i>-->
	<!--				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="LoginForm[email]">-->
	<!--			</div>-->
	<!--		</div>-->
	<!--		<div class="form-group">-->
	<!--			<label class="control-label visible-ie8 visible-ie9">Password</label>-->
	<!--			<div class="input-icon">-->
	<!--				<i class="fa fa-lock"></i>-->
	<!--				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="LoginForm[password]">-->
	<!--			</div>-->
	<!--		</div>-->
	<!--		<div class="form-actions">-->
	<!--			<label class="checkbox">-->
	<!--			<input type="checkbox" name="LoginForm[rememberMe]" value="1"> Remember me </label>-->
	<!--			<button type="submit" class="btn green-haze pull-right">-->
	<!--			Login <i class="m-icon-swapright m-icon-white"></i>-->
	<!--			</button>-->
	<!--		</div>-->

	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->

	<?php $form = ActiveForm::begin(['action'=>['/auth/authenticate/password-reset-request'],'id' => 'forget-form', 'options' => ['class' => 'my-forget-form']]); ?>
	<h3>Forget Password ?</h3>
	<p>
		Enter your e-mail address below to reset your password.
	</p>
	<div class="form-group">
		<div class="input-icon">
			<i class="fa fa-envelope"></i>
			<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email">
		</div>
	</div>
	<div class="form-actions">
		<button type="button" id="back-btn" class="btn">
			<i class="m-icon-swapleft"></i> Back </button>
		<button type="submit" class="btn green-haze pull-right">
			Submit <i class="m-icon-swapright m-icon-white"></i>
		</button>
	</div>
	<?php ActiveForm::end(); ?>

	<!-- END FORGOT PASSWORD FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	<?php echo date("Y") ?> &copy; COMPANY
</div>
<!-- END COPYRIGHT -->

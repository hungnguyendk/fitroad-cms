var MyLogin = function() {

    var handleLogin = function() {

        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },

            messages: {
                username: {
                    required: "Username is required."
                },
                password: {
                    required: "Password is required."
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }

    var handleForgetPassword = function() {
    	$('.my-forget-form').hide();
    	
    	$('#btn-submit-forget-from').click(function(){
    		var email = $('input[name="forget-email"]').val();
    		var form = '.my-forget-form';
    		$.ajax({
                type: 'POST',
                url: '/site/forget-password',
                data: 'email='+email+'&_csrf='+yii.getCsrfToken()
            })
            .done(function (response) {
            	if(response.status === true){
    				$(form).parent().text(response.msg);            				
    			}
    			else
    			{
    				$(form).find('.form-group').append('<span id="email-error" class="help-block">'+response.error+'</span>');
    			}
    			return false;
            });
        	return false;
    	});
        

//        $('.my-forget-form input').keypress(function(e) {
//            if (e.which == 13) {
//                if ($('.my-forget-form').validate().form()) {
//                	//alert('sd');return false;
//                    //$('.my-forget-form').submit();
//                }
//                return false;
//            }
//        });

        jQuery('#forget-password').click(function() {
            jQuery('.login-form').hide();
            jQuery('.my-forget-form').show();
        });

        jQuery('#back-btn').click(function() {
            jQuery('.login-form').show();
            jQuery('.my-forget-form').hide();
        });

    }

    return {
        //main function to initiate the module    	
        init: function() {        	
        	
            handleLogin();
            handleForgetPassword();

        }

    };

}();
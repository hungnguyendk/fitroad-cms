/************************************************
 * User: sangnguyen on  13/04/15 at 16:46         *
 * File name:                       *
 * Project name: Fit Road               *
 * Copyright (c) 2015 by COMPANY                 *
 * All rights reserved                          *
 ************************************************
 */

var UtilHelper= {
    handleCurrencyInput : function(){
        $("#currency").maskMoney({prefix:'',allowZero:true, allowNegative: true, thousands:'', decimal:'.', affixesStay: false});
        (function($) {
            $.fn.currencyInput = function() {
                this.each(function() {
                    var wrapper = $("<div class='currency-input' />");
                    $(this).wrap(wrapper);
                    $(this).before("<span class='currency-symbol'>đ</span>");
                   /* $(this).change(function() {
                        var min = parseFloat($(this).attr("min"));
                        var max = parseFloat($(this).attr("max"));
                        var value = this.valueAsNumber;
                        if(value < min)
                            value = min;
                        else if(value > max)
                            value = max;
                        $(this).val(value.toFixed(3));
                    });*/
                });
            };
        })(jQuery);
    },
    handleEditor : function () {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5();
        }
    }

};
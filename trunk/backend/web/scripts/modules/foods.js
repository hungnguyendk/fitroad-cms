/************************************************
 * User: sangnguyen on  5/2/16 at 09:45         *
 * File name:                       *
 * Project name: Fit-Road                *
 * Copyright (c) 2016 by Company                 *
 * All rights reserved                          *
 ************************************************
 */

FoodsModule = function(){
    this.borderColor = ['green','red','blue','purple','yellow'];
    this.ajaxFindRestaurantUrl = BASE_URL + '/fitroad-cms/trunk/restaurant/manage/ajax-find-restaurant';
    //this.ajaxFindRestaurantUrl = BASE_URL +  '/restaurant/manage/ajax-find-restaurant';

    this.ajaxSearchRestaurantUrl = BASE_URL +  '/fitroad-cms/trunk/restaurant/manage/ajax-search-restaurant-select-box';
    //this.ajaxSearchRestaurantUrl = BASE_URL +  '/restaurant/manage/ajax-search-restaurant-select-box';
};

FoodsModule.prototype = {
    constructor: FoodsModule,

    initRestaurantDropDownListToAssigned: function (resIds){
        var self = this;
        jQuery('#signupform-restaurant_id').select2({
            placeholder: "Search restaurant",
            allowClear: true,
            tags: true,
            closeOnSelect: false,
            formatResult: formatResult,
            formatSelection: formatSelection,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: self.ajaxSearchRestaurantUrl,
                type: 'post',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        _csrf: yii.getCsrfToken(),
                        query:term,
                        id:''
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    //return {
                    //    results: data.movies
                    //};

                    if(data.status){
                        return {
                            results: data.restaurants
                        };
                    }
                }
            },
            initSelection: function (element, callback) {
                callback(resIds);
            }
        })
    },

    handleTrackingImageChange: function (){
        var arrayImage =[];

        jQuery('.image-food-input').on('change',function(){
            var path = jQuery(this).val(),
                imageChange = {},
                key = jQuery(this).attr('id');

            if(path != ''){

                imageChange.id = key;
                imageChange.name = getName(path);

                arrayImage.push(imageChange);

            }else{

                arrayImage  = $.grep(arrayImage, function(e){
                    console.log(e.id);
                    return e.id != key;
                });

            }
            jQuery('#imageChanged').val(JSON.stringify(arrayImage));

        });
    },
    initRestaurantDropDownListUpdatePage : function(defaultData){
        var self = this;
        jQuery('#fitroadfood-restaurant_id').select2({
            allowClear: true,
            formatResult: formatResult,
            formatSelection: formatSelection,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: self.ajaxSearchRestaurantUrl,
                type: 'post',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        _csrf: yii.getCsrfToken(),
                        query:term,
                        id:''
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    //return {
                    //    results: data.movies
                    //};

                    if(data.status){
                        return {
                            results: data.restaurants
                        };
                    }
                }
            },
            initSelection: function (element, callback) {
                callback(defaultData);

                //var id = $(element).val();
                //if (id !== "") {
                //    $.ajax(self.ajaxSearchRestaurantUrl, {
                //        data: {
                //            _csrf: yii.getCsrfToken(),
                //            id:id,
                //            query:''
                //        },
                //        type: 'post',
                //        dataType: "json"
                //    }).done(function (data) {
                //        callback(data);
                //    });
                //}
            }
        })
            .on("change", function(e) {
            jQuery.ajax({
                url: self.ajaxFindRestaurantUrl,
                type: 'post',
                dataType : 'json',
                accepts: "application/json",
                data: {id:e.val,_csrf:yii.getCsrfToken()},
                success: function (response) {
                    if(response.status){
                        jQuery('#restaurant_address').html(' <li>Address : '+response.data.address+'</li>');
                    }
                }
            });

        });
     //  $('#fitroadfood-restaurant_id').select2('val',[8492], true);
        jQuery('.select2-input').attr('placeholder','Search restaurants');
        //jQuery('#fitroadfood-restaurant_id').val(["8492","AMart"]).trigger("change");
    },

        initRestaurantDropDownList: function (){
            var self = this;
        jQuery('#fitroadfood-restaurant_id').select2({
            placeholder: "Search restaurant",
            allowClear: true,
            tags: true,
            formatResult: formatResult,
            closeOnSelect: false,
            formatSelection: formatSelection,
            escapeMarkup: function (markup) { return markup; },
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: self.ajaxSearchRestaurantUrl,
                type: 'post',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        _csrf: yii.getCsrfToken(),
                        query:term,
                        id:''
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    //return {
                    //    results: data.movies
                    //};

                   if(data.status){
                       return {
                           results: data.restaurants
                       };
                   }
                }
            },
        }).on("change", function(e) {
            self.appendToRestaurantSelectedBox(e);

        });
    },
    appendToRestaurantSelectedBox: function (e) {
        var self = this;
        jQuery.ajax({
            url: self.ajaxFindRestaurantUrl,
            type: 'post',
            dataType: 'json',
            accepts: "application/json",
            data: {id: e.val, _csrf: yii.getCsrfToken()},
            success: function (response) {
                if (response.status) {
                    var html = '';
                    if (response.data.length > 0) {

                        for (var i = 0; i < response.data.length; i++) {
                            var color;
                            switch (i) {
                                case 0 :
                                case 5 :
                                case 10 :
                                    color = 'green';
                                    break;
                                case 1 :
                                case 6 :
                                case 11 :
                                    color = 'red';
                                    break;
                                case 2 :
                                case 7 :
                                case 12 :
                                    color = 'blue';
                                    break;
                                case 3 :
                                case 8:
                                case 13 :
                                    color = 'purple';
                                    break;
                                case 4 :
                                case 9 :
                                case 14 :
                                    color = 'yellow';
                                    break;

                                default :
                                    color = items[Math.floor(Math.random() * items.length)];

                            }

                            html += '<div class="todo-tasklist-item todo-tasklist-item-border-' + color + '">' +
                                '<img class="todo-userpic pull-left" src="' + response.data[i].icon + '" width="57px" height="57px">' +
                                '<div class="todo-tasklist-item-title">' + response.data[i].name +
                                '</div>' +
                                '<div class="todo-tasklist-item-text"><i class="fa fa-map-marker"></i> ' + response.data[i].address +
                                '</div>' +

                                '</div>';
                        }
                    }

                    jQuery('#logList').html(html);

                    var chatdiv = $('#boxLogs')
                    var textdiv = $("#logList");

                    console.log(textdiv.height());

                    chatdiv.animate({scrollTop: textdiv.outerHeight()}, "slow");
                } else {
                    jQuery('#logList').html('');
                }
            }
        });
    }

};



function formatResult(item,element,match) {
    if(!item.id) {
        // return `text` for optgroup
        return item.text;
    }
    //console.log(match);
    $(element).attr('title',item.address);

    var name = item.text;
    var re = new RegExp('(' + match.term.trim().split(/\s+/).join('|') + ')', "gi");

    name = name.replace( re, '<strong style="font-size:16px">$1</strong>');

    return '<div><i class="fa fa-cutlery"></i>&nbsp;'+name
        +'<br/><i class="fa fa-location-arrow"></i>&nbsp;'+item.address+'</div>';

};

function formatSelection(item) {
    // return selection template
    return '<b>' + item.text + '</b>';
};
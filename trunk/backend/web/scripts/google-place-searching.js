/************************************************
 * User: sangnguyen on  4/18/16 at 13:35         *
 * File name:                       *
 * Project name: Fit-Road                *
 * Copyright (c) 2016 by MyCompany                 *
 * All rights reserved                          *
 ************************************************
 */
// This example uses the autocomplete feature of the Google Places API.
// It allows the user to find all hotels in a given place, within a given
// country. It then displays markers for all the hotels returned,
// with on-click details for each hotel.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">




var map, places, infoWindow;
var markers = [];
var autocomplete;
var countryRestrict = {'country': 'ca'};
var MARKER_PATH = 'https://maps.gstatic.com/intl/en_us/mapfiles/marker_green';
var hostnameRegexp = new RegExp('^https?://.+?/');
var placeGoogleData = [];
var placeIdFilter = null;
var countries = {
    'au': {
        center: {lat: -25.3, lng: 133.8},
        zoom: 4
    },
    'br': {
        center: {lat: -14.2, lng: -51.9},
        zoom: 3
    },
    'ca': {
        center: {lat: 62, lng: -110.0},
        zoom: 3
    },
    'fr': {
        center: {lat: 46.2, lng: 2.2},
        zoom: 5
    },
    'de': {
        center: {lat: 51.2, lng: 10.4},
        zoom: 5
    },
    'mx': {
        center: {lat: 23.6, lng: -102.5},
        zoom: 4
    },
    'nz': {
        center: {lat: -40.9, lng: 174.9},
        zoom: 5
    },
    'it': {
        center: {lat: 41.9, lng: 12.6},
        zoom: 5
    },
    'za': {
        center: {lat: -30.6, lng: 22.9},
        zoom: 5
    },
    'es': {
        center: {lat: 40.5, lng: -3.7},
        zoom: 5
    },
    'pt': {
        center: {lat: 39.4, lng: -8.2},
        zoom: 6
    },
    'us': {
        center: {lat: 37.1, lng: -95.7},
        zoom: 3
    },
    'uk': {
        center: {lat: 54.8, lng: -4.6},
        zoom: 5
    },
    'vn':{
        center: {lat:10.762622, lng:106.660172},
        zoom:11
    }
};

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: countries['ca'].zoom,
        center: countries['ca'].center,
        mapTypeControl: false,
        panControl: false,
        zoomControl: false,
        streetViewControl: false
    });

    infoWindow = new google.maps.InfoWindow({
        content: document.getElementById('info-content')
    });

    // Create the autocomplete object and associate it with the UI input control.
    // Restrict the search to the default country, and to place type "cities".
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */ (
            document.getElementById('address')), {
            //types: ['establishment'],
            componentRestrictions: countryRestrict
        });
    autocomplete.bindTo('bounds', map);
    places = new google.maps.places.PlacesService(map);

    autocomplete.addListener('place_changed', onPlaceChanged);

    // Add a DOM event listener to react when the user selects a country.
    document.getElementById('country').addEventListener(
        'change', setAutocompleteCountry);
}

// When the user selects a city, get the place details for the city and
// zoom the map in on the city.
function onPlaceChanged() {
    var place = autocomplete.getPlace();
    if (place.geometry) {
        map.panTo(place.geometry.location);
        map.setZoom(17);
        search();
    } else {
        document.getElementById('address').placeholder = 'Enter a city';
    }
}

// Search for hotels in the selected city, within the viewport of the map.
function search() {
    var search = {
        bounds: map.getBounds(),
        types: ['restaurant']
    };

    places.nearbySearch(search, function(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            clearResults();
            clearMarkers();
            // Create a marker for each hotel found, and
            // assign a letter of the alphabetic to each marker icon.

            placeGoogleData = results;

            for (var i = 0; i < results.length; i++) {
                var markerLetter = String.fromCharCode('A'.charCodeAt(0) + i);
                var markerIcon = MARKER_PATH + markerLetter + '.png';
                // Use marker animation to drop the icons incrementally on the map.
                markers[i] = new google.maps.Marker({
                    position: results[i].geometry.location,
                    animation: google.maps.Animation.DROP,
                    icon: markerIcon
                });
                // If the user clicks a hotel marker, show the details of that hotel
                // in an info window.
                markers[i].placeResult = results[i];
                google.maps.event.addListener(markers[i], 'click', showInfoWindow);
                setTimeout(dropMarker(i), i * 100);
                addResult(results[i], i);

            }
        }
    });
}

function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
        if (markers[i]) {
            markers[i].setMap(null);
        }
    }
    markers = [];
}

// Set the country restriction based on user input.
// Also center and zoom the map on the given country.
function setAutocompleteCountry() {
    var country = document.getElementById('country').value;
    if (country == 'all') {
        autocomplete.setComponentRestrictions([]);
        map.setCenter({lat: 15, lng: 0});
        map.setZoom(2);
    } else {
        autocomplete.setComponentRestrictions({'country': country});
        map.setCenter(countries[country].center);
        map.setZoom(countries[country].zoom);
    }
    clearResults();
    clearMarkers();
}

function dropMarker(i) {
    return function() {
        markers[i].setMap(map);
    };
}

function addResult(result, i) {
    var results = document.getElementById('results');
    var markerLetter = String.fromCharCode('A'.charCodeAt(0) + i);
    var markerIcon = MARKER_PATH + markerLetter + '.png';

    var tr = document.createElement('tr');
    tr.style.backgroundColor = (i % 2 === 0 ? '#F0F0F0' : '#FFFFFF');


    var iconTd = document.createElement('td');
    var nameTd = document.createElement('td');
    var actionTd = document.createElement('td');
    var icon = document.createElement('img');
    var btnAdd = document.createElement('a');
    icon.src = markerIcon;
    icon.setAttribute('class', 'placeIcon');
    icon.setAttribute('className', 'placeIcon');

    var name = document.createTextNode(result.name);
    iconTd.appendChild(icon);
    nameTd.appendChild(name);
    tr.appendChild(iconTd);
    tr.appendChild(nameTd);
    //actionTd.appendChild('<a class="place-add-action" href="javascript:;" data-place-id="'+result.place_id+'">Insert into system</a>');


    var btnInsert = document.createElement('a');
    var linkText = document.createTextNode("Insert into system");
    btnInsert.appendChild(linkText);
    btnInsert.title = "Insert into system";
    btnInsert.href = "javascript:;";

    btnInsert.setAttribute('data-place-id', result.place_id);
    btnInsert.setAttribute('onClick', 'OpenDialog(jQuery(this));');
    btnInsert.setAttribute('class', 'btn-insert-place');
    btnInsert.setAttribute('data-toggle', 'modal');
    btnInsert.setAttribute('href','#responsive');

    tr.appendChild(btnInsert);

    iconTd.onclick = function() {
        google.maps.event.trigger(markers[i], 'click');
    };
    nameTd.onclick = function() {
        google.maps.event.trigger(markers[i], 'click');
    };

    results.appendChild(tr);

}

function clearResults() {
    var results = document.getElementById('results');
    while (results.childNodes[0]) {
        results.removeChild(results.childNodes[0]);
    }
}

// Get the place details for a hotel. Show the information in an info window,
// anchored on the marker for the hotel that the user selected.
function showInfoWindow() {
    var marker = this;
    places.getDetails({placeId: marker.placeResult.place_id},
        function(place, status) {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                return;
            }
            infoWindow.open(map, marker);
            buildIWContent(place);
        });
}

// Load the place information into the HTML elements used by the info window.
function buildIWContent(place) {
    document.getElementById('iw-icon').innerHTML = '<img class="hotelIcon" ' +
        'src="' + place.icon + '"/>';
    document.getElementById('iw-url').innerHTML = '<b><a href="' + place.url +
        '">' + place.name + '</a></b>';
    document.getElementById('iw-address').textContent = place.vicinity;

    if (place.formatted_phone_number) {
        document.getElementById('iw-phone-row').style.display = '';
        document.getElementById('iw-phone').textContent =
            place.formatted_phone_number;
    } else {
        document.getElementById('iw-phone-row').style.display = 'none';
    }

    // Assign a five-star rating to the hotel, using a black star ('&#10029;')
    // to indicate the rating the hotel has earned, and a white star ('&#10025;')
    // for the rating points not achieved.
    if (place.rating) {
        var ratingHtml = '';
        for (var i = 0; i < 5; i++) {
            if (place.rating < (i + 0.5)) {
                ratingHtml += '&#10025;';
            } else {
                ratingHtml += '&#10029;';
            }
            document.getElementById('iw-rating-row').style.display = '';
            document.getElementById('iw-rating').innerHTML = ratingHtml;
        }
    } else {
        document.getElementById('iw-rating-row').style.display = 'none';
    }

    // The regexp isolates the first part of the URL (domain plus subdomain)
    // to give a short URL for displaying in the info window.
    if (place.website) {
        var fullUrl = place.website;
        var website = hostnameRegexp.exec(place.website);
        if (website === null) {
            website = 'http://' + place.website + '/';
            fullUrl = website;
        }
        document.getElementById('iw-website-row').style.display = '';
        document.getElementById('iw-website').textContent = website;
    } else {
        document.getElementById('iw-website-row').style.display = 'none';
    }
}

function OpenDialog($this){
        placeIdFilter = $($this).data('place-id');
    var restaurant = null;

    for (var i = 0; i < placeGoogleData.length; i++) {
       console.log(placeGoogleData[i]);
        if(placeIdFilter == placeGoogleData[i].place_id){
            restaurant = placeGoogleData[i];
            break;
        }
    }


    var html = '<div class="row">'+
        '<div class="col-md-7 col-sm-12">'+
        '<aside class="agent-info clearfix">'+
        '<div class="agent-contact-info">'+
        '<h3>'+restaurant.name+'</h3>'+
        '<dl>'+
        '<dt>Address: </dt>'+
        '<dd>'+restaurant.vicinity+'</dd>'+
        '</dl>'+
        '</div>'+
        '</aside>'+
        '</div>'+
        '</div>';

    $('#confirmRestaurantData .modal-body').html(html);
    $('#confirmRestaurantData').modal('show', {backdrop: 'static'});
}

$('#modal-confirmed').on('click',function(){

    Metronic.blockUI({
        target: '#confirmRestaurantData',
        boxed: true,
        message: 'Processing...'
    });

    jQuery.ajax({
        url: BASE_URL + '/fitroad-cms/trunk/restaurant/manage/ajax-insert-restaurant',
        //url: BASE_URL + '/restaurant/manage/ajax-insert-restaurant',
        type: 'post',
        dataType : 'json',
        accepts: "application/json",
        data: {placeID:placeIdFilter,_csrf:yii.getCsrfToken()},
        success: function (response) {
            if(response.status){
                Metronic.unblockUI('#confirmRestaurantData');
                $('#confirmRestaurantData').modal('hide');
                Metronic.alert({
                    container: '#alert-message', // alerts parent container(by default placed after the page breadcrumbs)
                    place: 'append', // append or prepent in container
                    type: 'success',  // alert's type
                    message: response.message,  // alert's message
                    close: true, // make alert closable
                    reset: true, // close all previouse alerts first
                    focus: true, // auto scroll to the alert after shown
                    closeInSeconds: 5, // auto close after defined seconds
                    icon: 'check' // put icon before the message
                });
            }else{
                Metronic.unblockUI('#confirmRestaurantData');
                $('#confirmRestaurantData').modal('hide');
                Metronic.alert({
                    container: '#alert-message', // alerts parent container(by default placed after the page breadcrumbs)
                    place: 'append', // append or prepent in container
                    type: 'warning',  // alert's type
                    message: response.message,  // alert's message
                    close: true, // make alert closable
                    reset: true, // close all previouse alerts first
                    focus: true, // auto scroll to the alert after shown
                    closeInSeconds: 5, // auto close after defined seconds
                    icon: 'warning' // put icon before the message
                });

            }
        }
    });
});

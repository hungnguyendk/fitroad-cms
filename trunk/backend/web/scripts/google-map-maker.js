/************************************************
 * User: sangnguyen on  4/30/16 at 23:05         *
 * File name:                       *
 * Project name: Fit-Road                *
 * Copyright (c) 2016 by COMPANY                 *
 * All rights reserved                          *
 ************************************************
 */
function initMap() {
    var myLatLng = {lat: lat_, lng: lng_};

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: myLatLng
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!'
    });
}
$(document).ready(function(){
    	$(".multi-select").multiSelect({
			selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search new restaurants '>",
			selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search restaurants added'>",
			selectionFooter: "<div class='custom-header'>The restaurant has been assigned</div>",
			selectableFooter: "<div class='custom-header'>Restaurants list</div>",
			afterInit: function(ms){
				var that = this,
					$selectableSearch = that.$selectableUl.prev(),
					$selectionSearch = that.$selectionUl.prev(),
					selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
					selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
				console.log(that);

				that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
					.on('keydown', function(e){
						if (e.which === 40){
							that.$selectableUl.focus();
							return false;
						}
					});

				that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
					.on('keydown', function(e){
						if (e.which == 40){
							that.$selectionUl.focus();
							return false;
						}
					});
			},
			afterSelect: function(){
				this.qs1.cache();
				this.qs2.cache();
			},
			afterDeselect: function(){
				this.qs1.cache();
				this.qs2.cache();
			}
		});
    	$(".classnumber").keypress(function (e) {
	      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	          return false;
	      }
	     });
	     $("#perpage").change(function(){
	     	//alert(123);
	     	$("#formIndex").submit();
	     });
    });
    
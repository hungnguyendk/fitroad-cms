<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace backend\assets;

use yii\web\AssetBundle;

/**
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    // public $basePath = '@webroot';
    // public $siteUrl = '@web';
    public $sourcePath = '@bower/backend/';

    public $css = [
        // <!-- BEGIN GLOBAL MANDATORY STYLES -->
        'theme/assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'theme/assets/global/plugins/bootstrap/css/bootstrap.min.css',
        'theme/assets/global/plugins/uniform/css/uniform.default.css',
        'theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'theme/assets/global/plugins/select2/select2.css',
        // <!-- BEGIN THEME STYLES -->
        'theme/assets/admin/pages/css/profile.css',
        'theme/assets/admin/pages/css/tasks.css',
        'theme/assets/global/css/components.css',
        'theme/assets/global/css/plugins.css',
        'theme/assets/admin/layout/css/layout.css',
        'theme/assets/admin/pages/css/todo.css',
        'theme/assets/admin/layout/css/themes/darkblue.css',
        'theme/assets/admin/layout/css/custom.css',
        'theme/assets/global/plugins/jquery-multi-select/css/multi-select.css',
        'theme/assets/global/plugins/bootstrap-fileupload/bootstrap-fileupload.css',
    ];

    public $js = [
        'theme/assets/global/plugins/respond.min.js',
        'theme/assets/global/plugins/excanvas.min.js',
        'theme/assets/global/plugins/jquery.min.js',
        'theme/assets/global/plugins/jquery-migrate.min.js',
        // IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip
        'theme/assets/global/plugins/jquery-ui/jquery-ui.min.js',
        'theme/assets/global/plugins/bootstrap/js/bootstrap.min.js',
        'theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
        'theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'theme/assets/global/plugins/jquery.blockui.min.js',
        'theme/assets/global/plugins/jquery.cokie.min.js',
        'theme/assets/global/plugins/uniform/jquery.uniform.min.js',
        'theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'theme/assets/global/plugins/select2/select2.min.js',
        
        'theme/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js',
        'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js',
        'theme/assets/global/plugins/jquery.pulsate.min.js',
        'theme/assets/global/scripts/metronic.js',
        'theme/assets/admin/layout/scripts/layout.js',
        'theme/assets/admin/layout/scripts/quick-sidebar.js',
    	'theme/assets/global/plugins/bootbox/bootbox.min.js',
    	'theme/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js',
		'theme/assets/global/plugins/bootstrap-fileupload/bootstrap-fileupload.js',
		'theme/assets/global/plugins/flot/jquery.flot.min.js',
		'theme/assets/global/plugins/flot/jquery.flot.resize.min.js',
		'theme/assets/global/plugins/flot/jquery.flot.pie.min.js',
		'theme/assets/global/plugins/flot/jquery.flot.stack.min.js',
		'theme/assets/global/plugins/flot/jquery.flot.crosshair.min.js',
		'theme/assets/global/plugins/flot/jquery.flot.categories.min.js',
		
		'theme/assets/global/plugins/amcharts/amcharts/amcharts.js',
		'theme/assets/global/plugins/amcharts/amcharts/serial.js',
		'theme/assets/global/plugins/amcharts/amcharts/pie.js',
		'theme/assets/global/plugins/amcharts/amcharts/radar.js',
		'theme/assets/global/plugins/amcharts/amcharts/themes/light.js',
		'theme/assets/global/plugins/amcharts/amcharts/themes/patterns.js',
		'theme/assets/global/plugins/amcharts/amcharts/themes/chalk.js',
		'theme/assets/global/plugins/amcharts/ammap/ammap.js',
		'theme/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js',
		'theme/assets/global/plugins/amcharts/amstockcharts/amstock.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
//         'yii\bootstrap\BootstrapAsset'
    ];
}

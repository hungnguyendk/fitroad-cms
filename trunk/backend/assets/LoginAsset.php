<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace backend\assets;

use yii\web\AssetBundle;

/**
 *
 * @author SangNguyen <sangtom382@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    // public $basePath = '@webroot';
//     public $siteUrl = '@web';
    public $sourcePath = '@bower/backend/';

    public $css = [
        // <!-- BEGIN GLOBAL MANDATORY STYLES -->
        'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
        'theme/assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'theme/assets/global/plugins/bootstrap/css/bootstrap.min.css',
        'theme/assets/global/plugins/uniform/css/uniform.default.css',
        // <!-- BEGIN PAGE LEVEL STYLES -->
        'theme/assets/global/plugins/select2/select2.css',
        'theme/assets/admin/pages/css/login3.css',
        // <!-- BEGIN PAGE LEVEL STYLES -->
        'theme/assets/global/css/components.css',
        'theme/assets/global/css/plugins.css',
        'theme/assets/admin/layout/css/layout.css',
        'theme/assets/admin/layout/css/themes/light.css',
        'theme/assets/admin/layout/css/custom.css',
        'theme/assets/admin/cyclone.css',
    ];

    public $js = [
        // <!-- BEGIN CORE PLUGINS -->
        'theme/assets/global/plugins/respond.min.js',
        'theme/assets/global/plugins/excanvas.min.js',
//         'theme/assets/global/plugins/jquery.min.js',
        'theme/assets/global/plugins/jquery-migrate.min.js',
        'theme/assets/global/plugins/bootstrap/js/bootstrap.min.js',
        'theme/assets/global/plugins/jquery.blockui.min.js',
        'theme/assets/global/plugins/uniform/jquery.uniform.min.js',
        'theme/assets/global/plugins/jquery.cokie.min.js',
        // <!-- END CORE PLUGINS -->
        // <!-- BEGIN PAGE LEVEL PLUGINS -->
        'theme/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
        'theme/assets/global/plugins/select2/select2.min.js',
//         <!-- END PAGE LEVEL PLUGINS -->
//         <!-- BEGIN PAGE LEVEL SCRIPTS -->
        'theme/assets/global/scripts/metronic.js',
        'theme/assets/admin/layout/scripts/layout.js',
        //'/scripts/site/MyLogin.js',
    ];

    public $depends = [
//         'yii\web\YiiAsset',
//         'yii\bootstrap\BootstrapAsset'
    ];
    
    public $publishOptions = [
//         'forceCopy'=>true,
    ];
}

<?php
/**
 * User: sangnguyen on  5/11/16 at 23:07
 * File name: push-notifications.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;

use api\commons\helpers\ApiHelpers;

$this->title = Yii::t('backend', 'Push notifications');

$this->params['breadcrumbs'][] = $this->title;
$asset		= backend\assets\AppAsset::register($this);
?>
<div class="row">
    <?php $form = ActiveForm::begin(['id'=>'updateForm',
        'layout' => 'horizontal',
        'options' => ['enctype'=>'multipart/form-data'],
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8 marginBot',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div class="col-md-12">
        <div class="portlet">

            <div class="portlet-title">
                <div class="caption" style="font-size: 18pt">

                </div>

                <div class="actions pull-right">
                    <button type="submit" id="savebtn" class="btn green">
                        Send
                    </button>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-11"><!-- BEGIN Portlet PORTLET-->
                <div class="portlet">
                    <?php echo(isset($alert) && !empty($alert))?$alert:''; ?>
                    <div class="portlet-body from">
                        <?= $form->field($model, 'message',['template'=>"{label}\n<div class='col-md-8'>{input}\n{hint}\n{error}</div>"])
                            ->label(Yii::t('backend', 'Message'),['class'=>'control-label col-md-2'])
                            ->textInput(['placeholder'=>Yii::t('backend', 'Message'),'maxlength' => true])?>
                    </div>
            </div>
        </div>
    </div>
        <?php ActiveForm::end(); ?>
</div>

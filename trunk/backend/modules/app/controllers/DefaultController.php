<?php

namespace backend\modules\app\controllers;

use Yii;
use backend\commons\forms\PushNotificationsForm;
use yii\web\Controller;

/**
 * Default controller for the `app` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionPushNotifications()
    {

        $form = new PushNotificationsForm();
        $alert = null;
        $statusAlertClass = null;
        $alertMessage = null;
        if ($form->load(Yii::$app->request->post())){
            if($form->push()){
                $statusAlertClass = \Yii::$app->params['alertSuccessStatusClass'];
                $alertMessage =  'Notifications sent successfully';

            }else{
                $statusAlertClass = \Yii::$app->params['alertErrorStatusClass'];
                $alertMessage =  'Notifications sent failed';
            }
        }

        if($statusAlertClass !== null && $alertMessage !== null){
            $alert =  \yii\bootstrap\Alert::widget([
                'options' => [
                    'class' => 'alert '.$statusAlertClass,
                ],
                'body' =>$alertMessage,
            ]);
        }

        return $this->render('push-notifications',[
            'model'=>$form,
            'alert'=>$alert
        ]);
    }
}

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\commons\models\entities\FitRoadUser */

$this->title = Yii::t('backend', 'Create User');
\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
    'urlLevel1'=>['index'],
    'titleLevel1'=>Yii::t('backend', 'User Management')
]);
$asset		= backend\assets\AppAsset::register($this);
?>
<div class="fit-road-user-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

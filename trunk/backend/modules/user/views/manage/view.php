<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\commons\models\entities\FitRoadUser */

$this->title = Yii::t('backend', 'User Management').' View: '.trim($model->first_name .' '.$model->last_name);
\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
    'urlLevel1'=>['index'],
    'titleLevel1'=>Yii::t('backend', 'User Management'),
    'objectName'=>trim($model->first_name .' '.$model->last_name)
]);
?>
<div class="fit-road-user-view">

    <h1><?= trim($model->first_name .' '.$model->last_name) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            //'password',
            'email:email',
            'height',
            'weight',
            [
                'label'  => 'Gender',
                'value'  =>  ($model->gender ==1)?\Yii::t('backend','Male'):\Yii::t('backend','Female')
            ],
            'age',
            'phone_number',
            //'alias',
           // 'point',
           // 'password_reset_token',
            //'time_zone',

            [
                'label'  => 'Role',
                'value'  => \backend\commons\helpers\UtilHelper::getRoleName($model->role)
            ],
            //'status',

            [
                'label'  => 'Created At',
                'value'  =>  \Yii::$app->formatter->asDate($model->created_at,'php:D d M Y')
            ],
            [
                'label'  => 'Updated At',
                'value'  =>  \Yii::$app->formatter->asDate($model->updated_at,'php:D d M Y')
            ]
        ],
    ]) ?>

</div>

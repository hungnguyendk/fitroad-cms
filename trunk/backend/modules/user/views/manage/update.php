<?php

use yii\helpers\Html;
use yii\helpers\BaseHtml;
use yii\bootstrap\Alert;
/* @var $this yii\web\View */
/* @var $model backend\commons\models\entities\FitRoadUser */
$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => \Yii::t('backend','User'),
    ]) . ' ' . trim($model->first_name.' '.$model->last_name);

\backend\commons\helpers\UtilHelper::builtBreadcrumb(3,$this,[
    'titleLevel1'=>Yii::t('backend', 'User Management'),
    'titleLevel2'=>Yii::t('backend','Update'),
    'urlLevel1'=>['index'],
    'urlLevel2'=>['view','id'=>$model->id],
    'objectName'=>trim($model->first_name.' '.$model->last_name)
]);

if(isset($uploadFile) && is_array($uploadFile->getErrors()) && !empty($uploadFile->getErrors())){
    $alert =  Alert::widget([
        'options' => [
            'class' => 'alert '.\Yii::$app->params['alertErrorStatusClass'],
        ],
        'body' => BaseHtml::errorSummary($uploadFile),
    ]);
}
$jsScripts = <<<JS
$('select').select2();
JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
?>
<div class="fit-road-user-update">
    <!-- BEGIN NEW USER FORM -->
    <div class="profile-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet light">
                    <?php echo(isset($alert) && !empty($alert))?$alert:''; ?>
                    <!-- BEGIN TABBABLE LINE -->
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_1" data-toggle="tab" aria-expanded="true">Personal Info</a>
                            </li>
                            <li class="">
                                <a href="#tab_1_3" data-toggle="tab" aria-expanded="false">Change Password</a>
                            </li>
                            <?php
                                echo(Yii::$app->user->can('user'))?
                                    '<li class="">
                                <a href="#tab_1_2" data-toggle="tab" aria-expanded="false">Access Restaurants</a>
                            </li>':FALSE;
                            ?>
                        </ul>
                    </div>
                    <!-- END TABBABLE LINE -->
                    <?= $this->render('_account_settings', [
                        'model' => $model,
                        'resIds' => $resIds,
                        'avatar'=> $avatar
                    ]) ?>
                </div>
            </div>
        </div>
        <!-- END NEW USER FORM -->
</div>

<?php
/**
 * User: sangnguyen on  10/21/15 at 19:45
 * File name: _account_settings.php
 * Project name: ysd-tee-shirt
 * Copyright (c) 2015 by YSD
 * All rights reserved
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\commons\helpers\UtilHelper;
$asset	= backend\assets\AppAsset::register($this);
$roles  = \backend\modules\authorization\models\entity\AuthItem::find()
    ->andOnCondition(['type'=>1])
    ->andOnCondition(['not in','name',['sysadmin']])->all();
unset($roles['sysadmin']);
if(!\Yii::$app->user->can('authorization')){
    unset($roles['admin']);
}
$arrRoleManage = \yii\helpers\ArrayHelper::map($roles,'role_id','name');
/*echo '<pre>';
print_r($arrRoleManage);
echo '</pre>';
die();*/
$this->registerJsFile(Yii::$app->homeUrl.'scripts/jquery.quicksearch.js',[ 'depends' => 'yii\web\YiiAsset']);
$this->registerJsFile(Yii::$app->homeUrl.'scripts/modules/foods.js',[ 'depends' => 'yii\web\YiiAsset']);
//$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
$jsScripts = '';
$jsScripts .= 'var resIds;';
if($resIds && !empty($resIds)){
    $jsScripts .= 'resIds = '.$resIds.';';
}
$jsScripts .= <<<JS
	var Foods = new FoodsModule();
	Foods.initRestaurantDropDownListToAssigned(resIds);
JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);


?>
<style>

    .custom-header{
        text-align: center;
        padding: 3px;
        background: #4F8498;
        color: #fff;

    }
</style>

<div class="portlet-body">
    <div class="tab-content">
        <!-- PERSONAL INFO TAB -->
        <div class="tab-pane active" id="tab_1_1">
            <?php $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'options' => ['enctype'=>'multipart/form-data'],
                'successCssClass'=>'has-success has-feedback',
                'errorCssClass' => 'has-error has-feedback',
                'fieldConfig'=>[
                ]
            ]);?>

            <div class="form-group <?php echo isset($error_upload) && $error_upload ? 'has-error' : '';?>">
                <label class="control-label col-md-2">Avatar<br/>
                    <?php echo isset($error_message) && $error_message ? "<br/>".$error_message : '';?>
                </label>
                <div class="col-md-8">
                    <?php
                    $df_image = Yii::$app->params['urlAssetsImagesUploaded'] . '../../assets/df_i/default_avatar_undefined.png';
					if(isset($avatar->url)){
						$image = $avatar->url;
					}else{
						$image = false;
					}
                    for($i=0; $i<1;$i++){
                        ?>
                        <div class="col-lg-6">
                            <input accept="image/*"  type="hidden" name="avatar_file" value="<?php echo $avatar?$avatar->url:null;?>"/>
                            <div data-provides="fileupload" class="fileupload fileupload-new">
                                <div style="width: 100px;" class="fileupload-new thumbnail">
                                    <img src="<?php echo $image ?  \api\commons\helpers\ApiHelpers::builtUrlImages(Yii::$app->params['avatarsFinder'],$image,Yii::$app->params['normalName']) : $df_image;?>" alt="" />
                                </div>
                                <div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
                                <div>
                                   <span class="btn btn-white btn-file">
                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select Image</span>
                                   <span class="fileupload-exists"><i class="fa fa-undo"></i>Change Image</span>
                                    <input accept="image/*"  type="file" name="avatar_file" class="default" /></span>
                                    </span>
                                    <a data-dismiss="fileupload" class="btn btn-danger <?php echo  $image ? $image :  'fileupload-exists';?>" <?php echo  $image ? 'onclick="checkDelete(\''.$image.'\', \''.$avatar->id.'\', \''.$i.'\')"' : '';?> href="#"><i class="fa fa-trash"></i>Delete Image</a>
                                </div>

                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

                <?= $form->field($model, 'role',['template'=>"{label}\n<div class='col-md-9'>\n{input}\n{hint}\n{error}</div>"])
                ->label(Yii::t('backend', 'Role'),['class'=>'control-label col-md-2'])
                ->dropDownList(
                    $arrRoleManage,
                    [
                        'class'=>['form-control select2me'],
                        'options'=>[
                            $model->role => ['selected' => true]
                        ]
                    ]
                );?>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <?= $form->field($model, 'first_name',['template'=>"{label}\n<div class='col-md-8'>{input}\n{hint}\n{error}</div>"])
                                ->label(Yii::t('backend', 'Information'),['class'=>'control-label col-md-4'])
                                ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'First Name')]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'last_name',['template'=>"<div class='col-md-10'>{input}\n{hint}\n{error}</div>"])
                                ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'last Name')]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= $form->field($model, 'email',['template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"])
                ->label(Yii::t('backend', 'Email'),['class'=>'control-label col-md-2'])
                ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'Email')]) ?>

            <?= $form->field($model, 'phone_number',[
                'template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"
            ]) ->label(Yii::t('backend', 'Phone'),['class'=>'control-label col-md-2'])
                ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'Phone Number')]) ?>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <?= $form->field($model, 'weight',['template'=>"{label}\n<div class='col-md-6'>{input}\n{hint}\n{error}</div>"])
                                ->label(Yii::t('backend', 'Physical'),['class'=>'control-label col-md-4'])
                                ->textInput(['type'=>'text','maxlength' => true,'placeholder'=>Yii::t('backend', 'Weight')]) ?>
                        </div>
                        <div class="col-md-3" style="margin-left: -57px;">
                            <?= $form->field($model, 'height',['template'=>"<div class='col-md-12'>{input}\n{hint}\n{error}</div>"])
                                ->textInput(['type'=>'text','maxlength' => true,'placeholder'=>Yii::t('backend', 'Height')]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'age',['template'=>"<div class='col-md-12'>{input}\n{hint}\n{error}</div>"])
                                ->textInput(['type'=>'age','maxlength' => true,'placeholder'=>Yii::t('backend', 'Age')]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= $form->field($model, 'gender',
                ['template'=>"{label}\n<div class='col-md-7'>{input}\n{hint}\n{error}</div>"])
                ->dropDownList(['1'=>'Male','0'=>'Female'])->label(Yii::t('backend','Gender'),
                ['class'=>'control-label col-md-2']) ?>

                <div class="margiv-top-10">
                    <?= Html::submitButton(
                        $model->isNewRecord ?'<i class="fa fa-check"></i> '.Yii::t('backend', 'Submit'):'<i class="fa fa-check"></i> '.Yii::t('backend', 'Update'),
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
        </div>
        <!-- END PERSONAL INFO TAB -->
        <!-- CHANGE PASSWORD TAB -->
        <div class="tab-pane" id="tab_1_3">
            <?= $form->field($model, 'password',['template'=>"{label}\n<div class='col-md-8'>{input}\n{hint}\n{error}</div>"])
                ->label(Yii::t('backend', 'New Password'),['class'=>'control-label col-md-4'])
                ->passwordInput(['placeholder'=>Yii::t('backend', 'New Password'),'maxlength' => true])?>

            <?= $form->field($model, 'password_repeat',['template'=>"{label}\n<div class='col-md-8'>{input}\n{hint}\n{error}</div>"])
                ->label(Yii::t('backend', 'Re-type New Password'),['class'=>'control-label col-md-4'])
                ->passwordInput(['placeholder'=>Yii::t('backend', 'Re-type New Password'),'maxlength' => true])?>

                <div class="margin-top-10">
                    <?= Html::submitButton(
                        $model->isNewRecord ?'<i class="fa fa-check"></i> '.Yii::t('backend', 'Submit'):'<i class="fa fa-check"></i> '.Yii::t('backend', 'Update'),
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
        </div>
         <div class="tab-pane" id="tab_1_2">
            	<div class="form-body">
                    <div class="form-group">
                        <?= Html::submitButton(
                            $model->isNewRecord ?'<i class="fa fa-check"></i> '.Yii::t('backend', 'Submit'):'<i class="fa fa-check"></i> '.Yii::t('backend', 'Update'),
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
            	<div class="form-group">

                    <?= $form->field($model, 'restaurant_id',[
                        'template'=>"{label}\n<div class='col-md-12'>{input}\n{hint}\n{error}</div>"]
                    )->label(Yii::t('backend', 'Restaurants assigned'),['class'=>'control-label col-md-3'])
                        ->textInput([
                            'class'=>'form-control select2me',
                            'multiple'=>'multiple',
                            'value'=>$resIds
                        ]) ?>
				</div>

                </div>
            <?php ActiveForm::end(); ?>
        </div>
        <!-- END CHANGE PASSWORD TAB -->
    </div>
</div>
<div class="modal fade" id="dialog-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Confirm Delete</h4>
              </div>
              <div class="modal-body">
                  Are you sure to delete this image?
              </div>
              <div class="modal-footer">
                  <a data-dismiss="modal" class="btn btn-default" type="button">Close</a>
                  <a class="btn btn-warning delete-image-btn" type="button" onclick="DeleteImage()" href="javascript:void(0)"
                  		action="" img="" imgid="" index=""
                  > Delete</a>
              </div>
          </div>
      </div>
</div>
<script language="javascript">
    function checkDelete(img,imgid,index){
        $('.delete-image-btn').attr('img',img);
        $('.delete-image-btn').attr('imgid',imgid);
        $('.delete-image-btn').attr('index',index);
        $('#dialog-delete').modal('show');
    }
    function DeleteImage(){
        img = $('.delete-image-btn').attr('img');
        imgid = $('.delete-image-btn').attr('imgid');
        index = $('.delete-image-btn').attr('index');
        <?php if($model->id):?>
        $.ajax({
            url: "<?php echo \yii\helpers\Url::base() . "/user/manage/delimg"?>",
            type: "POST",
            data: ({'img':img, 'id':imgid}),
            success: function(msg){
                if(msg == 1){
                    window.location.href = '<?php echo \yii\helpers\Url::base() . "/user/manage/update?id=".$model->id?>';
                }
            }
        });
        <?php else:?>
        $('input[name=image_name'+index+']').val('');
        $('#img'+imgid).attr('src','<?php echo $df_image;?>&'+new Date().getTime());
        <?php endif;?>
        $('#dialog-delete').modal('hide');
    }
</script>
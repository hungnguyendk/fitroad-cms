<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\commons\models\entities\FitRoadUser */
/* @var $form yii\widgets\ActiveForm */


$asset	= backend\assets\AppAsset::register($this);
$roles  = \backend\modules\authorization\models\entity\AuthItem::find()
    ->andOnCondition(['type'=>1])
    ->andOnCondition(['not in','name',['sysadmin']])->all();
unset($roles['sysadmin']);
if(!\Yii::$app->user->can('authorization')){
    unset($roles['admin']);
}

$arrRoleManage = \yii\helpers\ArrayHelper::map($roles,'role_id','name');
//$arrRoleManage = array_merge($arrRoleManage,['3121'=>'customer']);
//echo '<pre>';
//print_r($arrRoleManage);
//echo '</pre>';
//die();
?>

<div class="fit-road-user-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'successCssClass'=>'has-success has-feedback',
        'errorCssClass' => 'has-error has-feedback',
        'fieldConfig'=>[
        ]

    ]);?>

    <?= $form->field($model, 'role',['template'=>"{label}\n<div class='col-md-9'>\n{input}\n{hint}\n{error}</div>"])
        ->label(Yii::t('backend', 'Role'),['class'=>'control-label col-md-2'])
        ->dropDownList(
            $arrRoleManage,
            [
                'class'=>['form-control select2me'],
                'options'=>[
                    $model->role => ['selected' => true]
                ]
            ]
        );?>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <?= $form->field($model, 'first_name',['template'=>"{label}\n<div class='col-md-8'>{input}\n{hint}\n{error}</div>"])
                        ->label(Yii::t('backend', 'Information'),['class'=>'control-label col-md-4'])
                        ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'First Name')]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'last_name',['template'=>"<div class='col-md-10'>{input}\n{hint}\n{error}</div>"])
                        ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'last Name')]) ?>
                </div>
            </div>
        </div>
    </div>
    <?= $form->field($model, 'email',['template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"])
        ->label(Yii::t('backend', 'Email'),['class'=>'control-label col-md-2'])
        ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'Email')]) ?>

    <?= $form->field($model, 'password',['template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"])
        ->label(Yii::t('backend', 'New Password'),['class'=>'control-label col-md-2'])
        ->passwordInput(['placeholder'=>Yii::t('backend', 'New Password'),'maxlength' => true])?>

    <?= $form->field($model, 'password_repeat',['template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"])
        ->label(Yii::t('backend', 'Re-type New Password'),['class'=>'control-label col-md-2'])
        ->passwordInput(['placeholder'=>Yii::t('backend', 'Re-type New Password'),'maxlength' => true])?>

    <?= $form->field($model, 'phone_number',[
        'template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"
    ]) ->label(Yii::t('backend', 'Phone'),['class'=>'control-label col-md-2'])
        ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'Phone Number')]) ?>

    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <?= $form->field($model, 'weight',['template'=>"{label}\n<div class='col-md-6'>{input}\n{hint}\n{error}</div>"])
                        ->label(Yii::t('backend', 'Physical'),['class'=>'control-label col-md-4'])
                        ->textInput(['type'=>'number','maxlength' => true,'placeholder'=>Yii::t('backend', 'Weight')]) ?>
                </div>
                <div class="col-md-3" style="margin-left: -86px;">
                    <?= $form->field($model, 'height',['template'=>"<div class='col-md-12'>{input}\n{hint}\n{error}</div>"])
                        ->textInput(['type'=>'number','maxlength' => true,'placeholder'=>Yii::t('backend', 'Height')]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'age',['template'=>"<div class='col-md-12'>{input}\n{hint}\n{error}</div>"])
                        ->textInput(['type'=>'number','maxlength' => true,'placeholder'=>Yii::t('backend', 'Age')]) ?>
                </div>
            </div>
        </div>
    </div>
    <?= $form->field($model, 'gender',
        ['template'=>"{label}\n<div class='col-md-7'>{input}\n{hint}\n{error}</div>"])
        ->dropDownList([null=>'Undefined',1=>'Male',0=>'Female'])->label(Yii::t('backend','Gender'),
            ['class'=>'control-label col-md-2']) ?>

    <div class="margin-top-10">
        <?= Html::submitButton(
            $model->isNewRecord ?'<i class="fa fa-check"></i> '.Yii::t('backend', 'Submit'):'<i class="fa fa-check"></i> '.Yii::t('backend', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php ActiveForm::end(); ?>

   <!-- <?php /*$form = ActiveForm::begin(); */?>

    <?/*= $form->field($model, 'first_name')->textInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'last_name')->textInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'password')->passwordInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'email')->textInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'height')->textInput() */?>

    <?/*= $form->field($model, 'weight')->textInput() */?>

    <?/*= $form->field($model, 'gender')->dropDownList([ 1 => '1', 0 => '0', ], ['prompt' => '']) */?>

    <?/*= $form->field($model, 'age')->textInput() */?>

    <?/*= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'alias')->textInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'point')->textInput() */?>

    <?/*= $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'time_zone')->textInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'role')->dropDownList([ -1 => '-1', '0001' => '0001', '0010' => '0010', '0011' => '0011', ], ['prompt' => '']) */?>

    <?/*= $form->field($model, 'status')->textInput() */?>

    <?/*= $form->field($model, 'created_at')->textInput() */?>

    <?/*= $form->field($model, 'updated_at')->textInput() */?>

    <div class="form-group">
        <?/*= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) */?>
    </div>

    --><?php /*ActiveForm::end(); */?>

</div>

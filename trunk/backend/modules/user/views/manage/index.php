<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use api\commons\helpers\ApiHelpers;
/* @var $this yii\web\View */
/* @var $searchModel backend\commons\models\searchs\FitRoadUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Fit Road Users');

$this->params['breadcrumbs'][] = $this->title;

$asset		= backend\assets\AppAsset::register($this);
/* @var $this yii\web\View */

$asset->css[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/jquery.fancybox.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-buttons.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.css';

$asset->js[] = 'theme/assets/global/plugins/fancybox/jquery.fancybox.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-buttons.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-media.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.js';
$this->registerJsFile(Yii::$app->homeUrl.'scripts/fancybox.js', ['depends' =>'yii\web\YiiAsset']);
$asset->css[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css';
$menuItems = [
    [
        'options' => [
            'class' => 'caption'
        ],
        'label' => \Yii::t('backend','Members'),
        'url' => ['manage/index'],
        'template' => '<a href="{url}">{label}</a>'
    ],
    [
        'options' => [
            'class' => 'caption'
        ],
        'label' => \Yii::t('backend','Employees'),
        'url' => ['manage/staff'],
        'template' => '<a href="{url}">{label}</a>'
    ],
];
?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<!-- BEGIN INCLUSION TABLE PORTLET-->
<div class="portlet gren">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <?php
                echo \nterms\pagesize\PageSize::widget([
                    'label' => false,
                    'template'=>'Show {list} records',
                    'options'=> ['class'=>'form-control input-xsmall input-inline']
                ]);

                ?>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="dataTables_length pull-right">
                    <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add new'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row row-vgrid-20">
            <div class="col-md-12 col-sm-1">
                <div class="tabbable-line">
                    <?= \yii\widgets\Menu::widget([
                        'options' => [
                            'class' => 'caption nav nav-tabs',
                            'data-keep-expanded' => false,
                            'data-auto-scroll' => true,
                            'data-slide-speed' => '200'
                        ],
                        'items' => $menuItems,
                        'submenuTemplate' => "\n<ul class='sub-menu'>\n{items}\n</ul>\n",
                        'encodeLabels' => false, // allows you to use html in labels
                        'activateParents' => true,
                        'activeCssClass' => 'active'
                    ]);
                    ?>
                    <p></p>
                    <?php Pjax::begin(); ?>
                    <?= GridView::widget([
                        'layout'=>"<div class='pull-left'>{summary}</div>\n{items}\n{pager}\n",
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
//                            [
//                                'format' => 'html',
//                                'filter'=>false,
//                                'label'=>'Avatar',
//                                'headerOptions' => ['width' => '60px'],
//                                'content' => function ($searchModel, $index, $widget) {
//                                    return ($searchModel->getAvatar())?Html::tag('a',Html::img($searchModel->getAvatar(Yii::$app->params['thumbName']),['class'=>'todo-userpic','height'=>'35px','width'=>'35px']) , [
//                                        'href'=>$searchModel->getAvatar(),
//                                        'class'=>'fancybox-thumbs',
//                                        'data-fancybox-group'=>'thumb'
//                                    ]):null;
//
//                                }
//                            ],
                            ['class' => 'yii\grid\ActionColumn'],
                            [
                                'attribute'=>'full_name',
                                //'header'=>'Event title',
                                // 'headerOptions'=>['class'=>$classSortEvent],
                                'filterInputOptions' => [
                                    'class'       => 'form-control',
                                    'placeholder' => 'Search Name'
                                ],
                                'content'=>function ($model, $key, $index, $column){
                                    $html = '';
                                    //return trim($model->first_name.' '.$model->last_name);

                                    $html .= '<div class="item-details">';
                                    $html .= ($model->getAvatar(Yii::$app->params['thumbName']))?'<a href="'.$model->getAvatar().'" class="fancybox-thumbs" data-fancybox-group="thumb"><img width="45" height="45" class="todo-userpic" src="'.$model->getAvatar(Yii::$app->params['thumbName']).'"></a>':'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                    $html .= '<a href="'.Yii::$app->urlManager->createUrl(['/user/manage/update','id'=>$model->id]).'" class="item-name primary-link">'.trim($model->first_name.' '.$model->last_name).'</a>
                                    </div>';

                                    return $html;
                                }
                            ],
//                            [
//                                'attribute' => 'first_name',
//                                'filterInputOptions' => [
//                                    'class'       => 'form-control',
//                                    'placeholder' => 'Search first name'
//                                ]
//                            ],
//                            [
//                                'attribute' => 'last_name',
//                                'filterInputOptions' => [
//                                    'class'       => 'form-control',
//                                    'placeholder' => 'Search last name'
//                                ]
//                            ],
                            //'password',
                            [
                                'attribute' => 'email',
                                'filterInputOptions' => [
                                    'class'       => 'form-control',
                                    'placeholder' => 'Search email'
                                ]
                            ],
                            [
                                'format' => 'integer',
                                'attribute' =>'height',
                                'headerOptions' => ["class"=>'sort-numerical'],
                                'filterInputOptions' => [
                                    'class'       => 'form-control',
                                    'placeholder' => 'Search weight'
                                ]
                            ],
                            [
                                'format' => 'integer',
                                'attribute' =>'weight',
                                'headerOptions' => ["class"=>'sort-numerical'],
                                'filterInputOptions' => [
                                    'class'       => 'form-control',
                                    'placeholder' => 'Search weight'
                                ]
                            ],
                            [
                                'format' => 'integer',
                                'attribute' =>'age',
                                'headerOptions' => ["class"=>'sort-numerical'],
                                'filterInputOptions' => [
                                    'class'       => 'form-control',
                                    'placeholder' => 'Search age'
                                ]
                            ],
                            [
                                'attribute'=>'gender',
                                'header'=>'Gender',
                                'filter'=>['1'=>'Male','0'=>'Female'],
                                'content'=> function ($model) {
                                    return ($model->gender ==1)?\Yii::t('backend','Male'):\Yii::t('backend','Female');
                                },
                            ],
//                            'phone_number',
//                            'alias',
                            // 'point',
                            // 'password_reset_token',
                            // 'time_zone',
                            // 'role',
                            // 'status',
                            // 'updated_at',
                            [
                                'attribute'=>'created_at',
                                'filter'=>FALSE,
                                'value'=> function ($model) {
                                    return \Yii::$app->formatter->asDate($model->created_at,'php:D d M Y');
                                },
                            ],

                        ],
                        'tableOptions' => [
                            'id'=> 'girdViewUser',
                            'class' => 'table table-striped table-bordered table-hover'
                        ]
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END INCLUSION TABLE PORTLET-->

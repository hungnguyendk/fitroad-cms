<?php

namespace backend\modules\user\controllers;

use api\commons\helpers\ApiHelpers;
use backend\modules\auth\controllers\AuthenticateController;
use backend\modules\food\models\FitRoadGalleryImage;
use Yii;
use backend\commons\models\entities\FitRoadUser;
use backend\commons\models\searchs\FitRoadUserSearch;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\commons\forms\SignUpForm;
use backend\commons\helpers\UtilHelper;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for FitRoadUser model.
 */
class ManageController extends AuthenticateController
{
    /**
     * Lists all FitRoadUser models.
     * @return mixed
     */
    public $enableCsrfValidation = false;  
    public function actionIndex()
    {
        $searchModel = new FitRoadUserSearch();
        $dataProvider = $searchModel->searchCustomer(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all FitRoadUser models.
     * @return mixed
     */
    public function actionProfile($id){
        $this->layout = '//profile';
        $searchModel = new FitRoadUserSearch();
        $alert = [];
        $msgAlert = null;
        $statusAlertClass = \Yii::$app->params['alertSuccessStatusClass'];

        $model = new SignUpForm(['scenario' =>['scenario' => SignUpForm::SCENARIO_UPDATE]]);

        $model = $model->findOne($id);

        $resIds = $searchModel->listGotRestaurants($id);

        $avatar = $model->getFitRoadGalleryImages()->where([
            'status'=>1,
            'is_current'=>1
        ])->one();
        // die(var_dump());
        $params = Yii::$app->request->post();

        if($params){
            if(isset($params['restaurant'])){
                $restaurantIds = $params['restaurant'];
                // Save restaurant;
                $searchModel->saveRestaurants($restaurantIds,$id);
            }
            $paramsSignUpForm = Yii::$app->request->post('SignUpForm');

            if(isset($paramsSignUpForm['age'])){
                $paramsSignUpForm = array_merge($paramsSignUpForm,[
                    'age'=>(int)$paramsSignUpForm['age']
                ]);

                $params = array_merge($params,['SignUpForm'=>$paramsSignUpForm]);
            }


        }
        $oldRole = $model->role;

        if ($model->load($params) && $model->save()){


            if($oldRole == FitRoadUser::FIX_ROAD_USER_ROOT){

                $model->role = FitRoadUser::FIX_ROAD_USER_ROOT;
                $model->save(FALSE);
                unset($model->changedAttributes['role']);
            }

            $uploadFile = UtilHelper::uploadAttachmentFile(
                Yii::getAlias('@publicImagesAvatars'),
                null,
                'avatar_file'
            );

            if($uploadFile && $uploadFile->errorStatus == FALSE){
                FitRoadUser::updateDoNotCurrentAllImage($model->id);

                $imageModel = new FitRoadGalleryImage();

                $galleryParam = [
                    'food_id'=>null,
                    'user_id'=>$id,
                    'url'=>$uploadFile->output[0],
                    'status'=> \api\commons\models\entities\FitRoadGalleryImage::STATUS_ACTIVE,
                    'type'=> \api\commons\models\entities\FitRoadGalleryImage::IMAGE_TYPE_AVATAR,
                    'is_current'=>\api\commons\models\entities\FitRoadGalleryImage::IS_CURRENT
                ];

                $imageModel->attributes = $galleryParam;
                $imageModel->save();

            }else if($uploadFile && $uploadFile->errorStatus == true){
                //die(var_dump($uploadFile));
                return $this->render('update', [
                    'model' => $model,
                    'uploadFile'=>$uploadFile,
                    'resIds'=> json_encode($resIds),
                    'avatar'=>$avatar
                ]);
            }

            if(!empty($model->getChangedAttributes())){
                $msgAlert = UtilHelper::summaryChangedAttributes($model);
            }else{
                if(isset($params['restaurant'])){
                    $msgAlert = \Yii::t('backend','New restaurants is added successfully.');
                }else{
                    $msgAlert = \Yii::t('backend','No attribute values are changed');
                    $statusAlertClass = \Yii::$app->params['alertInfoStatusClass'];
                }
            }
            $alert =  \yii\bootstrap\Alert::widget([
                'options' => [
                    'class' => 'alert '.$statusAlertClass,
                ],
                'body' => $msgAlert,
            ]);
        } else {
            $model->phone_number = $model->phone_number;
        }
        $model->password = null;
        $model->password_repeat = null;

        $resIds = $searchModel->listGotRestaurants($id);

        $avatar = $model->getFitRoadGalleryImages()->where([
            'status'=>1,
            'is_current'=>1
        ])->one();

        return $this->render('update',[
            'model' => $model,
            'alert'=> $alert,
            'resIds'=> json_encode($resIds),
            'avatar'=>$avatar
        ]);
    }

    public function actionStaff()
    {
        $searchModel = new FitRoadUserSearch();
        $dataProvider = $searchModel->searchStaff(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FitRoadUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = '//profile';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single FitRoadUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionProfileView($id)
    {
        $this->layout = '//profile';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FitRoadUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignUpForm(['scenario' => SignUpForm::SCENARIO_REGISTER]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FitRoadUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = '//profile';
		$searchModel = new FitRoadUserSearch();
        $alert = [];
        $msgAlert = null;
        $addAttribute = [];
        $statusAlertClass = \Yii::$app->params['alertSuccessStatusClass'];

        $model = new SignUpForm(['scenario' =>['scenario' => SignUpForm::SCENARIO_UPDATE]]);

        $model = $model->findOne($id);
       // die(var_dump());
        $params = Yii::$app->request->post();
        $resIds = $searchModel->listGotRestaurants($id);

        $avatar = $model->getFitRoadGalleryImages()->where([
            'status'=>1,
            'is_current'=>1
        ])->one();
        //die(var_dump($avatar->url));
        if($params){

			if(isset($params['SignUpForm']['restaurant_id']) && !empty($params['SignUpForm']['restaurant_id'])){
				$restaurantIds = explode(',',$params['SignUpForm']['restaurant_id']);

				// Save restaurant;
				$searchModel->saveRestaurants($restaurantIds,$id);
			}else{

                $searchModel->deleteAssigned($id);
            }
            $paramsSignUpForm = Yii::$app->request->post('SignUpForm');

            if(isset($paramsSignUpForm['age'])){
                $paramsSignUpForm = array_merge($paramsSignUpForm,[
                    'age'=>(int)$paramsSignUpForm['age']
                ]);

                $params = array_merge($params,['SignUpForm'=>$paramsSignUpForm]);
            }

        }

        if ($model->load($params) && $model->save()){

            $uploadFile = UtilHelper::uploadAttachmentFile(
                Yii::getAlias('@publicImagesAvatars'),
                null,
                'avatar_file'
            );

            if($uploadFile && $uploadFile->errorStatus == FALSE){
                FitRoadUser::updateDoNotCurrentAllImage($model->id);

                $imageModel = new FitRoadGalleryImage();

                $galleryParam = [
                    'food_id'=>null,
                    'user_id'=>$id,
                    'url'=>$uploadFile->output[0],
                    'status'=> \api\commons\models\entities\FitRoadGalleryImage::STATUS_ACTIVE,
                    'type'=> \api\commons\models\entities\FitRoadGalleryImage::IMAGE_TYPE_AVATAR,
                    'is_current'=>\api\commons\models\entities\FitRoadGalleryImage::IS_CURRENT
                ];

                $imageModel->attributes = $galleryParam;
                $imageModel->save();

            }else if($uploadFile && $uploadFile->errorStatus == true){
                //die(var_dump($uploadFile));
                return $this->render('update', [
                    'model' => $model,
                    'uploadFile'=>$uploadFile,
                    'resIds'=>(!empty($resIds))?json_encode($resIds):'',
                    'avatar'=>$avatar
                ]);
            }
//            array_merge($model->changedAttributes,['avatar'=>'success']);
//
//            die(var_dump($model->changedAttributes ));

            if(!empty($model->getChangedAttributes())){
                $msgAlert = UtilHelper::summaryChangedAttributes($model,[]);
                $alert =  \yii\bootstrap\Alert::widget([
                    'options' => [
                        'class' => 'alert '.$statusAlertClass,
                    ],
                    'body' => $msgAlert,
                ]);
            }

        } else {
            $model->phone_number = $model->phone_number;
        }

        $model->password = null;
        $model->password_repeat = null;

        $resIds = $searchModel->listGotRestaurants($id);

        $avatar = $model->getFitRoadGalleryImages()->where([
            'status'=>1,
            'is_current'=>1
        ])->one();

        return $this->render('update',[
            'model' => $model,
            'alert'=> $alert,
            'resIds'=> (!empty($resIds))?json_encode($resIds):'',
            'avatar'=>$avatar
        ]);
    }

    /**
     * Deletes an existing FitRoadUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $imageModel = (new FitRoadGalleryImage)->findAll(['user_id'=>$id,'type'=>'avatar']);

        if($imageModel && is_array($imageModel) && !empty($imageModel)){
            foreach($imageModel as $image){
                $url = $image->url;

                try{
                    @unlink(Yii::getAlias('@publicImagesAvatars').'/'.$url);
                    @unlink(Yii::getAlias('@publicImagesAvatars').'/'.Yii::$app->params['thumbName'].$url);
                    @unlink(Yii::getAlias('@publicImagesAvatars').'/'.Yii::$app->params['normalName'].$url);
                }catch (Exception $e){
                    Yii::error($e .'unlink avatar image');
                }
            }

        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FitRoadUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FitRoadUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FitRoadUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    function actionDelimg(){
        $data = Yii::$app->request->post();
        $img 	= $data['img'];
        $id 	= $data['id'];
        $imageModel = (new FitRoadGalleryImage)->findOne(['id'=>$id]);


        if($imageModel && $imageModel->delete()){

            try{
                @unlink(Yii::getAlias('@publicImagesAvatars').'/'.$img);
                @unlink(Yii::getAlias('@publicImagesAvatars').'/'.Yii::$app->params['thumbName'].$img);
                @unlink(Yii::getAlias('@publicImagesAvatars').'/'.Yii::$app->params['normalName'].$img);
            }catch (Exception $e){
                Yii::error($e .'unlink avatar image');
            }
        }

        echo 1;
    }

}

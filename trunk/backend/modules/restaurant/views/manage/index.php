<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use api\commons\helpers\ApiHelpers;
/* @var $this yii\web\View */
/* @var $searchModel backend\commons\models\searchs\FitRoadRestaurant */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Fit Road Restaurants');
$this->params['breadcrumbs'][] = $this->title;
$asset		= backend\assets\AppAsset::register($this);
/* @var $this yii\web\View */
$asset->css[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css';
$asset->css[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/jquery.fancybox.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-buttons.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.css';

$asset->js[] = 'theme/assets/global/plugins/fancybox/jquery.fancybox.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-buttons.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-media.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.js';
$this->registerJsFile(Yii::$app->homeUrl.'scripts/fancybox.js', ['depends' =>'yii\web\YiiAsset']);
$this->registerJsFile(Yii::$app->homeUrl.'scripts/myjs.js',[ 'depends' => 'yii\web\YiiAsset']);

?>
<div class="fit-road-restaurant-index">
 <div class="row">
            <div class="col-md-6 col-sm-6">
            	<form id="formIndex">
                <?php
                echo \nterms\pagesize\PageSize::widget([
                    'label' => false,
                    'template'=>'Show {list} records',
                    'options'=> ['class'=>'form-control input-xsmall input-inline','id'=>'perpage']
                ]);

                ?>
                </form>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="dataTables_length pull-right">
                    <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add new'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
    </div><hr/>
<?php Pjax::begin(); ?>    
	<?= GridView::widget([
        'layout'=>"<div class='pull-left'>{summary}</div>\n{items}\n{pager}\n",
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions'=>['class'=>'table table-striped table-bordered table-hover dataTable no-footer'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
           // 'place_id',
           // 'identifier',
//            [
//                'attribute' => 'id',
//                'headerOptions' => ["class"=>'sort-numerical'],
//                'filterInputOptions' => [
//                    'class'       => 'form-control',
//                    'placeholder' => 'Search Id'
//                ]
//            ],
            [
                'attribute' => 'name',
                'headerOptions' => ["width"=>'10%'],
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Search name'
                ]
            ],
            [
                'format' => 'html',
                'filter'=>false,
                'label'=>'Image',
                'headerOptions' => ['width' => '60px'],
                'value' => function ($searchModel, $index, $widget) {
                    return  Html::tag('a',Html::img(ApiHelpers::builtUrlImages(Yii::$app->params['restaurantsFinder'],$searchModel->avatar,Yii::$app->params['normalName']),['style'=>'width:100px']) , [
                        'href'=>ApiHelpers::builtUrlImages(Yii::$app->params['restaurantsFinder'],$searchModel->avatar,Yii::$app->params['normalName']),
                        'class'=>'fancybox-thumbs',
                        'data-fancybox-group'=>'thumb'
                    ]);

                }
            ],
            [
                'attribute'=>'manager_name',
                //'header'=>'Event title',
                // 'headerOptions'=>['class'=>$classSortEvent],
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Search Manager'
                ],
                'content'=>function ($model, $key, $index, $column){
                    $html = '';

                    if($model->fitRoadRestaurantHasManagedByUsers && !empty($model->fitRoadRestaurantHasManagedByUsers)){
                        foreach($model->fitRoadRestaurantHasManagedByUsers as $manager){
                          //  $html .= '<p><a href="'.Yii::$app->urlManager->createUrl(['/user/update','id'=>$manager->user->id]).'">'.trim($manager->user->first_name.' '.$manager->user->last_name).'</a></p>';
                            $html .= '<div class="item-details">';
                            $html .= ($manager->user->getAvatar(Yii::$app->params['thumbName']))?'<a href="'.$manager->user->getAvatar(Yii::$app->params['thumbName']).'" class="fancybox-thumbs" data-fancybox-group="thumb"><img width="27" height="27" class="todo-userpic" src="'.$manager->user->getAvatar(Yii::$app->params['thumbName']).'"></a>':'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                            $html .= '<a href= href="'.Yii::$app->urlManager->createUrl(['/user/update','id'=>$manager->user->id]).'" class="item-name primary-link">'.trim($manager->user->first_name.' '.$manager->user->last_name).'</a>
                                    </div>';
                        }

                    }
                    return $html;
                }
            ],

            //'description:ntext',
//            [
//                'attribute' => 'phone',
//                'filterInputOptions' => [
//                    'class'       => 'form-control',
//                    'placeholder' => 'Search phone'
//                ]
//            ],
            // 'fax',
            //'website',
            // 'lat',
            // 'long',
            [
                'attribute' => 'address',
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Search address'
                ]
            ],
            [
                'attribute'=>'is_vip',
                'header'=>'Vip',
                'filter'=>['1'=>'Vip','0'=>'Normal'],
                'content'=> function ($model) {
                    return ($model->is_vip == 1)?\Yii::t('backend','Vip'):\Yii::t('backend','Normal');
                },
            ],
            [
                'attribute'=>'created_at',
                'filter'=>FALSE,
                'value'=> function ($model) {
                    return \Yii::$app->formatter->asDate($model->created_at,'php:D d M Y');
                },
            ],
//            [
//            'format' => 'html',
//		    'filter'=>false,
//		    'label'=>'Icon',
//		    'headerOptions' => ['width' => '60px'],
//		    'value' => function ($searchModel, $index, $widget) {
//				return '<img style="width:40px" src="'.$searchModel->icon.'" alt="" />';
//		     }
//			],
            // 'type',
            // 'status',
            // 'google_data:ntext',
            // 'rating',
//            [
//            'format' => 'html',
//		    'filter'=>false,
//		    'label'=>'Avatar',
//		    'headerOptions' => ['width' => '60px'],
//		    'value' => function ($searchModel, $index, $widget) {
//				return '<img style="width:150px;height:100px" src="'.ApiHelpers::builtUrlImages(Yii::$app->params['restaurantsFinder'],$searchModel->avatar,Yii::$app->params['normalName']).'" alt="" />';
//		     }
//			],
            // 'updated_at',


        ],
    ]); 
    ?>
<?php Pjax::end(); ?></div>

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use api\commons\helpers\ApiHelpers;
/* @var $this yii\web\View */
/* @var $model backend\commons\models\entities\FitRoadRestaurant */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Restaurant',
]) . $model->name;

\backend\commons\helpers\UtilHelper::builtBreadcrumb(3,$this,[
	'titleLevel1'=>Yii::t('backend', 'Restaurants Management'),
	'titleLevel2'=>Yii::t('backend','Update'),
	'urlLevel1'=>['index'],
	'urlLevel2'=>['view','id'=>$model->id],
	'objectName'=>trim($model->name)
]);
if(isset($uploadFile) && is_array($uploadFile->getErrors()) && !empty($uploadFile->getErrors())){
	$alert =  \yii\bootstrap\Alert::widget([
		'options' => [
			'class' => 'alert '.\Yii::$app->params['alertErrorStatusClass'],
		],
		'body' => \yii\helpers\BaseHtml::errorSummary($uploadFile),
	]);
}
?>
<script>
	var id  = "<?=$model->id ?>";
</script>
<?php
$jsScripts = <<<JS
 jQuery('#upgradeVip').on('click',function(){
   	bootbox.confirm("Are you sure?", function(result) {
        if(result == true){
 		 jQuery.ajax({
            url: BASE_URL + '/fitroad-cms/trunk/restaurant/manage/ajax-upgrade-vip',
        	//url: BASE_URL + '/restaurant/manage/ajax-upgrade-vip',
            type: 'post',
            dataType : 'json',
            accepts: "application/json",
            data: {modelId:id,_csrf:yii.getCsrfToken()},
            success: function (response) {
					if(response.status){
					 Metronic.alert({
                    container: null, // alerts parent container(by default placed after the page breadcrumbs)
                    place: 'append', // append or prepent in container
                    type: 'success',  // alert's type
                    message: response.message,  // alert's message
                    close: true, // make alert closable
                    reset: true, // close all previouse alerts first
                    focus: true, // auto scroll to the alert after shown
                    closeInSeconds: 5, // auto close after defined seconds
                    icon: 'check' // put icon before the message
                });


                jQuery('#upgradeVip').html(response.btnText);
					}else{
						 Metronic.alert({
						container: null, // alerts parent container(by default placed after the page breadcrumbs)
						place: 'append', // append or prepent in container
						type: 'warning',  // alert's type
						message: response.message,  // alert's message
						close: true, // make alert closable
						reset: true, // close all previouse alerts first
						focus: true, // auto scroll to the alert after shown
						closeInSeconds: 5, // auto close after defined seconds
						icon: 'warning' // put icon before the message
					});
					}
            }
        		});
          }
      });


 })
JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
?>
<div class="fit-road-restaurant-update">
	<?php echo(isset($alert) && !empty($alert))?$alert:''; ?>
<div class="row">
			<?php $form = ActiveForm::begin(['id'=>'updateForm',
				'layout' => 'horizontal',
				'options' => ['enctype'=>'multipart/form-data'],
				'fieldConfig' => [
			        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
			        'horizontalCssClasses' => [
			            'label' => 'col-sm-4',
			            'offset' => 'col-sm-offset-4',
			            'wrapper' => 'col-sm-8 marginBot',
			            'error' => '',
			            'hint' => '',
			        ],
			    ],
			]); ?>
			<div class="col-md-12">
			<div class="portlet">
				
			<div class="portlet-title">
				<div class="caption" style="font-size: 18pt">
					<span class="caption-subject">General Infomation</span> <span style="font-size: 12pt">* required</span>
				</div>
							
				<div class="actions pull-right">
					<button type="button" id="upgradeVip" class="btn warning">
						<?php echo (boolval($model->is_vip) == FALSE)?'Vip Upgrade ':'Vip Downgrade';?>
					</button>
					<button type="submit" id="savebtn" class="btn green">
						 Save
					</button>
					<a href="<?= Yii::$app->homeUrl . "food"?>" id="savebtn" class="btn red">
						 Cancel
					</a>						
				</div>
			</div>				
			</div>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-11"><!-- BEGIN Portlet PORTLET-->
							<div class="portlet">
								<div class="portlet-body from">
											
												<?= $form->field($model, 'name',['labelOptions'=>['label'=>'Name']]) ?>
												<?= $form->field($model, 'description',['labelOptions'=>['label'=>'Description']])->textArea(['rows' => '3']) ?>
												<?= $form->field($model, 'phone',['labelOptions'=>['label'=>'Phone']]) ?>						
												<?= $form->field($model, 'fax',['labelOptions'=>['label'=>'Fax']]) ?>	
												<?= $form->field($model, 'website',['labelOptions'=>['label'=>'Website']]) ?>
												<?= $form->field($model, 'lat',['labelOptions'=>['label'=>'Latitude']]) ?>	
												<?= $form->field($model, 'long',['labelOptions'=>['label'=>'Longitude']]) ?>
												<?= $form->field($model, 'address',['labelOptions'=>['label'=>'Address']]) ?>




								</div>
								<div class="form-group <?php echo isset($error_upload) && $error_upload ? 'has-error' : '';?>">
			                      	<label class="control-label col-md-4">Avatar<br/>
			                      		<?php echo isset($error_message) && $error_message ? "<br/>".$error_message : '';?>
			                      	</label>
			                      	<div class="col-md-8">                                                               
			                      	<?php
				                      	$df_image = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
								    	$image	  = isset($model->avatar)?$model->avatar:false;
								    	for($i=0; $i<1;$i++){				    		
								    	?>
								    	<div class="col-lg-6">
								    		<input type="hidden" name="restaurant_file" value="<?php echo $image;?>"/>
								     		<div data-provides="fileupload" class="fileupload fileupload-new">
			                              		<div style="width: 200px; height: 150px;" class="fileupload-new thumbnail">
			                                  		<img src="<?php echo $image ?  ApiHelpers::builtUrlImages(Yii::$app->params['restaurantsFinder'],$image,Yii::$app->params['normalName']) : $df_image;?>" alt="" />
			                              		</div>
			                              		<div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
			                          			<div>
					                               <span class="btn btn-white btn-file">
					                               <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select Image</span>
					                               <span class="fileupload-exists"><i class="fa fa-undo"></i>Change Image</span>
					                              	<input type="file" name="restaurant_file" class="default" /></span>
					                               </span>
			                              			<a data-dismiss="fileupload" class="btn btn-danger <?php echo $image ? '' : 'fileupload-exists';?>" <?php echo $image ? 'onclick="checkDelete(\''.$image.'\', \''.md5($image).'\', \''.$i.'\')"' : '';?> href="#"><i class="fa fa-trash"></i>Delete Image</a>
			                          			</div>
			                      				
			                          		</div>
			                          	</div>			    	
								    	<?php
								    	}
								    	?>					    	                         
			                      </div>                                            
			                  </div>
								<?php if(Yii::$app->user->identity->role == \backend\commons\models\entities\FitRoadUser::FIX_ROAD_ADMIN
								|| Yii::$app->user->identity->role == \backend\commons\models\entities\FitRoadUser::FIX_ROAD_USER_ROOT):?>
								<div class="form-group">
										<label class="col-sm-4" style="text-align: right">Is Active</label>
										<div class="col-md-2" style="">
										<?= $form->field($model, 'status')->checkbox(['label' => '', 'value'=>1, 'uncheckValue'=>0,'class'=>""]); ?>	<br/>
										</div>
									</div>
								<?php endif ?>
							</div>
							
					</div>
				</div>
			</div>
</div>
</div>
<div class="modal fade" id="dialog-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Confirm Delete</h4>
              </div>
              <div class="modal-body">
                  Are you sure to delete this image?
              </div>
              <div class="modal-footer">
                  <a data-dismiss="modal" class="btn btn-default" type="button">Close</a>
                  <a class="btn btn-warning delete-image-btn" type="button" onclick="DeleteImage()" href="javascript:void(0)"
                  		action="" img="" imgid="" index=""
                  > Delete</a>
              </div>
          </div>
      </div>
</div>
<script language="javascript">

    function checkDelete(action,img,imgid,index){
    	 $('.delete-image-btn').attr('img',img);
    	 $('.delete-image-btn').attr('imgid',imgid);
    	 $('.delete-image-btn').attr('index',index);
    	 $('#dialog-delete').modal('show');    	 
    }
    function DeleteImage(){
	    	 img = $('.delete-image-btn').attr('img');
	    	 imgid = $('.delete-image-btn').attr('imgid');
	    	 index = $('.delete-image-btn').attr('index');
	    	 	<?php if($model->id):?>
				$.ajax({
					url: "<?php echo Url::base() . "/restaurant/manage/delimg"?>",
					type: "POST",
					data: ({'img':img, 'id':'<?php echo $model->id;?>'}),
					success: function(msg){
						if(msg == 1){
							window.location.reload();
						}
					}
				});	
				<?php else:?>	
					$('input[name=image_name'+index+']').val('');
					$('#img'+imgid).attr('src','<?php echo $df_image;?>&'+new Date().getTime());	
				<?php endif;?>	
	    	 $('#dialog-delete').modal('hide');
    }
</script>
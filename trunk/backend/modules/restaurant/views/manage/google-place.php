<?php
/**
 * User: sangnguyen on  4/18/16 at 13:26
 * File name: google-place.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

use yii\helpers\Html;

$this->title = Yii::t('backend', 'Search places');

$asset		= backend\assets\AppAsset::register($this);

\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
    'titleLevel1'=>Yii::t('backend', 'Restaurant Management'),
    'titleLevel2'=>Yii::t('backend','Search places'),
    'urlLevel1'=>['index'],
    'urlLevel2'=>['google-search']
]);


$asset->css[] = 'theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css';
$asset->js [] = 'theme/assets/global/plugins/jquery.blockui.min.js';
$asset->js [] = 'theme/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js';
$asset->js [] = 'theme/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js';

$this->registerJsFile(Yii::$app->homeUrl.'scripts/google-place-searching.js',
    [ 'depends' => 'yii\web\YiiAsset']);

$this->registerJsFile('http://maps.googleapis.com/maps/api/js?key=AIzaSyBkasIPKFOEKKUKeKwxmTfCaEg4G6cWYO0&libraries=places&callback=initMap',
    [ 'depends' => 'yii\web\YiiAsset']);


?>

<div class="portlet-body">
    <div class="col-md-12">
        <div id="alert-message" > </div>
    </div>


        <div class="col-md-7">
            <div class="form-group">

                <input  id="address" placeholder="Enter address" type="text" class="form-control">
            </div>
        </div>
            <div class="col-md-2">
                <select id="country" class="form-control input-sm">
                    <option value="all">All</option>
                    <option value="au">Australia</option>
                    <option value="br">Brazil</option>
                    <option value="ca" selected>Canada</option>
                    <option value="fr">France</option>
                    <option value="de">Germany</option>
                    <option value="mx">Mexico</option>
                    <option value="nz">New Zealand</option>
                    <option value="it">Italy</option>
                    <option value="za">South Africa</option>
                    <option value="es">Spain</option>
                    <option value="pt">Portugal</option>
                    <option value="us" >U.S.A.</option>
                    <option value="uk">United Kingdom</option>
                    <option value="vn" >Viet Nam</option>
                </select>
            </div>


    <div id="map-container">
        <div class="col-md-8">
            <div id="map" class="gmaps"> </div>
        </div>
        <div class="col-md-4">
            <div id="listing">
                <table id="resultsTable">
                    <tbody id="results"></tbody>
                </table>
            </div>
        </div>
        <div style="display: none">
            <div id="info-content">
                <table>
                    <tr id="iw-url-row" class="iw_table_row">
                        <td id="iw-icon" class="iw_table_icon"></td>
                        <td id="iw-url"></td>
                    </tr>
                    <tr id="iw-address-row" class="iw_table_row">
                        <td class="iw_attribute_name">Address:</td>
                        <td id="iw-address"></td>
                    </tr>
                    <tr id="iw-phone-row" class="iw_table_row">
                        <td class="iw_attribute_name">Telephone:</td>
                        <td id="iw-phone"></td>
                    </tr>
                    <tr id="iw-rating-row" class="iw_table_row">
                        <td class="iw_attribute_name">Rating:</td>
                        <td id="iw-rating"></td>
                    </tr>
                    <tr id="iw-website-row" class="iw_table_row">
                        <td class="iw_attribute_name">Website:</td>
                        <td id="iw-website"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</div>


<div id="confirmRestaurantData" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Restaurant Info</h4>
    </div>
    <div class="modal-body">
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
        <button type="button" class="btn blue" id="modal-confirmed">Insert</button>
    </div>
</div>


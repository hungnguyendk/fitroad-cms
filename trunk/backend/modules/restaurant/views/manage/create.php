<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\commons\models\entities\FitRoadRestaurant */

$this->title = Yii::t('backend', 'Create Restaurant');

\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
	'urlLevel1'=>['index'],
	'titleLevel1'=>Yii::t('backend', 'Restaurants Management')
]);

if(isset($uploadFile) && is_array($uploadFile->getErrors()) && !empty($uploadFile->getErrors())){
	$alert =  \yii\bootstrap\Alert::widget([
		'options' => [
			'class' => 'alert '.\Yii::$app->params['alertErrorStatusClass'],
		],
		'body' => \yii\helpers\BaseHtml::errorSummary($uploadFile),
	]);
}
?>
<div class="fit-road-restaurant-create">
	<?php echo(isset($alert) && !empty($alert))?$alert:''; ?>
<div class="row">
			<?php $form = ActiveForm::begin(['id'=>'updateForm',
				'layout' => 'horizontal',
				'options' => ['enctype'=>'multipart/form-data'],
				'fieldConfig' => [
			        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
			        'horizontalCssClasses' => [
			            'label' => 'col-sm-4',
			            'offset' => 'col-sm-offset-4',
			            'wrapper' => 'col-sm-8 marginBot',
			            'error' => '',
			            'hint' => '',
			        ],
			    ],
			]); ?>
			<div class="col-md-12">
			<div class="portlet">
				
			<div class="portlet-title">
				<div class="caption" style="font-size: 18pt">
					<span class="caption-subject">General Infomation</span> <span style="font-size: 12pt">* required</span>
				</div>
							
				<div class="actions pull-right">
					<button type="submit" id="savebtn" class="btn green">
						 Save
					</button>
					<a href="<?= Yii::$app->homeUrl . "food"?>" id="savebtn" class="btn red">
						 Cancel
					</a>						
				</div>
			</div>				
			</div>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-11"><!-- BEGIN Portlet PORTLET-->
							<div class="portlet">
								<div class="portlet-body from">

												<?= $form->field($model, 'name',['labelOptions'=>['label'=>'Name']]) ?>
												<?= $form->field($model, 'description',['labelOptions'=>['label'=>'Description']])->textArea(['rows' => '3']) ?>
												<?= $form->field($model, 'phone',['labelOptions'=>['label'=>'Phone']]) ?>						
												<?= $form->field($model, 'fax',['labelOptions'=>['label'=>'Fax']]) ?>	
												<?= $form->field($model, 'website',['labelOptions'=>['label'=>'Website']]) ?>
												<?= $form->field($model, 'lat',['labelOptions'=>['label'=>'Latitude']]) ?>	
												<?= $form->field($model, 'long',['labelOptions'=>['label'=>'Longitude']]) ?>
												<?= $form->field($model, 'address',['labelOptions'=>['label'=>'Address']]) ?>




								</div>
								<div class="form-group <?php echo isset($error_upload) && $error_upload ? 'has-error' : '';?>">
			                      	<label class="control-label col-md-4">Avatar<br/>
			                      		<?php echo isset($error_message) && $error_message ? "<br/>".$error_message : '';?>
			                      	</label>
			                      	<div class="col-md-8">                                                               
			                      	<?php
				                      	$df_image = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
								    	$strimage = array();
								    	for($i=0; $i<1;$i++){
								    		$image = false;			    		
								    	?>
								    	<div class="col-lg-6">
								    		<input type="hidden" name="restaurant_file" value="<?php echo $image;?>"/>
								     		<div data-provides="fileupload" class="fileupload fileupload-new">
			                              		<div style="width: 200px; height: 150px;" class="fileupload-new thumbnail">
			                                  		<img src="<?php echo $image ? $image : $df_image;?>" alt="" />
			                              		</div>
			                              		<div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
			                          			<div>
					                               <span class="btn btn-white btn-file">
					                               <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select Image</span>
					                               <span class="fileupload-exists"><i class="fa fa-undo"></i>Change Image</span>
					                              	<input type="file" name="restaurant_file" class="default" /></span>
					                               </span>
			                              			<a data-dismiss="fileupload" class="btn btn-danger <?php echo $image ? '' : 'fileupload-exists';?>" <?php echo $image ? 'onclick="checkDelete(\''.$image.'\', \''.md5($image).'\', \''.$i.'\')"' : '';?> href="#"><i class="fa fa-trash"></i>Delete Image</a>
			                          			</div>
			                      				
			                          		</div>
			                          	</div>			    	
								    	<?php
								    	}
								    	?>					    	                         
			                      </div>                                            
			                  </div>
								<?php if(Yii::$app->user->identity->role == \backend\commons\models\entities\FitRoadUser::FIX_ROAD_ADMIN
								|| Yii::$app->user->identity->role == \backend\commons\models\entities\FitRoadUser::FIX_ROAD_USER_ROOT):?>
								<div class="form-group">
										<label class="col-sm-4" style="text-align: right">Is Active</label>
										<div class="col-md-2" style="">
										<?= $form->field($model, 'status')->checkbox(['label' => '', 'value'=>1, 'uncheckValue'=>0,'class'=>""]); ?>	<br/>
										</div>
									</div>
								<?php endif ?>
							</div>
							
					</div>
				</div>
			</div>
</div>
</div>


<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use api\commons\helpers\ApiHelpers;

/* @var $this yii\web\View */
/* @var $model backend\commons\models\entities\FitRoadRestaurant */

$this->title = $model->name;

\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
    'urlLevel1'=>['index'],
    'titleLevel1'=>Yii::t('backend', 'Restaurants Management'),
    'objectName'=>trim($model->name)
]);
$asset		= backend\assets\AppAsset::register($this);

$asset->css[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/jquery.fancybox.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-buttons.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.css';

$asset->js[] = 'theme/assets/global/plugins/fancybox/jquery.fancybox.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-buttons.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-media.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.js';



$this->registerJsFile(Yii::$app->homeUrl.'scripts/fancybox.js', ['depends' =>'yii\web\YiiAsset']);

//
//$this->registerJsFile(Yii::$app->homeUrl.'scripts/google-map-maker.js',
//    [ 'depends' => 'yii\web\YiiAsset']);

$this->registerJsFile('http://maps.googleapis.com/maps/api/js?key=AIzaSyBkasIPKFOEKKUKeKwxmTfCaEg4G6cWYO0&libraries=places&callback=initMap',
    [ 'depends' => 'yii\web\YiiAsset']);

$jsScripts = 'var places;var lat_ = '.$model->lat.';var    lng_ = '.$model->long.';
var place_id = "'.$model->place_id.'";var infoWindow;var marker; var map';
$jsScripts .= <<<JS

 function initMap(){
 var myLatLng = {lat: lat_, lng: lng_};

     map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: myLatLng
    });


    marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!',
        animation: google.maps.Animation.DROP
    });


     google.maps.event.addListener(marker, 'click', showInfoWindow);
     places = new google.maps.places.PlacesService(map);

     infoWindow = new google.maps.InfoWindow({
            content: document.getElementById('info-content')
        });

 }
 function toggleBounce() {
        if (marker.getAnimation() !== null) {
          marker.setAnimation(null);
        } else {
          marker.setAnimation(google.maps.Animation.BOUNCE);
        }
      }
      // Get the place details for a hotel. Show the information in an info window,
// anchored on the marker for the hotel that the user selected.
function showInfoWindow() {
    places.getDetails({placeId: place_id},
        function(place, status) {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                return;
            }
            infoWindow.open(map, marker);
            buildIWContent(place);

        });
}

// Load the place information into the HTML elements used by the info window.
function buildIWContent(place) {
    document.getElementById('iw-icon').innerHTML = '<img class="hotelIcon" ' +
        'src="' + place.icon + '"/>';
    document.getElementById('iw-url').innerHTML = '<b><a href="' + place.url +
        '">' + place.name + '</a></b>';
    document.getElementById('iw-address').textContent = place.vicinity;

    if (place.formatted_phone_number) {
        document.getElementById('iw-phone-row').style.display = '';
        document.getElementById('iw-phone').textContent =
            place.formatted_phone_number;
    } else {
        document.getElementById('iw-phone-row').style.display = 'none';
    }

    // Assign a five-star rating to the hotel, using a black star ('&#10029;')
    // to indicate the rating the hotel has earned, and a white star ('&#10025;')
    // for the rating points not achieved.
    if (place.rating) {
        var ratingHtml = '';
        for (var i = 0; i < 5; i++) {
            if (place.rating < (i + 0.5)) {
                ratingHtml += '&#10025;';
            } else {
                ratingHtml += '&#10029;';
            }
            document.getElementById('iw-rating-row').style.display = '';
            document.getElementById('iw-rating').innerHTML = ratingHtml;
        }
    } else {
        document.getElementById('iw-rating-row').style.display = 'none';
    }

    // The regexp isolates the first part of the URL (domain plus subdomain)
    // to give a short URL for displaying in the info window.
    if (place.website) {
        var fullUrl = place.website;
        var website = hostnameRegexp.exec(place.website);
        if (website === null) {
            website = 'http://' + place.website + '/';
            fullUrl = website;
        }
        document.getElementById('iw-website-row').style.display = '';
        document.getElementById('iw-website').textContent = website;
    } else {
        document.getElementById('iw-website-row').style.display = 'none';
    }
}
JS;

$this->registerJs($jsScripts, \yii\web\View::POS_HEAD, $key = null);

?>

<div class="fit-road-restaurant-view">
    <p>
        <?= Html::a('<i class="fa fa-edit"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i> Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add new'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('<i class="fa fa-list"></i> '.Yii::t('app', 'List Foods'), ['/food/manage/index','res_id'=>$model->id], ['class' => 'btn btn-warning']) ?>
    </p>
    <div class="row margin-bottom-30">
        <div class="col-md-6 ">
            <h1><?=$model->name;?></h1>
            <ul class="stars list-inline">
                <li>
                    <i class="fa fa-star"></i>
                </li>
                <li>
                    <i class="fa fa-star"></i>
                </li>
                <li>
                    <i class="fa fa-star"></i>
                </li>
                <li>
                    <i class="fa fa-star"></i>
                </li>
                <li>
                    <i class="fa fa-star-empty"></i>
                </li>
            </ul>
            <blockquote class="hero">
                <?=($model->description)?'<p>'.$model->description.'</p>':'<small>(None description)</small>'?>
            </blockquote>

            <ul class="restaurant-information list-unstyled margin-top-10 margin-bottom-10">
                <li class="fa-item col-md-4 col-sm-6">
                    <i class="fa fa-phone"></i><?=$model->phone?>
                </li>
                <li class="fa-item col-md-4 col-sm-6">
                    <i class="fa fa-fax"></i> <?=$model->fax?>
                </li>
                <li class="fa-item col-md-4 col-sm-6">
                    <i class="fa fa-link"></i> <?=($model->website)?'<a target="_blank" href="'.$model->website.'">'.$model->website.'</a>':''?>
                </li>
                <li class="fa-item col-md-12 col-sm-12">
                    <i class="fa fa-location-arrow"></i><?=$model->address?>
                </li>
            </ul>
            <!-- Blockquotes -->

        </div>
        <div class="col-md-6">
           <?php
          echo ($model->avatar && !empty($model->avatar))?
           Html::tag('a',Html::img(
               ApiHelpers::builtUrlImages(Yii::$app->params['restaurantsFinder'],
               $model->avatar,
                   Yii::$app->params['normalName']
               ),['style'=>'width:300px']) , [
               'href'=>ApiHelpers::builtUrlImages(Yii::$app->params['restaurantsFinder'],
                   $model->avatar,Yii::$app->params['normalName']),
               'class'=>'fancybox-thumbs',
               'data-fancybox-group'=>'thumb'
           ]):'<img src="http://www.placehold.it/400x250/EFEFEF/AAAAAA&amp;text=no+image"/>'
           ?>
        </div>

    </div>
    <hr/>
    <div class="col-md-12">
        <div class="col-md-10">
            <div id="map" class="gmaps">
            </div>
        </div>
    </div>
    <div style="display: none">
        <div id="info-content">
            <table>
                <tr id="iw-url-row" class="iw_table_row">
                    <td id="iw-icon" class="iw_table_icon"></td>
                    <td id="iw-url"></td>
                </tr>
                <tr id="iw-address-row" class="iw_table_row">
                    <td class="iw_attribute_name">Address:</td>
                    <td id="iw-address"></td>
                </tr>
                <tr id="iw-phone-row" class="iw_table_row">
                    <td class="iw_attribute_name">Telephone:</td>
                    <td id="iw-phone"></td>
                </tr>
                <tr id="iw-rating-row" class="iw_table_row">
                    <td class="iw_attribute_name">Rating:</td>
                    <td id="iw-rating"></td>
                </tr>
                <tr id="iw-website-row" class="iw_table_row">
                    <td class="iw_attribute_name">Website:</td>
                    <td id="iw-website"></td>
                </tr>
            </table>
        </div>
    </div>


</div>

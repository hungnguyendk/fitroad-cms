<?php

namespace backend\modules\restaurant\controllers;

use backend\commons\helpers\UtilHelper;
use backend\commons\models\entities\FitRoadUser;
use backend\modules\auth\controllers\AuthenticateController;
use Yii;
use backend\modules\food\models\FitRoadRestaurant;
use backend\commons\models\searchs\FitRoadRestaurant as FitRoadRestaurantSearch;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use joshtronic\GooglePlaces;

use yii\web\UploadedFile;
use api\commons\helpers\ApiHelpers;

/**
 * ManageController implements the CRUD actions for FitRoadRestaurant model.
 */
class ManageController extends AuthenticateController
{
    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false; 
   

    /**
     * Lists all FitRoadRestaurant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FitRoadRestaurantSearch();
        $dataProvider = $searchModel->searchcms(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FitRoadRestaurant model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FitRoadRestaurant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FitRoadRestaurant();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $uploadFile = UtilHelper::uploadAttachmentFile(
                Yii::getAlias('@publicImagesRestaurants'),
                null,
                'restaurant_file'
            );
            if($uploadFile && $uploadFile->errorStatus == FALSE){

                $model->avatar = $uploadFile->output[0];

            }else if($uploadFile && $uploadFile->errorStatus == TRUE) {
                return $this->render('create', [
                    'model' => $model,
                    'uploadFile'=>$uploadFile,

                ]);
            }

            if(!$model->save()){
                echo json_encode($model->getErrors());exit;
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FitRoadRestaurant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $uploadFile = UtilHelper::uploadAttachmentFile(
                Yii::getAlias('@publicImagesRestaurants'),
                null,
                'restaurant_file'
            );

            if($uploadFile && $uploadFile->errorStatus == FALSE){

                $model->avatar = $uploadFile->output[0];

            }else if($uploadFile && $uploadFile->errorStatus == TRUE) {
                return $this->render('create', [
                    'model' => $model,
                    'uploadFile'=>$uploadFile,
                ]);
            }

            if(!$model->save()){
                echo json_encode($model->getErrors());exit;
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FitRoadRestaurant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);

        if($model){

            $model->delete();

            if($model->avatar && !empty($model->avatar))
            try{
                @unlink(Yii::getAlias('@publicImagesRestaurants').'/'.$model->avatar);
                @unlink(Yii::getAlias('@publicImagesRestaurants').'/'.Yii::$app->params['thumbName'].$model->avatar);
                @unlink(Yii::getAlias('@publicImagesRestaurants').'/'.Yii::$app->params['normalName'].$model->avatar);
            }catch (Exception $e){
                Yii::error($e .'unlink restaurant image');
            }

        }


        return $this->redirect(['index']);
    }

    public function actionAjaxUpgradeVip(){
        $id =  \Yii::$app->request->post('modelId');

        if(Yii::$app->user->identity->role == FitRoadUser::FIX_ROAD_USER_ROOT
        ){
            $model = $this->findModel($id);
            $current = $model->is_vip;
            $model->is_vip = boolval($current) == TRUE? 0:1;

            $btnText = boolval($current) == TRUE? 'Vip Upgrade':'Vip Downgrade';
            $message = boolval($current) == TRUE? 'Vip Downgrade ':'Vip Upgrade';

            if($model->save()){

               echo json_encode(['status'=>true,
                   'btnText'=>$btnText,
                   'message'=>$message.' successful!']);
           }else{
               echo json_encode(['status'=>FALSE,'message'=>$message.' failed!']);
           }
           die();
       }

    }

    public function actionAjaxInsertRestaurant(){
        $placeId =  \Yii::$app->request->post('placeID');

        $res = FitRoadRestaurant::findOne(['place_id'=>$placeId]);
        if($res){
          echo json_encode(['status'=>FALSE,'message'=>'This restaurant have existed! Please try the other']);
            die();
        }

        $googlePlace = new GooglePlaces('AIzaSyBkasIPKFOEKKUKeKwxmTfCaEg4G6cWYO0');
        $googlePlace->placeid = $placeId;
        $googlePlace->rankby  = 'distance';
        $res               = $googlePlace->details();
        $results  = json_encode($res['result']);
        $results  = json_decode($results);

        $googleDataRestaurant = [
            'name'=>$results->name,
            'place_id'=>isset($results->place_id)?$results->place_id:null,
            'identifier'=>isset($results->id)?$results->id:null,
            'description'=>null,
            'phone	'=>isset($results->international_phone_number)?$results->international_phone_number:null,
            'fax'=>null,
            'website'=>isset($results->website)?$results->website:null,
            'address'=>isset($results->vicinity)?$results->vicinity:null,
            'lat'=>isset($results->geometry->location->lat)?$results->geometry->location->lat:null,
            'long'=>isset($results->geometry->location->lng)?$results->geometry->location->lng:null,
            'icon'=>isset($results->icon)?$results->icon:null,
            'type'=>(isset($results->type) && !empty($results->type))?json_encode($results->type):null,
            'google_data'=>json_encode([
                'id'=>isset($results->id)?$results->id:null,
                'openingHours'=>(isset($results->opening_hours) && $results->opening_hours->open_now)?'Open now':null,
                'placeId'=>isset($results->place_id)?$results->place_id:null,
                'reference'=>isset($results->reference)?$results->reference:null,
                'scope'=>'Local',
                'originScope'=>'Google',
                'priceLevel'=>isset($results->price_level)?$results->price_level:null,
                'photo'=>isset($results->photos)?$res['result']['photos']:[],
                'vicinity'=>isset($results->vicinity)?$results->vicinity:null,
                'formattedAddress'=>isset($results->formattedAddress)?$results->photo->formatted_address:null,
                'map_url'=>isset($results->url)?$results->url:null,
                'utc_offset'=>isset($results->utc_offset)?$results->utc_offset:null,
                'formatted_phone_number'=>isset($results->formatted_phone_number)?$results->formatted_phone_number:null,
                'international_phone_number'=>isset($results->international_phone_number)?$results->international_phone_number:null,
                'address_components'=>isset($results->address_components)?$results->address_components:null,
                'rating'=>isset($results->rating)?$results->rating:null,
                //'reviews'=>isset($details->reviews)?$details->reviews:null
            ])
        ];

        $restaurant = new FitRoadRestaurant();
        $restaurant->attributes = $googleDataRestaurant;

        try{
            $restaurant->save(FALSE);
            echo json_encode(['status'=>true,'message'=>Yii::t('backend','{res_name} inserted successfully!',[
                'res_name'=>$results->name
            ])]);
            die();

        }catch(\yii\db\Exception $e){
            \Yii::error('Error \'s name: '.$e->getName(), 'Save data from google');
            \Yii::error('Error \'s message: '.$e->getMessage(), 'Save data from google');
        }

    }

    public function actionGoogleSearch(){
        $model = [];

        $googlePlace = new GooglePlaces('AIzaSyBkasIPKFOEKKUKeKwxmTfCaEg4G6cWYO0');

        if(Yii::$app->request->post()){
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];
            $place_types = $_POST['place_types'];

            $googlePlace->location = array($lat, $lng);
            $googlePlace->radius = 8046; //hard-coded radius
            $googlePlace->types = $place_types;
            $nearby_places = $googlePlace->nearbySearch();
            die(var_dump(json_encode($nearby_places)));
        }

        return $this->render('google-place', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the FitRoadRestaurant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FitRoadRestaurant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FitRoadRestaurant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAjaxFindRestaurant(){

        $data = Yii::$app->request->post();
        $resp = [];
        $id 	= $data['id'];
        if(is_array($id) && !empty($id)){
            foreach($id as $i){
                $model = $this->findModel($i);

                $re = [
                    'address'=>($model)?$model->address:null,
                    'icon'=>($model && $model->avatar)?$model->avatar:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image',
                    'name'=>($model)?$model->name:null,
                    'description'=>($model->description)?$model->description:''
                ];
                $resp[] =$re;
            }
            echo json_encode(['status'=>true,'data'=>$resp]);
        }else{
            $model = $this->findModel((int)$id);
            if(!$model)
                echo json_encode(['status'=>FALSE]);

            echo json_encode(['status'=>TRUE,'data'=>['address'=>($model)?$model->address:null]]);
        }

        die();
    }
	function actionDelimg(){
		$data = Yii::$app->request->post();
		$id 	= $data['id'];		
		$model = FitRoadRestaurant::findOne($id);
        $img = $model->avatar;
		$model->avatar = null;

        if($model->save()){
            try{
                @unlink(Yii::getAlias('@publicImagesRestaurants').'/'.$img);
                @unlink(Yii::getAlias('@publicImagesRestaurants').'/'.Yii::$app->params['thumbName'].$img);
                @unlink(Yii::getAlias('@publicImagesRestaurants').'/'.Yii::$app->params['normalName'].$img);
            }catch (Exception $e){
                Yii::error($e .'unlink restaurant image');
            }
        }

		echo 1;
	}

    public function actionAjaxSearchRestaurantSelectBox(){
        $post =  Yii::$app->request->post();

        $queryString =    $post['query'];

        $id =  $post['id'];
        $query = \api\commons\models\entities\FitRoadRestaurant::find();

            $query->andOnCondition(['status'=>1])->limit(500);

        if($id && !empty($id)){
            $query->andOnCondition(['id'=>$id]);
            $models = $query->one();
            echo json_encode([
                'id'=>$models->id,
                'text'=>$models->name,
                'address'=>$models->address
            ]);
            die();
        }else{
            if($queryString && !empty($queryString)){
                $query->andWhere('`name` LIKE "%' . $queryString . '%"');
            }

            if(Yii::$app->user->identity->role != FitRoadUser::FIX_ROAD_USER_ROOT
            && Yii::$app->user->identity->role != FitRoadUser::FIX_ROAD_ADMIN){
                $query->andOnCondition('id in (select restaurant_id from fit_road_restaurant_has_managed_by_user where user_id='.Yii::$app->user->identity->id.')');

            }

            $models = $query->all();
            $data= [];

            if($models){
                foreach($models as $_item){
                    $res = [
                        'id'=>$_item->id,
                        'text'=>$_item->name,
                        'address'=>$_item->address
                    ];

                    $data[] = $res;
                }
            }
            echo json_encode(['status'=>TRUE,'restaurants'=>$data]);
            die();
        }

    }
}

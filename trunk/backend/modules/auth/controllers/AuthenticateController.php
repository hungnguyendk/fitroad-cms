<?php
/**
 * User: sangnguyen on  4/10/16 at 22:51
 * File name: AuthenticateController.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace backend\modules\auth\controllers;


use api\commons\forms\SentTokenRestPasswordForm;
use backend\commons\forms\ResetPasswordForm;
use backend\commons\models\entities\FitRoadUser;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\commons\forms\LoginForm;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class AuthenticateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'denyCallback' => function ($rule, $action) {
                //                    throw new \Exception('You are not allowed to access this page');
                //                },
                'rules' => [
                    [
                        'actions' => ['verify', 'error','reset-password','password-reset-request'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $module = Yii::$app->controller->module->id;
                            $controller = Yii::$app->controller->id;
                            $acion = Yii::$app->controller->action->id;
                            //                            $post = Yii::$app->request->post();

                            if(\Yii::$app->user->can($module)){
                                return true;
                            }elseif(\Yii::$app->user->can($module.$controller)){
                                return true;
                            }elseif(\Yii::$app->user->can($module.'-'.$controller.'-'.$acion)){
                                return true;
                            }

                            return FALSE;
                            // throw new NotFoundHttpException(Yii::t('backend','Page not found'));
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionVerify()
    {
        $this->layout = '//login';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if(Yii::$app->user->identity->role=='0001')	{
        		return $this->redirect(['/report/manage/index']);
        	}	
			else if(Yii::$app->user->identity->role == FitRoadUser::FIX_ROAD_MANAGER)	{
                return $this->redirect(['/restaurant/manage/index']);
            }
            return $this->goBack();
        } else {
            return $this->render('@backend/views/site/login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);

        } catch (InvalidParamException $e) {

            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * @var : Password reset request
     */
    public function actionPasswordResetRequest(){

        $form = new SentTokenRestPasswordForm();
        $form->email = (Yii::$app->request->post('email'))?Yii::$app->request->post('email'):null;
        if($form->validate()){
            $send = $form->sendEmail();

        }
        return $this->redirect(['/auth/authenticate/verify']);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->urlManager->createUrl(['auth/authenticate/verify']));
    }
}
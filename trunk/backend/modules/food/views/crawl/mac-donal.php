<?php
use yii\helpers\Html;
/**
 * User: sangnguyen on  4/23/16 at 10:55
 * File name: mac-donal.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */
\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
    'urlLevel1'=>['index'],
    'titleLevel1'=>Yii::t('backend', 'Foods Management'),
    'objectName'=>'MacDonal Crawling'
]);
$this->registerJsFile(Yii::$app->homeUrl.'scripts/mustache.js',[ 'depends' => 'yii\web\YiiAsset']);
$jsScripts = <<<JS
 var link = BASE_URL + '/fitroad-cms/trunk/food/crawl/mac-donal';
    //var link = BASE_URL + '/food/crawl/mac-donal';
function fadeItem() {
    $('ul#list-food li:hidden:first').delay(500).fadeIn(fadeItem);
}
$('.btn-crawling').on('click',function(){
$('#logCrawling').html('');
    Metronic.blockUI({
            target: '#dataInput',
            animate: true
        });
        jQuery.ajax({
            url: link,
            type: 'post',
            dataType : 'json',
            accepts: "application/json",
            data: {url:$('#urlCrawl').val(),_csrf:yii.getCsrfToken()},
            success: function (response) {
                if(response.status){
                    if(response.data != ''){
                         Metronic.unblockUI('#dataInput');

                         var examplesHTML = Mustache.to_html($('#food-output-template').html(), response.data);
                              $('#logCrawling').append(examplesHTML);

                         fadeItem();
                    }else{
                        $('#logCrawling').html('Empty data');
                    }
                }else{
                 Metronic.unblockUI('#dataInput');

                }

            }
        });
});


JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
?>
<div id="dataInput" class="col-md-12 col-sm-12">
    <div class="form-group">
        <label class="control-label col-md-3">Food Page url</label>
        <div class="col-md-6">
            <select class="form-control select2me " data-placeholder="Select..." id="urlCrawl" name="urlCrawl">
                <option value="http://www.mcdonalds.com/content/us/en/food/full_menu/sandwiches.html">Burgers & Sandwiches</option>
                <option value="http://www.mcdonalds.com/content/us/en/food/full_menu/chicken.html">Chicken & Fish</option>
                <option value="http://www.mcdonalds.com/content/us/en/food/full_menu/breakfast.html">Breakfast</option>
                <option value="http://www.mcdonalds.com/content/us/en/food/full_menu/salads.html">Salads</option>
                <option value="http://www.mcdonalds.com/content/us/en/food/full_menu/snacks_and_sides.html"> Snacks & Sides</option>
                <option value="http://www.mcdonalds.com/content/us/en/food/full_menu/beverages.html"> Beverages</option>
                <option value="http://www.mcdonalds.com/content/us/en/food/full_menu/mc_cafe.html"> McCafé</option>
                <option value="http://www.mcdonalds.com/content/us/en/food/full_menu/desserts_and_shakes.html"> Desserts & Shakes</option>

            </select>
        </div>
        <span class="input-group-btn">
            <?= Html::a('<i class="fa fa-floppy-o"></i> '.Yii::t('app', 'Start crawl'), 'javascript:;', ['class' => 'btn-crawling btn btn-success']) ?>
            </span>
    </div>
</div>
<hr/>
<label class="col-md-3 control-label"><h1>Logs</h1></label>
<div class="col-md-12 col-sm-12">
    <div class="row" id="logCrawling" >

    </div>
</div>
<script id="food-output-template" type="text/x-mustache">
<ul id="list-food" style="list-style-type:none">
     {{#items}}
        <li style="display:none"><div class="food-item-crawling-results col-sm-12 col-md-3">
            <div class="thumbnail" style="height: 300px;overflow-x: scroll;">
                <span class="badge badge-roundless {{class_alert}}">{{message_alert}}</span>
                <img src="{{image_src}}" style="width:80px" data-src="">
                <div class="caption">
                    <h3>Name: {{title}}</h3>
                    <h4>Nutrient: </h4>
                     <ul>
                        {{#nutrients}}
                            <li>{{value}}{{uom}} {{name}}</li>
                        {{/nutrients}}
                     </ul>
                </div>
            </div>
        </div></li>
 {{/items}}
</ul>

</script>

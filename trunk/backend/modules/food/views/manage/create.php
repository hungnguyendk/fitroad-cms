<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\modules\food\models\FitRoadRestaurant;
use api\commons\helpers\ApiHelpers;

/* @var $this yii\web\View */
/* @var $model backend\modules\food\models\FitRoadFood */

$this->title = 'Create Food';

$this->registerJsFile(Yii::$app->homeUrl.'scripts/modules/foods.js',[ 'depends' => 'yii\web\YiiAsset']);

\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
	'urlLevel1'=>['index'],
	'titleLevel1'=>Yii::t('backend', 'Foods Management')
]);
if(isset($uploadFile) && is_array($uploadFile->getErrors()) && !empty($uploadFile->getErrors())){
	$alert =  \yii\bootstrap\Alert::widget([
		'options' => [
			'class' => 'alert '.\Yii::$app->params['alertErrorStatusClass'],
		],
		'body' => \yii\helpers\BaseHtml::errorSummary($uploadFile),
	]);
}

$jsScripts = <<<JS
	var Foods = new FoodsModule();

	Foods.initRestaurantDropDownList();
	Foods.handleTrackingImageChange();

	jQuery('.btn-remove-image').on('click',function(){

			var previewId = jQuery(this).data('image-preview');

			jQuery('#'+previewId).attr('src','http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image');
			jQuery(this).addClass('fileupload-exists')
	})

JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);

?>

<div class="">
	<?php echo(isset($alert) && !empty($alert))?$alert:''; ?>
    <div class="row">
			<?php $form = ActiveForm::begin(['id'=>'updateForm',
				'layout' => 'horizontal',
				'options' => ['enctype'=>'multipart/form-data'],
				'fieldConfig' => [
			        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
			        'horizontalCssClasses' => [
			            'label' => 'col-sm-4',
			            'offset' => 'col-sm-offset-4',
			            'wrapper' => 'col-sm-8 marginBot',
			            'error' => '',
			            'hint' => '',
			        ],
			    ],
			]); ?>
			<div class="col-md-8">
			<div class="portlet">
				<input type="hidden" value="" id="imageChanged" name="imageChanged" />
			<div class="portlet-title">
				<div class="caption" style="font-size: 18pt">
					<span class="caption-subject">General Infomation</span> <span style="font-size: 12pt">* required</span>
				</div>
							
				<div class="actions pull-right">
					<button type="submit" id="savebtn" class="btn green">
						 Save
					</button>	
					<a href="<?= Yii::$app->homeUrl . "restaurant/manage/index"?>" id="savebtn" class="btn red">
						 Cancel
					</a>
										
				</div>
			</div>				
			</div>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-8"><!-- BEGIN Portlet PORTLET-->
							<div class="portlet">
							<div class="portlet-body from">
								<?= $form->field($model, 'name',['labelOptions'=>['label'=>'Name']]) ?>
								<?= $form->field($model, 'description',['labelOptions'=>['label'=>'Description']]) ?>
								<?= $form->field($model, 'price',['labelOptions'=>['label'=>'Price']]) ?>
								<?= $form->field($model, 'type',['labelOptions'=>['label'=>'Type']])
								->dropDownList(['1'=>'Big Meal','2'=>'Snack'],['class'=>"form-control"]) ?>

								<?= $form->field($model, 'restaurant_id',[
									'labelOptions'=>['label'=>'Restaurant']])
									->textInput([
									'class'=>'form-control select2me',
									'multiple'=>'multiple',
										'value'=>''
								]) ?>

								<?= $form->field($model, 'calories',['labelOptions'=>['label'=>'Calories']])->textInput(['class'=>'form-control classnumber']) ?>
								<?= $form->field($model, 'weight',['labelOptions'=>['label'=>'Weight']])->textInput(['class'=>'form-control classnumber']) ?>
								</div>

								<div class="form-group <?php echo isset($error_upload) && $error_upload ? 'has-error' : '';?>">
									<label class="control-label col-md-4">Photo<br/>
										<?php echo isset($error_message) && $error_message ? "<br/>".$error_message : '';?>
									</label>
									<div class="col-md-8">
										<?php
										$imageId = null;

										$df_image = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
										$strimage = json_decode($images,true);
										//die(var_dump($images));
										for($i=0; $i<4;$i++){
											if(isset($strimage[$i]) && !empty($strimage[$i])){
												$image = ($strimage[$i]['url'] && !empty($strimage[$i]['url']))
													? ApiHelpers::builtUrlImages(Yii::$app->params['foodsFinder'],$strimage[$i]['url'],Yii::$app->params['normalName']):'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
												$imageId =  $strimage[$i]['id'];
											}else{
												$image = false;
											}
											?>
											<div class="col-lg-6" style="height: 232px;">
												<input accept="image/*"  type="hidden" name="image_name[]" value="<?php echo $image;?>"/>
												<div data-provides="fileupload" class="fileupload fileupload-new">
													<div style="width: 200px; height: 150px;" class="fileupload-new thumbnail">
														<img id="preview<?=$i?>" src="<?php echo $image ? $image : $df_image;?>" alt="" />
													</div>
													<div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
													<div>
					                               <span class="btn btn-white btn-file">
					                               <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select Image</span>
					                               <span class="fileupload-exists"><i class="fa fa-undo"></i>Change Image</span>
					                              	<input accept="image/*"  id="<?=($imageId)?$imageId:null?>" type="file" name="image_name[]" class="image-food-input default" /></span>
														</span>
														<a data-image-preview="preview<?=$i?>" data-dismiss="fileupload" class="btn btn-danger btn-remove-image <?php echo $image ? '' : 'fileupload-exists';?>"  href="#"><i class="fa fa-trash"></i>Delete Image</a>
													</div>

												</div>
											</div>
											<?php
											$imageId = null;
										}
										?>
									</div>
								</div>
								<?php if(Yii::$app->user->identity->role == \backend\commons\models\entities\FitRoadUser::FIX_ROAD_ADMIN
								|| Yii::$app->user->identity->role == \backend\commons\models\entities\FitRoadUser::FIX_ROAD_USER_ROOT):?>
								<div class="form-group">
										<label class="col-sm-4" style="text-align: right">Is Active</label>
										<div class="col-md-2" style="">
										<?= $form->field($model, 'status')->checkbox(['label' => '', 'value'=>1, 'uncheckValue'=>0,'class'=>""]); ?>	<br/>
										</div>
									</div>
								<?php endif ?>
							</div>
							
					</div>
					<div class="col-md-4">
						<div id="boxLogs" style="overflow: scroll; height: 500px;" >
							<div class="todo-tasklist" id="logList">

							</div>
						</div>
					</div>
			</div>
</div>
		<script language="javascript">
			function getName(filename) {

				var lastIndex = filename.lastIndexOf("\\");
				if (lastIndex >= 0) {
					filename = filename.substring(lastIndex + 1);
				}
				return  filename;
			}

		</script>
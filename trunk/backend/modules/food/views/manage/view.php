<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use api\commons\helpers\ApiHelpers;

/* @var $this yii\web\View */
/* @var $model backend\modules\food\models\FitRoadFood */

$this->title = $model->name;
//$this->params['breadcrumbs'][] = ['label' => 'Fit Road Foods', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;

\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
    'urlLevel1'=>['index'],
    'titleLevel1'=>Yii::t('backend', 'Foods Management'),
    'objectName'=>trim($model->name)
]);
$asset		= backend\assets\AppAsset::register($this);

$asset->css[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/jquery.fancybox.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-buttons.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.css';

$asset->js[] = 'theme/assets/global/plugins/fancybox/jquery.fancybox.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-buttons.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-media.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.js';


$this->registerJsFile(Yii::$app->homeUrl.'scripts/fancybox.js', ['depends' =>'yii\web\YiiAsset']);

$this->registerJsFile(Yii::$app->homeUrl.'scripts/myjs.js',[ 'depends' => 'yii\web\YiiAsset']);
?>
<div class="fit-road-food-view">
    <p>
        <?= Html::a('<i class="fa fa-download"></i> Clone', ['create', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('<i class="fa fa-edit"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i> Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add new'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="food-item-crawling-results col-sm-12 col-md-12">
        <div class="thumbnail" style="height: 500px;overflow-x: scroll;">
            <span class="badge badge-roundless
            <?= \backend\commons\helpers\UtilHelper::buildClassForCalories($model->calories)?>">
                <?= 'Calories:'. $model->calories?>
            </span>
            <?php
            $html = '';
            $data = $model->fitRoadGalleryImages;
            if(is_array($data) && !empty($data)){
                $html .='<div class="col-md-12 blog-sidebar" style="margin-top: 15px">
                                    <ul class="list-inline blog-images">';
                $i = 0;
                foreach($data as $value){
                    $i++;

                    if(isset($value['url']) && !empty($value['url'])){
                        $html .= '<li style="display: inline-block;height: 100px;width: 100px"><a href="'.ApiHelpers::builtUrlImages(
                                Yii::$app->params['foodsFinder'],$value['url'],
                                Yii::$app->params['thumbName']).'" class="fancybox-thumbs"  data-fancybox-group="thumb">
                        <img src="'.ApiHelpers::builtUrlImages(Yii::$app->params['foodsFinder'],$value['url']).'" alt="" style="height:100%;width:100%">
                        </a></li>';
                    }
                }

                $html .='</ul>
                                </div>';

                echo $html;
            }else{
                echo ($model->out_side_icon_url)?'<a data-fancybox-group="thumb" class="fancybox-thumbs"
                href="'.$model->out_side_icon_url.'">
                    <img src="'.$model->out_side_icon_url.'" alt="" style="width:100px;">
                 </a>':null;

            }
            ?>
            <div class="col-md-12">
                <div class="caption">
                    <h2>Name: <?=$model->name?></h2>
                    <ul class="list-inline">
                        <li><h4>Restaurant:&nbsp;<?=($model->restaurant)?$model->restaurant->name:null ?></h4>&nbsp;
                            <i class="fa fa-location-arrow"></i><?=($model->restaurant)
                                ?$model->restaurant->address:null ?></li>
                    </ul>
                    <h4>Nutrients: </h4>
                    <ul>
                        <li>Weight: <?=$model->weight?></li>
                        <?php
                            if($model->nutrients && !empty($model->nutrients)){
                                $nutrients = json_decode($model->nutrients);
                                if(is_array($nutrients)){
                                    foreach($nutrients as $n){
                                        echo '<li>'.$n->name.':&nbsp;'.$n->value.'('.$n->uom_description.')</li>';
                                    }
                                }
                            }
                        ?>
                    </ul>
                </div>
            </div>

        </div>
    </div>


</div>

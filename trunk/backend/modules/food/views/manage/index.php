<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use api\commons\helpers\ApiHelpers;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\food\models\FitRoadFoodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Foods Management';
$this->params['breadcrumbs'][] = $this->title;
$asset		= backend\assets\AppAsset::register($this);
/* @var $this yii\web\View */

$asset->css[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/jquery.fancybox.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-buttons.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.css';

$asset->js[] = 'theme/assets/global/plugins/fancybox/jquery.fancybox.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-buttons.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-media.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/helpers/jquery.fancybox-thumbs.js';


$this->registerJsFile(Yii::$app->homeUrl.'scripts/fancybox.js', ['depends' =>'yii\web\YiiAsset']);

$this->registerJsFile(Yii::$app->homeUrl.'scripts/myjs.js',[ 'depends' => 'yii\web\YiiAsset']);
?>
<div class="fit-road-food-index">
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <form id="formIndex">
                <?php
                echo \nterms\pagesize\PageSize::widget([
                    'label' => false,
                    'template'=>'Show {list} records',
                    'options'=> ['class'=>'form-control input-xsmall input-inline','id'=>'perpage']
                ]);

                ?>
            </form>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="dataTables_length pull-right">
                <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add new'), ['create'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div><hr/><br/>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'layout'=>"<div class='pull-left'>{summary}</div>\n{items}\n{pager}\n",
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{clone} {view} {update} {delete} ',
                'buttons' => [
                    //                    'viewEvent'=>function($url, $model){
                    //                        $icon = '<span class="glyphicon glyphicon-picture"></span>';
                    //                        $url = Url::to(['/photo/event','event_id'=>$model->id]);
                    //                        return Html::a($icon,$url,['title'=>'View photo of event']);
                    //                    }
                    'clone'=>function($url,$model){
                        $icon = '<span style="" class="fa fa-download"></span>';
                        $url = \yii\helpers\Url::to(['create','id'=>$model->id]);
                        return Html::a($icon,$url,['title'=>'Clone food\'s info']);
                    }
                ]
            ],
//            [
//                'attribute' => 'id',
//                'headerOptions' => ["class"=>'sort-numerical'],
//                'filterInputOptions' => [
//                    'class'       => 'form-control',
//                    'placeholder' => 'Search Id'
//                ]
//            ],
            [
                'format' => 'html',
                'filter'=>false,
                'label'=>'Image',
                'headerOptions' => ['width' => '60px'],
                'content' => function ($searchModel, $index, $widget) {
                    $html = '';
                    $data = $searchModel->fitRoadGalleryImages;
                    if(is_array($data) && !empty($data)){
                        $html .='<div class="col-md-8 blog-sidebar">
                                    <ul class="list-inline blog-images">';
                        $i = 0;
                        foreach($data as $value){
                            $i++;

                            if(isset($value['url']) && !empty($value['url'])){
                                $html .= '<li><a href="'.ApiHelpers::builtUrlImages(Yii::$app->params['foodsFinder'],$value['url'],Yii::$app->params['thumbName']).'" class="fancybox-thumbs"  data-fancybox-group="thumb">
                                        <img src="'.ApiHelpers::builtUrlImages(Yii::$app->params['foodsFinder'],$value['url']).'" alt="" style="width:50px;">
                                        </a></li>';
                                if($i == 2){
                                    $html .='<br/>';
                                }
                            }

                        }

                        $html .='</ul>
                                </div>';

                        return $html;
                    }else{
                        return ($searchModel->out_side_icon_url)?'<a data-fancybox-group="thumb" class="fancybox-thumbs" href="'.$searchModel->out_side_icon_url.'">
                                    <img src="'.$searchModel->out_side_icon_url.'" alt="" style="width:100px;">
                                </a>':null;

                    }

                }
            ],
            [
                'attribute'=>'restaurant_name',
                //'header'=>'Event title',
                // 'headerOptions'=>['class'=>$classSortEvent],
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Search name or address'
                ],
                'content'=>function ($model, $key, $index, $column){
                    if($model->restaurant){
                        return $model->restaurant->name.'<br/><i class="fa fa-location-arrow"></i><small>'.$model->restaurant->address.'</small>';
                    }
                }
            ],


            [
                'attribute' => 'name',
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Search name'
                ]
            ],

//            [
//                'attribute' => 'price',
//                'headerOptions' => ["class"=>'sort-numerical'],
//                'filterInputOptions' => [
//                    'class'       => 'form-control',
//                    'placeholder' => 'Search price'
//                ]
//            ],

            // 'description',

            [
                'attribute' => 'calories',
                'headerOptions' => ["class"=>'sort-numerical'],
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Search calories'
                ]
            ],
            [
                'attribute' => 'weight',
                'headerOptions' => ["class"=>'sort-numerical'],
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Search weight'
                ]
            ],

            [
                'attribute'=>'type',
                'header'=>'Type',
                'filter'=>['1'=>'Big meal','2'=>'Snack'],
                'content'=> function ($model) {
                    return ($model->type == 1)?\Yii::t('backend','Big meal'):\Yii::t('backend','Snack');
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter'=>false,
                'label'=>'Active',
                'headerOptions' => ['width' => '60px'],
                'value' => function ($searchModel, $index, $widget) {
                    return Html::checkbox('status[]', $searchModel->status, ['value' => $index, 'disabled' => true]);

		        },
			],
            [
                'attribute'=>'created_at',
                'filter'=>FALSE,
                'value'=> function ($model) {
                    return \Yii::$app->formatter->asDate($model->created_at,'php:D d M Y');
                },
            ],

            // 'created_at',
            // 'updated_at',


        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php
/**
 * User: sangnguyen on  4/23/16 at 10:47
 * File name: CrawlController.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace backend\modules\food\controllers;

use backend\commons\helpers\UtilHelper;
use backend\modules\food\models\FitRoadCategory;
use backend\modules\food\models\FitRoadFood;
use Yii;
use backend\modules\auth\controllers\AuthenticateController;
use Sunra\PhpSimple\HtmlDomParser;

class CrawlController extends AuthenticateController
{

   public function actionTest(){

       $html=	HtmlDomParser::file_get_html('http://www.mcdonalds.com/content/us/en/food/full_menu/sandwiches.html');

       // Find all images
       $p1 = $html->find('div.nut_product_item');

       foreach($p1 as $element){
           $div = $element->find('div.product_item',0);

            $item = $div->find('div');
            $image = $item[0]->children(0);
            $title = $item[0]->children(1);


            $data['id'][]   =   $item[0]->id;
            $data['image_src'][] = $image->src;
            $data['title'][] = $title->plaintext;
       }
       die();
       // echo json_encode($data);

   }
    public function actionMacDonal(){
        // Create DOM from URL or file
        $data = [];
        $url = null;
        $macDonalDomain = 'http://www.mcdonalds.com';

        if(Yii::$app->request->isPost){

            $url = Yii::$app->request->post("url");
            $html=	HtmlDomParser::file_get_html($url);

            $p1 = $html->find('div.nut_product_item');

            if(is_array($p1) && !empty($p1)){
                foreach($p1 as $element){
                    $saveItem = true;
                    $div = $element->find('div.product_item',0);

                    $item = $div->find('div');

                    $ajaxUrl = 'http://www.mcdonalds.com/wws/json/getItemSummary.htm?country=us&language=en&showLiveData=true&item=%s&itemAttributeId=17|18|264&nutritionAttributeId=2|21|22|11|7';

                    $response = UtilHelper::getDataUrlByGetMethod(sprintf($ajaxUrl,$item[0]->id));
                    $response = json_decode($response);

                    if(isset($response->item->nutrient_facts->nutrient) && !empty($response->item->nutrient_facts->nutrient)){
                        $nutrients= [];
                        foreach($response->item->nutrient_facts->nutrient as $nutrient){
                            if($nutrient->name == 'Calories'){
                                $caloriesValue = $nutrient->value;
                            }
                            if($nutrient->name == 'Total Fat'){
                                $weight = $nutrient->value;
                            }
                            $n = [
                                'uom_description'=>$nutrient->uom_description,
                                'name'=>$nutrient->name,
                                'value'=>$nutrient->value,
                                'uom'=>(!is_object($nutrient->uom))?$nutrient->uom:''
                            ];
                            $nutrients[] = $n;
                        }
                    }

                    $image = $item[0]->children(0);
                    $title = $item[0]->children(1);

                    $macdonal =[
                        'nutrients'=>(isset($nutrients))?$nutrients:[],
                        'id'=>$item[0]->id,
                        'image_src'=>$macDonalDomain.$image->src,
                        'title'=>$title->plaintext,
                        'category'=>(isset($response->item->default_category->category->name))?$response->item->default_category->category->name:null
                    ];

                    if(isset($response->item->default_category->category->name)){
                        $category = FitRoadCategory::findOne(['name'=>$response->item->default_category->category->name]);
                        if(!$category){
                            $category = new FitRoadCategory();
                            $category->name = $response->item->default_category->category->name;
                            $category->status =1;
                            $category->restaurant_id    =   10110;
                            $category->save(FALSE);
                        }
                    }


                    $fitRoadFoodData = [
                        'identifier'=>$item[0]->id,
                        'out_side_icon_url'=>$macDonalDomain.$image->src,
                        'restaurant_id'=>'10110',
                        'category_id'=>($category && !empty($category->id))?$category->id:null,
                        'name'=>$title->plaintext,
                        'type'=>($category && (strpos($category->name,'Snacks') !== false || strpos($category->name,'Salads') !== false))?2:1,
                        'description'=>$title->plaintext,
                        'calories'=>isset($caloriesValue)?$caloriesValue:0,
                        'weight'=>isset($weight)?$weight:0,
                        'status'=>1,
                        'nutrients'=>(isset($nutrients) && !empty($nutrients))?json_encode($nutrients):null,
                    ];

                    $foodModel = new FitRoadFood();
                    $foodModel->attributes = $fitRoadFoodData;

                    if(!$foodModel->save()){
                        $saveItem = FALSE;
                    }

                    if($saveItem){
                        $macdonal['class_alert'] = 'badge-success';
                        $macdonal['message_alert'] = 'Saved';
                    }else{
                        $macdonal['class_alert'] = 'badge-warning';
                        $macdonal['message_alert'] = 'Existed';
                    }

                    $data[] = $macdonal;
                }
            }

            echo json_encode(['status'=>true,'data'=>['items'=>$data]]);
            die();
        }

        return $this->render('mac-donal');
    }
}
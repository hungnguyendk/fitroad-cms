<?php

namespace backend\modules\food\controllers;
use backend\commons\helpers\UtilHelper;
use backend\modules\auth\controllers\AuthenticateController;
use Sunra\PhpSimple\HtmlDomParser;
use Yii;
use backend\modules\food\models\FitRoadFood;
use backend\modules\food\models\FitRoadFoodSearch;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\modules\food\models\FitRoadGalleryImage;
use api\commons\helpers\ApiHelpers;
/**
 * FoodController implements the CRUD actions for FitRoadFood model.
 */
class ManageController extends AuthenticateController
{
    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false;


    /**
     * Lists all FitRoadFood models.
     * @return mixed
     */
    public function actionIndex($res_id=null)
    {
        $searchModel = new FitRoadFoodSearch();
		$andConditions = [];
		if($res_id && !empty($res_id)){
			$andConditions = ['restaurant_id'=>(int)$res_id];
		}
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$andConditions);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FitRoadFood model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FitRoadFood model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate($id=null)
    {
		$clone = FALSE;
		$images = [];
		$imagesUpload = [];
		if($id && !empty($id)){
			$model = $this->findModel($id);
			$clone = true;
			$searchModel = new FitRoadFoodSearch();
			$images = $searchModel->getImages($id);
		}else{
			$model = new FitRoadFood();
		}

		if (Yii::$app->request->post()) {
			$restaurants = Yii::$app->request->post('FitRoadFood')['restaurant_id'];
			$imageChanged = json_decode(Yii::$app->request->post('imageChanged'));

			$restaurants = explode(',',$restaurants);

			if(is_array($restaurants) && !empty($restaurants)){
				$i=0;
				foreach($restaurants as $resId){
					$model = new FitRoadFood();
					$model->attributes = Yii::$app->request->post('FitRoadFood');

					$model->restaurant_id = $resId;

					if(!$model->save()){
						return $this->render('create', [
							'model' => $model,
							'images'=>json_encode($images)
						]);
					}

					if($i == 0 && $clone == FALSE){
						$uploadFile = UtilHelper::uploadAttachmentFile(
							Yii::getAlias('@publicImagesFoods'),
							null,
							'image_name'
						);

						if($uploadFile && $uploadFile->errorStatus == FALSE){
							$imagesUpload = $uploadFile->output;
							foreach($uploadFile->output as $image){
								$imageModel = new FitRoadGalleryImage();
								$imageModel->user_id = yii::$app->user->id;
								$imageModel->food_id = $model->id;
								$imageModel->url = $image;
								$imageModel->type = 'food';
								$imageModel->status = 1;

								if(!$imageModel->save()){
									echo json_encode($imageModel->getErrors());exit;
								}
							}
						}else if($uploadFile && $uploadFile->errorStatus == true){
							//die(var_dump($uploadFile));
							return $this->render('create', [
								'model' => $model,
								'uploadFile'=>$uploadFile,
								'images'=>json_encode($images)
							]);
						}
					}else if($i !=0 && $clone == FALSE){
						if(is_array($imagesUpload) && !empty($imagesUpload)){
							foreach($imagesUpload as $im){

								$this->downloadImageAndSave($model,$im);

							}
						}
					}elseif($i == 0 && $clone == TRUE){
						$uploadFile = UtilHelper::uploadAttachmentFile(
							Yii::getAlias('@publicImagesFoods'),
							null,
							'image_name'
						);

						if($uploadFile && $uploadFile->errorStatus == FALSE){
							$imagesUpload = $uploadFile->output;
							foreach($uploadFile->output as $image){
								$imageModel = new FitRoadGalleryImage();
								$imageModel->user_id = yii::$app->user->id;
								$imageModel->food_id = $model->id;
								$imageModel->url = $image;
								$imageModel->type = 'food';
								$imageModel->status = 1;

								if(!$imageModel->save()){
									echo json_encode($imageModel->getErrors());exit;
								}
							}
						}else if($uploadFile && $uploadFile->errorStatus == true){
							//die(var_dump($uploadFile));
							return $this->render('create', [
								'model' => $model,
								'uploadFile'=>$uploadFile,
								'images'=>json_encode($images),

							]);
						}
					}elseif($i !=0 && $clone == TRUE){
						if(is_array($imagesUpload) && !empty($imagesUpload)){
							foreach($imagesUpload as $im){

								$this->downloadImageAndSave($model,$im);

							}
						}
					}

					$images = ArrayHelper::index($images, 'id');

					if(is_array($imageChanged) && !empty($imageChanged)){
						foreach($imageChanged as $imC){
							unset($images[$imC->id]);
						}
					}

					$images = array_values($images);

					if(is_array($images) && !empty($images)){
						foreach($images as $im){
								$this->downloadImageAndSave($model,$im['url']);
						}
					}


					$i++;

				}
			}

			return $this->redirect(['view', 'id' => $model->id]);
		}else{
			return $this->render('create', [
				'model' => $model,
				'images'=>json_encode($images),
				'dataDefault'=>($model->restaurant)?json_encode(['id'=>$model->restaurant_id,'text'=>$model->restaurant->name]):'{}'
			]);
		}

    }

	private function downloadImageAndSave($model,$urlImage){
		$fileName = UtilHelper::downloadImageFromLink(Yii::getAlias('@publicImagesFoods').'/',
			ApiHelpers::builtUrlImages(Yii::$app->params['foodsFinder'],$urlImage));

		$imageModel = new FitRoadGalleryImage();
		$imageModel->user_id = yii::$app->user->id;
		$imageModel->food_id = $model->id;
		$imageModel->url = $fileName;
		$imageModel->type = 'food';
		$imageModel->status = 1;

		if(!$imageModel->save()){
			echo json_encode($imageModel->getErrors());exit;
		}
	}
    /**
     * Updates an existing FitRoadFood model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$model->scenario	=	'update';
		$searchModel = new FitRoadFoodSearch();
		$images = $searchModel->getImages($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {

			$imageChanged = json_decode(Yii::$app->request->post('imageChanged'));

			$uploadFile = UtilHelper::uploadAttachmentFile(
				Yii::getAlias('@publicImagesFoods'),
				null,
				'image_name',
				true,
				true
			);
			if($uploadFile && $uploadFile->errorStatus == FALSE){
				foreach($imageChanged as $item){
					$imageModel = (new FitRoadGalleryImage)->findOne(['id'=>$item->id]);

					if(!$imageModel){
						$imageModel = new FitRoadGalleryImage;
						$imageModel->user_id = yii::$app->user->id;
						$imageModel->food_id = $model->id;
						$imageModel->type = 'food';
						$imageModel->status = 1;
					}

					foreach($uploadFile->output  as $key=>$image){
						if($image['oldName'] == $item->name){
							$imageModel->url = $image['name'];
							unset($uploadFile->output[$key]);
						}
					}
					if(!$imageModel->save(FALSE)){
						echo json_encode($imageModel->getErrors());exit;
					}
				}
			}else if($uploadFile && $uploadFile->errorStatus == true){
				//die(var_dump($uploadFile));
				return $this->render('update', [
					'model' => $model,
					'uploadFile'=>$uploadFile,
					'images'=> json_encode($images),
					'dataDefault'=>json_encode(['id'=>$model->restaurant_id,'text'=>$model->restaurant->name])
				]);
			}
			return $this->redirect(['view', 'id' => $model->id]);
		}else{
			return $this->render('update', [
				'model' => $model,
				'images'=> json_encode($images),
				'dataDefault'=>json_encode(['id'=>$model->restaurant_id,'text'=>$model->restaurant->name])
			]);
		}

    }

    /**
     * Deletes an existing FitRoadFood model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

		$imageModel = (new FitRoadGalleryImage)->findAll(['food_id'=>$id,'type'=>'food']);

		if($imageModel && is_array($imageModel) && !empty($imageModel)){
			foreach($imageModel as $image){
				$url = $image->url;

				try{
					@unlink(Yii::getAlias('@publicImagesFoods').'/'.$url);
					@unlink(Yii::getAlias('@publicImagesFoods').'/'.Yii::$app->params['thumbName'].$url);
					@unlink(Yii::getAlias('@publicImagesFoods').'/'.Yii::$app->params['normalName'].$url);
				}catch (Exception $e){
					Yii::error($e .'unlink food image');
				}
			}

		}

		$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	/**
     * Finds the FitRoadFood model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FitRoadFood the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FitRoadFood::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	

	
	function actionDelimg(){
		$searchModel = new FitRoadFoodSearch();
		$data = Yii::$app->request->post();
		$img 	= $data['img'];
		$id 	= $data['id'];
		$imageModel = (new FitRoadGalleryImage)->findOne(['id'=>$id]);


		if($imageModel ){
			$url = $imageModel->url;
			$imageModel->url = null;
			$imageModel->save();
			try{
				@unlink(Yii::getAlias('@publicImagesFoods').'/'.$url);
				@unlink(Yii::getAlias('@publicImagesFoods').'/'.Yii::$app->params['thumbName'].$url);
				@unlink(Yii::getAlias('@publicImagesFoods').'/'.Yii::$app->params['normalName'].$url);
			}catch (Exception $e){
				Yii::error($e .'unlink food image');
			}
		}

		echo 1;
	}

}

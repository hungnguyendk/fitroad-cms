<?php

namespace backend\modules\food\models;

/**
 * This is the ActiveQuery class for [[FitRoadGalleryImage]].
 *
 * @see FitRoadGalleryImage
 */
class FitRoadGalleryImageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return FitRoadGalleryImage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FitRoadGalleryImage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

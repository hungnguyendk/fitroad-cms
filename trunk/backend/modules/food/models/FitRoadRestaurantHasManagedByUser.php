<?php

namespace backend\modules\food\models;

use backend\commons\models\entities\FitRoadUser;
use Yii;

/**
 * This is the model class for table "fit_road_restaurant_has_managed_by_user".
 *
 * @property integer $id
 * @property integer $restaurant_id
 * @property integer $user_id
 * @property string $role
 * @property integer $status
 * @property string $created_at
 *
 * @property FitRoadRestaurant $restaurant
 * @property FitRoadUser $user
 */
class FitRoadRestaurantHasManagedByUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_restaurant_has_managed_by_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'user_id'], 'required'],
            [['restaurant_id', 'user_id', 'status'], 'integer'],
            [['role'], 'string'],
            [['created_at'], 'string', 'max' => 45],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadRestaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'restaurant_id' => Yii::t('backend', 'Restaurant ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'role' => Yii::t('backend', 'Role'),
            'status' => Yii::t('backend', 'Status'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(FitRoadRestaurant::className(), ['id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(FitRoadUser::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return FitRoadRestaurantHasManagedByUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FitRoadRestaurantHasManagedByUserQuery(get_called_class());
    }
}

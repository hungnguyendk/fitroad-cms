<?php

namespace backend\modules\food\models;

use Yii;
use api\commons\models\entities\FitRoadUser;
use backend\modules\food\models\FitRoadFood;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "fit_road_gallery_image".
 *
 * @property integer $id
 * @property integer $food_id
 * @property integer $user_id
 * @property string $url
 * @property integer $status
 * @property string $type
 * @property integer $is_current
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $message
 *
 * @property FitRoadUser $user
 * @property FitRoadFood $food
 */
class FitRoadGalleryImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_gallery_image';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['food_id', 'user_id', 'status', 'is_current', 'created_at', 'updated_at'], 'integer'],
            [['type'], 'required'],
            [['type'], 'string'],
            [['url'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 115],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['food_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadFood::className(), 'targetAttribute' => ['food_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'food_id' => 'Food ID',
            'user_id' => 'User ID',
            'url' => 'Url',
            'status' => 'Status',
            'type' => 'Type',
            'is_current' => 'Is Current',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'message' => 'Message',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(FitRoadUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFood()
    {
        return $this->hasOne(FitRoadFood::className(), ['id' => 'food_id']);
    }

    /**
     * @inheritdoc
     * @return FitRoadGalleryImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FitRoadGalleryImageQuery(get_called_class());
    }
}

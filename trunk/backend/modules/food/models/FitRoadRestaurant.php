<?php

namespace backend\modules\food\models;

use Yii;

/**
 * This is the model class for table "fit_road_restaurant".
 *
 * @property integer $id
 * @property string $avatar
 * @property string $place_id
 * @property string $identifier
 * @property string $name
 * @property string $description
 * @property string $phone
 * @property string $fax
 * @property string $website
 * @property string $lat
 * @property string $long
 * @property string $address
 * @property string $icon
 * @property string $type
 * @property integer $status
 * @property string $google_data
 * @property integer $rating
 * @property integer $calories
 * @property integer $is_vip
 * @property integer $is_local
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property FitRoadCategory[] $fitRoadCategories
 * @property FitRoadFood[] $fitRoadFoods
 * @property FitRoadRestaurantHasManagedByUser[] $fitRoadRestaurantHasManagedByUsers
 * @property FitRoadRestaurantVisitReport[] $fitRoadRestaurantVisitReports
 */
class FitRoadRestaurant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_restaurant';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'name'], 'required'],
            [['description', 'google_data'], 'string'],
            [['status', 'rating', 'calories', 'is_vip', 'is_local', 'created_at', 'updated_at'], 'integer'],
            [['avatar', 'identifier', 'address', 'type'], 'string', 'max' => 255],
            [['place_id'], 'string', 'max' => 155],
            [['name', 'website'], 'string', 'max' => 115],
            [['phone'], 'string', 'max' => 15],
            [['fax'], 'string', 'max' => 25],
            [['lat', 'long'], 'string', 'max' => 45],
            [['icon'], 'string', 'max' => 225],
            [['place_id'], 'unique'],
            [['identifier'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'place_id' => Yii::t('backend', 'Place ID'),
            'identifier' => Yii::t('backend', 'Identifier'),
            'name' => Yii::t('backend', 'Name'),
            'description' => Yii::t('backend', 'Description'),
            'phone' => Yii::t('backend', 'Phone'),
            'fax' => Yii::t('backend', 'Fax'),
            'website' => Yii::t('backend', 'Website'),
            'lat' => Yii::t('backend', 'Lat'),
            'long' => Yii::t('backend', 'Long'),
            'address' => Yii::t('backend', 'Address'),
            'icon' => Yii::t('backend', 'Icon'),
            'type' => Yii::t('backend', 'Type'),
            'status' => Yii::t('backend', 'Status'),
            'google_data' => Yii::t('backend', 'Google Data'),
            'rating' => Yii::t('backend', 'Rating'),
            'calories' => Yii::t('backend', 'Calories'),
            'is_vip' => Yii::t('backend', 'Is Vip'),
            'is_local' => Yii::t('backend', 'Is Local'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadCategories()
    {
        return $this->hasMany(FitRoadCategory::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadFoods()
    {
        return $this->hasMany(FitRoadFood::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadRestaurantHasManagedByUsers()
    {
        return $this->hasMany(FitRoadRestaurantHasManagedByUser::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadRestaurantVisitReports()
    {
        return $this->hasMany(FitRoadRestaurantVisitReport::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return FitRoadRestaurantQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FitRoadRestaurantQuery(get_called_class());
    }
}

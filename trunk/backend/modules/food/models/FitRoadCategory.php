<?php

namespace backend\modules\food\models;

use Yii;

/**
 * This is the model class for table "fit_road_category".
 *
 * @property integer $id
 * @property integer $name
 * @property integer $status
 * @property integer $created_at
 * @property integer $restaurant_id
 *
 * @property FitRoadRestaurant $restaurant
 * @property FitRoadFood[] $fitRoadFoods
 */
class FitRoadCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_category';
    }
    /**
     * @author Sang Nguyen
     * (non-PHPdoc)
     * @see \yii\base\Component::behaviors()
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\AttributeBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at' ],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['created_at'],
                ],
                'value' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'status', 'created_at', 'restaurant_id'], 'integer'],
            [['name'], 'unique'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadRestaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'status' => Yii::t('backend', 'Status'),
            'created_at' => Yii::t('backend', 'Created At'),
            'restaurant_id' => Yii::t('backend', 'Restaurant ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(FitRoadRestaurant::className(), ['id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadFoods()
    {
        return $this->hasMany(FitRoadFood::className(), ['category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return FitRoadCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FitRoadCategoryQuery(get_called_class());
    }
}

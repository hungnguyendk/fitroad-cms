<?php

namespace backend\modules\food\models;

use backend\commons\models\entities\FitRoadUser;
use backend\commons\models\searchs\FitRoadRestaurant;
use backend\modules\food\models\FitRoadFood;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\food\models\FitRoadGalleryImage;
/**
 * FitRoadFoodSearch represents the model behind the search form about `backend\modules\food\models\FitRoadFood`.
 */
class FitRoadFoodSearch extends FitRoadFood
{
    public $restaurant_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'restaurant_id', 'price', 'type', 'calories', 'weight', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'description','restaurant_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$conditions=[])
    {
        $query = FitRoadFood::find();
        $query->joinWith(['restaurant']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        if($conditions && !empty($conditions)){
            $query->andOnCondition($conditions);
        }
        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params)
         * statement below
         */
        $dataProvider->setSort([
            'defaultOrder' => [
                'created_at' => SORT_DESC
            ],
            'attributes' => [
                'id',
                'restaurant_name' => [
                    'asc' => [FitRoadRestaurant::tableName().'.name' => SORT_ASC],
                    'desc' => [FitRoadRestaurant::tableName().'.name' => SORT_DESC],
                    'label'=>'Restaurant',
                    //'default' => SORT_ASC
                ],
                'name',
                'price',
                'type',
                'calories',
                'weight',
                'created_at'
            ]
        ]);


        if(Yii::$app->user->identity->role != FitRoadUser::FIX_ROAD_USER_ROOT
         && Yii::$app->user->identity->role != FitRoadUser::FIX_ROAD_ADMIN )	{
            $query->where('restaurant_id in (select restaurant_id from fit_road_restaurant_has_managed_by_user where user_id='.Yii::$app->user->identity->id.')');
        }
        // add conditions that should always apply here

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
           'id' => $this->id,
            FitRoadFood::tableName().'.restaurant_id' => $this->restaurant_id,
            FitRoadFood::tableName().'.price' => $this->price,
            FitRoadFood::tableName().'.type' => $this->type,
            FitRoadFood::tableName().'.calories' => $this->calories,
            FitRoadFood::tableName().'.weight' => $this->weight,
            FitRoadFood::tableName().'.status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like',  FitRoadFood::tableName().'.name', $this->name])
            ->orFilterWhere(['like', FitRoadRestaurant::tableName().'.name', $this->restaurant_name])
            ->orFilterWhere(['like', FitRoadRestaurant::tableName().'.address', $this->restaurant_name])
            ->andFilterWhere(['like',  FitRoadFood::tableName().'.description', $this->description]);

        $query->joinWith(['restaurant' => function ($q) {
            $q->orWhere( FitRoadRestaurant::tableName().'.`name` LIKE "%' . $this->restaurant_name . '%"');
            $q->orWhere( FitRoadRestaurant::tableName().'.`address` LIKE "%' . $this->restaurant_name . '%"');
        }]);

        return $dataProvider;
    }

	public function getImages($foodId){
		$connection = \Yii::$app->db;
		$query ="SELECT * from fit_road_gallery_image where status =1 AND `type` ='food' AND food_id='".$foodId."'";
		$user = $connection->createCommand($query);
		$model = $user->queryAll();
		$result = array();
		if($model){
			foreach($model as $value){
				$data['url'] = $value['url'];
                $data['id'] =  $value['id'];
                $result[] = $data;
			}
		}
		return $result;
	}
	
	public function saveImages($data){
		$connection = \Yii::$app->db;
		$query ="DELETE from fit_road_gallery_image where food_id='".$data['id']."'";
		$user = $connection->createCommand($query);
		$user->execute();
		foreach($data['photo'] as $url){

			$imageModel = new FitRoadGalleryImage();
			$imageModel->user_id = yii::$app->user->id;
			$imageModel->food_id = $data['id'];
			$imageModel->url = $url;
			$imageModel->type = 'avatar';
			$imageModel->status = 1;
			$imageModel->updated_at = strtotime(date("Y-m-d H:i:s"));
			if(!$imageModel->save()){
				echo json_encode($imageModel->getErrors());exit;
			}
		}
	}

    public function removerRestaurants(){

    }
}

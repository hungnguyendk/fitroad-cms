<?php

namespace backend\modules\food\models;

/**
 * This is the ActiveQuery class for [[FitRoadRestaurantHasManagedByUser]].
 *
 * @see FitRoadRestaurantHasManagedByUser
 */
class FitRoadRestaurantHasManagedByUserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return FitRoadRestaurantHasManagedByUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FitRoadRestaurantHasManagedByUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

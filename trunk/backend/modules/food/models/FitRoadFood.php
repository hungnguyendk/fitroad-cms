<?php

namespace backend\modules\food\models;

use Yii;

/**
 * This is the model class for table "fit_road_food".
 *
 * @property integer $id
 * @property string $identifier
 * @property integer $restaurant_id
 * @property integer $category_id
 * @property string $name
 * @property integer $price
 * @property integer $type
 * @property string $description
 * @property integer $calories
 * @property integer $weight
 * @property string $out_side_icon_url
 * @property integer $status
 * @property string $nutrients
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property FitRoadCategory $category
 * @property FitRoadRestaurant $restaurant
 * @property FitRoadGalleryImage[] $fitRoadGalleryImages
 * @property FitRoadMealLog[] $fitRoadMealLogs
 */
class FitRoadFood extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_food';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'name', 'description', 'calories'], 'required'],
            [['restaurant_id','category_id', 'price', 'type', 'calories', 'weight', 'status', 'created_at', 'updated_at'], 'integer'],
            [['identifier'], 'string', 'max' => 45],
            [['description'], 'string', 'max' => 555],
            [['out_side_icon_url','name'], 'string', 'max' => 255],
            [['identifier'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => FitRoadRestaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id'] ,'on' =>  self::SCENARIO_UPDATE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'identifier' => Yii::t('backend', 'Identifier'),
            'restaurant_id' => Yii::t('backend', 'Restaurant ID'),
            'category_id' => Yii::t('backend', 'Category ID'),
            'name' => Yii::t('backend', 'Name'),
            'price' => Yii::t('backend', 'Price'),
            'type' => Yii::t('backend', 'Type'),
            'description' => Yii::t('backend', 'Description'),
            'calories' => Yii::t('backend', 'Calories'),
            'weight' => Yii::t('backend', 'Weight'),
            'out_side_icon_url' => Yii::t('backend', 'Out Side Icon Url'),
            'status' => Yii::t('backend', 'Status'),
            'nutrients' => Yii::t('backend', 'Nutrients'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FitRoadCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(FitRoadRestaurant::className(), ['id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadGalleryImages()
    {
        return $this->hasMany(FitRoadGalleryImage::className(), ['food_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadMealLogs()
    {
        return $this->hasMany(FitRoadMealLog::className(), ['food_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return FitRoadFoodQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FitRoadFoodQuery(get_called_class());
    }
}

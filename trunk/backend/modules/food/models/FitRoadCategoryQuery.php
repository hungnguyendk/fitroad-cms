<?php

namespace backend\modules\food\models;

/**
 * This is the ActiveQuery class for [[FitRoadCategory]].
 *
 * @see FitRoadCategory
 */
class FitRoadCategoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return FitRoadCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FitRoadCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

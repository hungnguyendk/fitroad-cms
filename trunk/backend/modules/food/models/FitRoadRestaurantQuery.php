<?php

namespace backend\modules\food\models;

/**
 * This is the ActiveQuery class for [[FitRoadRestaurant]].
 *
 * @see FitRoadRestaurant
 */
class FitRoadRestaurantQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return FitRoadRestaurant[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FitRoadRestaurant|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

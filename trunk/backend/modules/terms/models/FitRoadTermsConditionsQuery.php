<?php

namespace backend\modules\terms\models;

/**
 * This is the ActiveQuery class for [[FitRoadTermsConditions]].
 *
 * @see FitRoadTermsConditions
 */
class FitRoadTermsConditionsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return FitRoadTermsConditions[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FitRoadTermsConditions|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

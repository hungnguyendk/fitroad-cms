<?php

namespace backend\modules\terms\models;

use Yii;

/**
 * This is the model class for table "fit_road_terms_conditions".
 *
 * @property integer $id
 * @property string $name
 * @property string $content
 */
class FitRoadTermsConditions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_terms_conditions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'content' => Yii::t('backend', 'Content'),
        ];
    }

    /**
     * @inheritdoc
     * @return FitRoadTermsConditionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FitRoadTermsConditionsQuery(get_called_class());
    }
}

<?php

namespace backend\modules\terms\controllers;

use backend\modules\auth\controllers\AuthenticateController;
use kartik\mpdf\Pdf;
use Yii;
use backend\modules\terms\models\FitRoadTermsConditions;
use backend\modules\terms\models\FitRoadTermsConditionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ManageController implements the CRUD actions for FitRoadTermsConditions model.
 */
class ManageController extends AuthenticateController
{

    /**
     * Lists all FitRoadTermsConditions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FitRoadTermsConditionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FitRoadTermsConditions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
        $model = $this->findModel($id);

        $html = $this->renderPartial("_template", ['content' => $model->content]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
                // set to use core fonts only
                // 'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $html,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting
                //'cssFile' => '@backend/web/css/pdf/tc.css',
                // set mPDF properties on the fly
                'options' => [
                    'title' => $model->name
                ]
            ]
        // call mPDF methods on the fly
        // 'methods' => [
        // 'SetHeader'=>['Krajee Report Header'],
        // 'SetFooter'=>['{PAGENO}'],
        // ]
        );

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * Creates a new FitRoadTermsConditions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FitRoadTermsConditions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FitRoadTermsConditions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FitRoadTermsConditions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FitRoadTermsConditions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FitRoadTermsConditions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FitRoadTermsConditions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

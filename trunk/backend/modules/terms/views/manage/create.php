<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\terms\models\FitRoadTermsConditions */

$this->title = Yii::t('backend', 'Create Fit Road Terms Conditions');

\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
    'urlLevel1'=>['index'],
    'titleLevel1'=>Yii::t('backend', 'Terms Conditions Management'),
]);
?>
<div class="fit-road-terms-conditions-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

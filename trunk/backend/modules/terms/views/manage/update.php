<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\terms\models\FitRoadTermsConditions */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Fit Road Terms Conditions',
]) . $model->name;

\backend\commons\helpers\UtilHelper::builtBreadcrumb(3,$this,[
    'titleLevel1'=>Yii::t('backend', 'Terms Conditions Management'),
    'titleLevel2'=>Yii::t('backend','Update'),
    'urlLevel1'=>['index'],
    'urlLevel2'=>['view','id'=>$model->id],
    'objectName'=>trim($model->name)
]);

?>
<div class="fit-road-terms-conditions-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\terms\models\FitRoadTermsConditions */
/* @var $form yii\widgets\ActiveForm */
$asset		= backend\assets\AppAsset::register($this);

$asset->css[] = 'theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-summernote/summernote.css';


$asset->js[] = 'theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-summernote/summernote.min.js';

$jsScripts = <<<JS
    var ComponentsEditors = function () {

    var handleWysihtml5 = function () {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["../../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

    var handleSummernote = function () {
        $('#summernote_1').summernote({height: 300});
        //API:
        //var sHTML = $('#summernote_1').code(); // get code
        //$('#summernote_1').destroy(); // destroy
    }

    return {
        //main function to initiate the module
        init: function () {
            handleWysihtml5();
            handleSummernote();
        }
    };

}();
ComponentsEditors.init();
JS;

$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);


?>

<div class="fit-road-terms-conditions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6,'id'=>'summernote_1']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

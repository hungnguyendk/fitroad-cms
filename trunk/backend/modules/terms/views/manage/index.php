<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\terms\models\FitRoadTermsConditionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Fit Road Terms & Conditions');
$this->params['breadcrumbs'][] = $this->title;
$asset		= backend\assets\AppAsset::register($this);
?>
<div class="fit-road-terms-conditions-index">

    <div class="row">
    <div class="col-md-6 col-sm-6">
        <form id="formIndex">
            <?php
            echo \nterms\pagesize\PageSize::widget([
                'label' => false,
                'template'=>'Show {list} records',
                'options'=> ['class'=>'form-control input-xsmall input-inline','id'=>'perpage']
            ]);

            ?>
        </form>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="dataTables_length pull-right">
            <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add new'), ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div><hr/><br/>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'layout'=>"<div class='pull-left'>{summary}</div>\n{items}\n{pager}\n",
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summary'=>'',
    'tableOptions'=>['class'=>'table table-striped table-bordered table-hover dataTable no-footer'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

       // 'id',
        'name',
        //'content:ntext',

        ['class' => 'yii\grid\ActionColumn'],
    ],
]);
?>
<?php Pjax::end(); ?></div>

<?php

namespace backend\modules\authorization\models\query;

/**
 * This is the ActiveQuery class for [[\backend\manage\authorization\models\entity\AuthAssignment]].
 *
 * @see \backend\manage\authorization\models\entity\AuthAssignment
 */
class AuthAssignmentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\manage\authorization\models\entity\AuthAssignment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\manage\authorization\models\entity\AuthAssignment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
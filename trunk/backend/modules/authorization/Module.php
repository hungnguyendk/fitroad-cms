<?php

namespace backend\modules\authorization;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\authorization\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\manage\authorization\models\entity\AuthItem */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="portlet-body form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'class'=>'form-horizontal'
        ]
    ]); ?>
    <div class="form-body">
        <?= $form->field(
            $model,
            'name',[
            'template'=>"<div class='col-md-2'>{label}</div>\n<div class='col-md-7'>\n{input}\n{hint}\n{error}</div>"
        ])->textInput(['maxlength' => true]) ;
        ?>

        <?= $form->field($model, 'description',
            [
                'template'=>"<div class='col-md-2'>{label}</div>\n<div class='col-md-7'>\n{input}\n{hint}\n{error}</div>"
            ])->textarea(['rows' => 6]) ;
        ?>
    </div>
    <div class="col-md-offset-2 col-md-10">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-check"></i> '.Yii::t('backend', 'Create') : '<i class="fa fa-check"></i> '.Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

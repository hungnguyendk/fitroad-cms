<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\commons\helpers\UtilHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\authorization\models\search\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$asset		= backend\assets\AppAsset::register($this);
$this->title = Yii::t('backend', 'Permission Management');
//$this->params['breadcrumbs'][] = ['label'=>$this->title,'template' => "<li><b>{link}</b></li>\n"];
\backend\commons\helpers\UtilHelper::builtBreadcrumb(1,$this);
/* @var $this yii\web\View */
$asset->css[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css';
$this->registerJsFile(Yii::$app->homeUrl.'scripts/authorization-manage.js',[ 'depends' => 'yii\web\YiiAsset']);
$jsScripts = <<<JS
Authorization.handelAssignmentPermission()
JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
?>
<!-- BEGIN INCLUSION TABLE PORTLET-->
<div class="portlet gren">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <?php
                echo \nterms\pagesize\PageSize::widget([
                    'label' => false,
                    'template'=>'Show {list} records',
                    'options'=> ['class'=>'form-control input-xsmall input-inline']
                ]);
                ?>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="dataTables_length pull-right">
                    <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('backend', 'Create Permission'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <hr/>
        <div class="col-md-12 col-sm-12">
            <div class="row row-vgrid-20">
                <?= GridView::widget([
                    'layout'=>"<div class='pull-left'>{summary}</div>\n{items}\n{pager}\n",
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'filterSelector' => 'select[name="per-page"]',
                    'rowOptions' => function ($model, $key, $index, $grid) {
                        $modules = Yii::$app->modules;
                        foreach($modules as $key=>$value){
                            if($key === strtolower($model->name)){
                                return [ 'class' => 'active'];
                            }
                        };
                    },
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'name',
                        //'type',
                        'description:ntext',
                        //'rule_name',
                        //'data:ntext',
                        // 'created_at',
                        // 'updated_at',
                        //$arrayRole,
                        [
                            'class' => 'yii\grid\CheckboxColumn',
                            'header'=>'System admin',
                            'name'=>'sysadmin',
                            'checkboxOptions' => function($model, $key, $index, $column) {
                                $access = UtilHelper::checkMatrixPermission('sysadmin',$model);
                                return ['checked'=>$access,'disabled'=>true];
                            }
                        ],
                        [
                            'class' => 'yii\grid\CheckboxColumn',
                            'header'=>'Admin',
                            'name'=>'admin',
                            'checkboxOptions' => function($model, $key, $index, $column) {
                                $access = UtilHelper::checkMatrixPermission('admin',$model);
                                $option = UtilHelper::generateDataPermissionBelongMoudle($model);
                                return ['checked'=>$access,'data-key'=>$option['data-key'],'id'=>'adR'.$option['id']];
                            }
                        ],
                        [
                            'class' => 'yii\grid\CheckboxColumn',
                            'header'=>'Manager',
                            'name'=>'manager',
                            'checkboxOptions' => function($model, $key, $index, $column) {
                                $access = UtilHelper::checkMatrixPermission('manager',$model);
                                $option = UtilHelper::generateDataPermissionBelongMoudle($model);
                                return ['checked'=>$access,'data-key'=>$option['data-key'],'id'=>'maR'.$option['id']];
                            }
                        ],
                        [
                            'class' => 'yii\grid\CheckboxColumn',
                            'header'=>'Employee',
                            'name'=>'staff',
                            'checkboxOptions' => function($model, $key, $index, $column) {
                                $access = UtilHelper::checkMatrixPermission('staff',$model);
                                $option = UtilHelper::generateDataPermissionBelongMoudle($model);
                                return ['checked'=>$access,'data-key'=>$option['data-key'],'id'=>'stR'.$option['id']];
                            }
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header'=>'Action'
                        ]
                    ],
                    'tableOptions' => [
                        'id'=> 'girdViewRole',
                        'class' => 'table table-striped table-bordered table-hover'
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
<!-- END INCLUSION TABLE PORTLET-->


<?php
namespace backend\modules\authorization\rbac;

use Yii;
use yii\rbac\Rule;

/**
 * Checks if user group matches
 */
class UserGroupRule extends Rule
{
    public $name = 'userGroup';
    
    
    public function execute($user, $item, $params)
    {

        if (!Yii::$app->user->isGuest) {
            $role =Yii::$app->user->identity->role;
            if ($item->name === 'sysadmin') {
               return  $role === '0001';
            } elseif ($item->name === 'admin') {
                return  $role === '0001' || $role === '0010';
            }elseif ($item->name === 'manager') {
                return  $role === '0001' || $role === '0010' ||  $role === '0011';
            }elseif ($item->name === 'staff') {
                return  $role === '0001' || $role === '0010' ||  $role === '0011' ||  $role === '4';
            }
        }
        return false;
    }
//     public function execute($user, $item, $params)
//     {
//         if (!Yii::$app->user->isGuest) {
//             $role = Yii::$app->user->identity->role;
//             if ($item->name === 'sysadmin') {
//                 return $role === $item->name;
//             } elseif ($item->name === 'admin') {
//                 return $role === $item->name || $role === 'sysadmin';
//             } 
//             elseif ($item->name === 'user') {
//                 return $role === $item->name || $role === 'superadmin' || $role === 'admin';
//             }
//         }
//         return false;
//     }
}
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use api\commons\helpers\ApiHelpers;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\food\models\FitRoadFoodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
$asset		= backend\assets\AppAsset::register($this);
/* @var $this yii\web\View */
$asset->css[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css';
$this->registerJsFile(Yii::$app->homeUrl.'scripts/charts-flotcharts.js',[ 'depends' => 'yii\web\YiiAsset']);
$this->registerJsFile(Yii::$app->homeUrl.'scripts/mychart.js',[ 'depends' => 'yii\web\YiiAsset']);

?>
<div class="fit-road-food-index">
   <div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Registered Users & Created Restaurants
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="chart_2" class="chart">
							</div>
						</div>
	</div>
</div>
<div class="row">
						<div class="col-md-12">
							<!-- BEGIN CHART PORTLET-->
							<div class="portlet light bordered">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i>
										<span class="caption-subject bold uppercase font-green-haze"> Top Visited Restaurants</span>
										<span class="caption-helper"></span>
									</div>
									<div class="tools">
										<a href="javascript:;" class="collapse">
										</a>
										<a href="#portlet-config" data-toggle="modal" class="config">
										</a>
										<a href="javascript:;" class="reload">
										</a>
										<a href="javascript:;" class="fullscreen">
										</a>
										<a href="javascript:;" class="remove">
										</a>
									</div>
								</div>
								<div class="portlet-body">
									<div id="chart_5" class="chart" style="height: 400px;">
									</div>
									<!--<div class="well margin-top-20">
										<div class="row">
											<div class="col-sm-3">
												<label class="text-left">Top Radius:</label>
												<input class="chart_5_chart_input" data-property="topRadius" type="range" min="0" max="1.5" value="1" step="0.01"/>
											</div>
											<div class="col-sm-3">
												<label class="text-left">Angle:</label>
												<input class="chart_5_chart_input" data-property="angle" type="range" min="0" max="89" value="30" step="1"/>
											</div>
											<div class="col-sm-3">
												<label class="text-left">Depth:</label>
												<input class="chart_5_chart_input" data-property="depth3D" type="range" min="1" max="120" value="40" step="1"/>
											</div>
										</div>
									</div>-->
								</div>
							</div>
							<!-- END CHART PORTLET-->
						</div>
					</div>
<script>
function chart2() {
    if ($('#chart_2').size() != 1) {
        return;
    }
    var visitors = [
        [1, <?php echo $numberOfUsers[1]?>],
        [2, <?php echo $numberOfUsers[2]?>],
        [3, <?php echo $numberOfUsers[3]?>],
        [4, <?php echo $numberOfUsers[4]?>],
        [5, <?php echo $numberOfUsers[5]?>],
        [6, <?php echo $numberOfUsers[6]?>],
        [7, <?php echo $numberOfUsers[7]?>],
        [8, <?php echo $numberOfUsers[8]?>],
        [9, <?php echo $numberOfUsers[9]?>],
        [10, <?php echo $numberOfUsers[10]?>],
        [11, <?php echo $numberOfUsers[11]?>],
        [12, <?php echo $numberOfUsers[12]?>],
    ];
	var restaus = [
        [1, <?php echo $numberOfRests[1]?>],
        [2, <?php echo $numberOfRests[2]?>],
        [3, <?php echo $numberOfRests[3]?>],
        [4, <?php echo $numberOfRests[4]?>],
        [5, <?php echo $numberOfRests[5]?>],
        [6, <?php echo $numberOfRests[6]?>],
        [7, <?php echo $numberOfRests[7]?>],
        [8, <?php echo $numberOfRests[8]?>],
        [9, <?php echo $numberOfRests[9]?>],
        [10, <?php echo $numberOfRests[10]?>],
        [11, <?php echo $numberOfRests[11]?>],
        [12, <?php echo $numberOfRests[12]?>],
    ];	
    var plot = $.plot($("#chart_2"), [{
	        data: visitors,
	        label: "Registered Users in <?php echo date("Y")?>" ,
	        lines: {
	            lineWidth: 1,
                shadowSize: 0
	        }
       		},{
	        data: restaus,
	        label: "Created Restaurants in <?php echo date("Y")?>" ,
	        lines: {
	            lineWidth: 1,
	            shadowSize: 0
	        }   
    		}
    	], {
        series: {
            lines: {
                show: true,
                lineWidth: 2,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.05
                    }, {
                        opacity: 0.01
                    }]
                }
            },
            points: {
                show: true,
                radius: 3,
                lineWidth: 1
            },
            shadowSize: 2
        },
        grid: {
            hoverable: true,
            clickable: true,
            tickColor: "#eee",
            borderColor: "#eee",
            borderWidth: 1
        },
        colors: ["#d12610", "#37b7f3", "#52e136"],
        xaxis: {
            ticks: 11,
            tickDecimals: 0,
            tickColor: "#eee",
        },
        yaxis: {
            ticks: 11,
            tickDecimals: 0,
            tickColor: "#eee",
        }
    });


    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 15,
            border: '1px solid #333',
            padding: '4px',
            color: '#fff',
            'border-radius': '3px',
            'background-color': '#333',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#chart_2").bind("plothover", function(event, pos, item) {
        $("#x").text(pos.x.toFixed(0));
        $("#y").text(pos.y.toFixed(0));

        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(0),
                    y = item.datapoint[1].toFixed(0);

                showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
}
function chart_1_1(){
	 provider = [];
	 <?php 
	 $i=0;
	 $dataColor = array("#FF0F00","#FF6600","#FF9E01","#FCD202","#F8FF01",
	 				"#B0DE09","#04D215","#0D8ECF","#0D52D1","#2A0CD0");
	 foreach($topRestaurants as $key=>$value):?>
	    data = [];
	    data['country']=<?php echo json_encode($key)?>;
	    data['visits']='<?php echo $value?>';
	    data['color']='<?php echo $dataColor[$i]?>';
	 	<?php $i+=1;?>
   // console.log(data);
		provider.push(data);

     <?php endforeach;?>
	 var initChartSample5 = function() {
        var chart = AmCharts.makeChart("chart_5", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',
            
            "color":    '#888',

            "dataProvider": provider,
            "valueAxes": [{
                "position": "left",
                "axisAlpha": 0,
                "gridAlpha": 0
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 40,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "labelRotation": 30
            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);

        jQuery('.chart_5_chart_input').off().on('input change', function() {
            var property = jQuery(this).data('property');
            var target = chart;
            chart.startDuration = 0;

            if (property == 'topRadius') {
                target = chart.graphs[0];
            }

            target[property] = this.value;
            chart.validateNow();
        });

        $('#chart_5').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }
    initChartSample5();
}
</script>	
<?php

namespace backend\modules\report\controllers;

use backend\modules\auth\controllers\AuthenticateController;
use Yii;
use backend\commons\models\entities\FitRoadUser;
use backend\commons\models\searchs\FitRoadUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\commons\forms\SignUpForm;
use backend\commons\helpers\UtilHelper;

/**
 * UserController implements the CRUD actions for FitRoadUser model.
 */
class ManageController extends AuthenticateController
{
    /**
     * Lists all FitRoadUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FitRoadUserSearch();
        $numberOfUsers = $searchModel->getRegisteredUsers();
		$numberOfRests = $searchModel->getRests();
		$topRests = $searchModel->getTopTenRestaurants();
        return $this->render('index', [
        	'numberOfUsers'=>$numberOfUsers,
        	'numberOfRests'=>$numberOfRests,
        	'topRestaurants'=>$topRests
        ]);
    }
}

<?php

namespace backend\commons\models\queries;

/**
 * This is the ActiveQuery class for [[\backend\commons\models\entities\FitRoadUser]].
 *
 * @see \backend\commons\models\entities\FitRoadUser
 */
class FitRoadUserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \backend\commons\models\entities\FitRoadUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\commons\models\entities\FitRoadUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace backend\commons\models\queries;

/**
 * This is the ActiveQuery class for [[\backend\commons\models\entities\FitRoadRestaurant]].
 *
 * @see \backend\commons\models\entities\FitRoadRestaurant
 */
class FitRoadRestaurantQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \backend\commons\models\entities\FitRoadRestaurant[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\commons\models\entities\FitRoadRestaurant|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

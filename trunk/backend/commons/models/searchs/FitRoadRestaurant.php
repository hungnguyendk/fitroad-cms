<?php

namespace backend\commons\models\searchs;

use backend\commons\models\entities\FitRoadUser;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use \backend\modules\food\models\FitRoadRestaurant as FitRoadRestaurantModel;

/**
 * FitRoadRestaurant represents the model behind the search form about `backend\commons\models\entities\FitRoadRestaurant`.
 */
class FitRoadRestaurant extends FitRoadRestaurantModel
{

    public $manager_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'rating', 'calories', 'created_at', 'updated_at'], 'integer'],
            [['manager_name','place_id', 'identifier', 'name', 'description', 'phone', 'fax', 'website', 'lat', 'long', 'address', 'icon', 'type', 'google_data','avatar','is_vip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
//    public function search($params)
//    {
//        $query = \backend\modules\food\models\FitRoadRestaurant::find();
//
//        // add conditions that should always apply here
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
//
//        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'status' => $this->status,
//            'rating' => $this->rating,
//            'calories' => $this->calories,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//        ]);
//
//        $query->andFilterWhere(['like', 'place_id', $this->place_id])
//            ->andFilterWhere(['like', 'identifier', $this->identifier])
//            ->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'phone', $this->phone])
//            ->andFilterWhere(['like', 'fax', $this->fax])
//            ->andFilterWhere(['like', 'website', $this->website])
//            ->andFilterWhere(['like', 'lat', $this->lat])
//            ->andFilterWhere(['like', 'long', $this->long])
//            ->andFilterWhere(['like', 'address', $this->address])
//            ->andFilterWhere(['like', 'icon', $this->icon])
//            ->andFilterWhere(['like', 'type', $this->type])
//            ->andFilterWhere(['like', 'google_data', $this->google_data]);
//
//        return $dataProvider;
//    }

	public function searchcms($params)
    {
        $query = \backend\modules\food\models\FitRoadRestaurant::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params)
         * statement below
         */
        $dataProvider->setSort([
            'defaultOrder' => [
                'created_at' => SORT_DESC
            ],
            'attributes' => [
                //'id',
                'name',
                'manager_name' => [
                    'asc' => ['CONCAT(`'.FitRoadUser::tableName().'`.`first_name`,`'.FitRoadUser::tableName().'`.`last_name`'.')' => SORT_ASC],
                    'desc' => ['CONCAT(`'.FitRoadUser::tableName().'`.`first_name`,`'.FitRoadUser::tableName().'`.`last_name`'.')' => SORT_DESC],
                    'label'=>'Manger',
                    //'default' => SORT_ASC
                ],

                'is_vip',
                //'phone',
                'address',
                'created_at'
            ]
        ]);

        if(Yii::$app->user->identity->role !== FitRoadUser::FIX_ROAD_USER_ROOT
        && Yii::$app->user->identity->role !== FitRoadUser::FIX_ROAD_ADMIN )	{
            $query->where('`fit_road_restaurant` .`id` in (select restaurant_id from fit_road_restaurant_has_managed_by_user where user_id='.Yii::$app->user->identity->id.')');
        }
        // add conditions that should always apply here
        //$query->andOnCondition(['is_local'=>1]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            FitRoadRestaurantModel::tableName().'.id' => $this->id,
            FitRoadRestaurantModel::tableName().'.status' => $this->status,
            FitRoadRestaurantModel::tableName().'.rating' => $this->rating,
            FitRoadRestaurantModel::tableName().'.is_vip'=> $this->is_vip,
            FitRoadRestaurantModel::tableName().'.calories' => $this->calories,
            FitRoadRestaurantModel::tableName().'.created_at' => $this->created_at,
            FitRoadRestaurantModel::tableName().'.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'place_id', $this->place_id])
            ->andFilterWhere(['like', 'identifier', $this->identifier])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'is_vip', $this->is_vip])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'lat', $this->lat])
            ->andFilterWhere(['like', 'long', $this->long])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'google_data', $this->google_data]);
        // filter by owner email
        // filter by owner email
        $query->joinWith(['fitRoadRestaurantHasManagedByUsers' => function ($q) {
            $q->joinWith(['user'=>function($_q){
               if($this->manager_name && !empty($this->manager_name)){
                   $_q->orWhere(FitRoadUser::tableName().'.`last_name` LIKE "%' . $this->manager_name . '%"');
                   $_q->orWhere(FitRoadUser::tableName().'.`first_name` LIKE "%' . $this->manager_name . '%"');
               }
            }],true, 'LEFT JOIN' );
        }],true, 'LEFT JOIN' );
        return $dataProvider;
    }

	 
}

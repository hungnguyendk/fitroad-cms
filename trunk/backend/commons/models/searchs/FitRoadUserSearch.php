<?php

namespace backend\commons\models\searchs;

use backend\commons\helpers\UtilHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\commons\models\entities\FitRoadUser;

/**
 * FitRoadUserSearch represents the model behind the search form about `backend\commons\models\entities\FitRoadUser`.
 */
class FitRoadUserSearch extends FitRoadUser
{
    /**
     * @inheritdoc
     */
    public $full_name;

    public function rules()
    {
        return [
            [['id', 'height', 'weight', 'age', 'point', 'status', 'created_at', 'updated_at'], 'integer'],
            [['full_name','first_name', 'last_name', 'password', 'email', 'gender', 'phone_number', 'alias', 'password_reset_token', 'time_zone', 'role'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchCustomer($params)
    {
        $query = FitRoadUser::find();
        $query->andOnCondition(['in','role',[
            FitRoadUser::FIX_ROAD_CUSTOMER
        ]]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ]
            ]
        ]);
        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params)
         * statement below
         */
        $dataProvider->setSort([
            'defaultOrder' => [
                'full_name' => SORT_ASC
            ],
            'attributes' => [
                //'id',
                'full_name' => [
                    'asc' => ['CONCAT(`'.FitRoadUser::tableName().'`.`first_name`,`'.FitRoadUser::tableName().'`.`last_name`'.')' => SORT_ASC],
                    'desc' => ['CONCAT(`'.FitRoadUser::tableName().'`.`first_name`,`'.FitRoadUser::tableName().'`.`last_name`'.')' => SORT_DESC],
                    'label'=>'Full Name',
                    //'default' => SORT_ASC
                ],

                'email',
                'height',
                'weight',
                'age',
                'gender',
                'created_at'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->filter($query);
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchStaff($params)
    {
        $query = FitRoadUser::find();
        $query->andOnCondition(['not in','role',[
            FitRoadUser::FIX_ROAD_USER_ROOT,
            FitRoadUser::FIX_ROAD_CUSTOMER
        ]]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ]
            ]
        ]);
        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params)
         * statement below
         */
        $dataProvider->setSort([
            'defaultOrder' => [
                'full_name' => SORT_ASC
            ],
            'attributes' => [
                //'id',
                'full_name' => [
                    'asc' => ['CONCAT(`'.FitRoadUser::tableName().'`.`first_name`,`'.FitRoadUser::tableName().'`.`last_name`'.')' => SORT_ASC],
                    'desc' => ['CONCAT(`'.FitRoadUser::tableName().'`.`first_name`,`'.FitRoadUser::tableName().'`.`last_name`'.')' => SORT_DESC],
                    'label'=>'Full Name',
                    //'default' => SORT_ASC
                ],

                'email',
                'height',
                'weight',
                'age',
                'gender',
                'created_at'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->filter($query);
        return $dataProvider;
    }

    private function filter($query){
        $query->andFilterWhere([
            'id' => $this->id,
            'height' => $this->height,
            'weight' => $this->weight,
            'age' => $this->age,
            'point' => $this->point,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'time_zone', $this->time_zone])
            ->orFilterWhere(['like', 'last_name', $this->full_name])
            ->orFilterWhere(['like', 'first_name', $this->full_name])
            ->andFilterWhere(['like', 'role', $this->role]);
    }

	public function listGotRestaurants($id){
		$connection = \Yii::$app->db;
		$query ="SELECT restaurant_id,fit_road_restaurant.name from fit_road_restaurant_has_managed_by_user
        LEFT JOIN fit_road_restaurant ON fit_road_restaurant.id = fit_road_restaurant_has_managed_by_user.restaurant_id
        where fit_road_restaurant_has_managed_by_user.status = 1 AND fit_road_restaurant_has_managed_by_user.user_id='".$id."'";

		$user = $connection->createCommand($query);
		$model = $user->queryAll();
		$result = array();
        $data = [];

		if($model){

			foreach($model as $value){
				$result['id'] = $value['restaurant_id'];
                $result['text'] = $value['name'];
                $data[] = $result;
			}
		}
		return $data;
	}

    public function deleteAssigned($id){
        $connection = \Yii::$app->db;
        $query ="DELETE FROM `fit_road_restaurant_has_managed_by_user` where user_id = '$id'";
        $user = $connection->createCommand($query);
        $user->execute();
    }
	public function saveRestaurants($restaurants,$id){
		$connection = \Yii::$app->db;
		$userId = $id;
		$today = date("Y-m-d H:i:s");
		if(count($restaurants)>0){
			$query ="DELETE FROM `fit_road_restaurant_has_managed_by_user` where user_id = '$userId'";
			$user = $connection->createCommand($query);
			$user->execute();
			foreach($restaurants as $value){
				if(!empty($value)){
                    try{
                        $query ="INSERT INTO `fit_road_restaurant_has_managed_by_user`(`restaurant_id`, `user_id`, `role`, `status`, `created_at`)
					VALUES ('$value','$userId','staff','1',".new \yii\db\Expression('UNIX_TIMESTAMP()').")";
                        $user = $connection->createCommand($query);
                        $user->execute();
                    }catch (\yii\db\Exception $e){
                        Yii::error($e->getMessage() .' error');
                    }

				}
			}
		}	
	}
	
	public function getRegisteredUsers(){
		$connection = \Yii::$app->db;
		$year = date("Y");
		for($i=1;$i<=12;$i++){
			$query ="SELECT count(*) as total from 	fit_road_user where MONTH(FROM_UNIXTIME(created_at))='$i' AND YEAR(FROM_UNIXTIME(created_at))='$year'";
			$user = $connection->createCommand($query);
			$model = $user->queryOne();
			$data[$i] = $model['total'];
		}
		return $data;
	}
	
	public function getTopTenRestaurants(){
		$connection = \Yii::$app->db;
		$year = date("Y");
		$query ="SELECT fit_road_restaurant.id, fit_road_restaurant.name,  count(fit_road_restaurant_visit_report.restaurant_id) as total from 	fit_road_restaurant_visit_report 
			left join fit_road_restaurant on fit_road_restaurant.id = fit_road_restaurant_visit_report.restaurant_id
			
			group by fit_road_restaurant.id, fit_road_restaurant.name order by total DESC LIMIT 10 ";
		$user = $connection->createCommand($query);
		$model = $user->queryAll();
		$data = array();
		if($model){
			foreach($model as $value){
				//$data[UtilHelper::shorten($value['name'],20)] = $value['total'];
                $data[$value['name']] = $value['total'];
			}
		}
		return $data;
	}
	
	public function getRests(){
		$connection = \Yii::$app->db;
		$year = date("Y");
		for($i=1;$i<=12;$i++){
			$query ="SELECT count(*) as total from 	fit_road_restaurant where MONTH(FROM_UNIXTIME(created_at))='$i' AND YEAR(FROM_UNIXTIME(created_at))='$year'";
			$user = $connection->createCommand($query);
			$model = $user->queryOne();
			$data[$i] = $model['total'];
		}
		return $data;
	}
}

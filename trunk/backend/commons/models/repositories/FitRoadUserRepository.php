<?php
/**
 * User: sangnguyen on  4/11/16 at 14:50
 * File name: FixRoadUserRepository.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by AppsCyclone
 * All rights reserved
 */

namespace backend\commons\models\repositories;
use backend\modules\food\models\FitRoadGalleryImage;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class FitRoadUserRepository extends ActiveRecord{
    CONST FIX_ROAD_USER_ROOT = '0001';
    CONST FIX_ROAD_ADMIN = '0010';
    CONST FIX_ROAD_MANAGER = '0011';
    CONST FIX_ROAD_STAFF   = '4';
    CONST FIX_ROAD_CUSTOMER = '-1';

    public $changedAttributes;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert){

        if(isset($this->password ) && !empty($this->password)){
            $this->password = \Yii::$app->security->generatePasswordHash($this->password);
        }else{
            unset($this['password']);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert,$changedAttributes){
        $this->changedAttributes = $changedAttributes;
        return parent::afterSave($insert,$changedAttributes);
    }

    /**
     * @return array $changedAttributes
     */
    public function getChangedAttributes(){
        unset($this->changedAttributes['updated_at']);
        return $this->changedAttributes;
    }

    static public function updateDoNotCurrentAllImage($user_id,$type='avatar'){

        return $model = \Yii::$app->db->createCommand()->update(
            FitRoadGalleryImage::tableName(),[
            'is_current'=>0
        ],[
            'type'=>$type,
            'user_id'=>$user_id
        ])->execute();

    }
}
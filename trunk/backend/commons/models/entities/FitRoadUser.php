<?php

namespace backend\commons\models\entities;

use api\commons\helpers\ApiHelpers;
use api\commons\models\entities\FitRoadGalleryImage;
use backend\commons\models\repositories\FitRoadUserRepository;
use backend\modules\food\models\FitRoadRestaurantHasManagedByUser;
use Yii;

/**
 * This is the model class for table "fit_road_user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $password
 * @property string $email
 * @property string $height
 * @property string $avatar
 * @property string $weight
 * @property string $gender
 * @property integer $age
 * @property string $phone_number
 * @property string $alias
 * @property integer $point
 * @property string $password_reset_token
 * @property string $time_zone
 * @property string $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthenticationToken[] $authenticationTokens
 * @property FitRoadGalleryImage[] $fitRoadGalleryImages
 * @property FitRoadMealLog[] $fitRoadMealLogs
 * @property FitRoadRestaurantHasManagedByUser[] $fitRoadRestaurantHasManagedByUsers
 * @property FitRoadRestaurantVisitReport[] $fitRoadRestaurantVisitReports
 * @property FitRoadUserActivity[] $fitRoadUserActivities
 */
class FitRoadUser extends FitRoadUserRepository
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_road_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email'], 'required'],
            [['age', 'point', 'status', 'created_at', 'updated_at'], 'integer'],
            [['role'], 'string'],
            [['first_name', 'last_name', 'email', 'alias', 'time_zone'], 'string', 'max' => 45],
            [['password','avatar'], 'string', 'max' => 255],
            [['height', 'weight'], 'string', 'max' => 11],
            [['gender'], 'string', 'max' => 4],
            [['phone_number'], 'string', 'max' => 15],
            [['password_reset_token'], 'string', 'max' => 115],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'first_name' => Yii::t('backend', 'First Name'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'last_name' => Yii::t('backend', 'Last Name'),
            'password' => Yii::t('backend', 'Password'),
            'email' => Yii::t('backend', 'Email'),
            'height' => Yii::t('backend', 'Height'),
            'weight' => Yii::t('backend', 'Weight'),
            'gender' => Yii::t('backend', 'Gender'),
            'age' => Yii::t('backend', 'Age'),
            'phone_number' => Yii::t('backend', 'Phone Number'),
            'alias' => Yii::t('backend', 'Alias'),
            'point' => Yii::t('backend', 'Point'),
            'password_reset_token' => Yii::t('backend', 'Password Reset Token'),
            'time_zone' => Yii::t('backend', 'Time Zone'),
            'role' => Yii::t('backend', 'Role'),
            'status' => Yii::t('backend', 'Status'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthenticationTokens()
    {
        return $this->hasMany(AuthenticationToken::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadGalleryImages()
    {
        return $this->hasMany(\api\commons\models\entities\FitRoadGalleryImage::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadMealLogs()
    {
        return $this->hasMany(FitRoadMealLog::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadRestaurantHasManagedByUsers()
    {
        return $this->hasMany(FitRoadRestaurantHasManagedByUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadRestaurantVisitReports()
    {
        return $this->hasMany(FitRoadRestaurantVisitReport::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitRoadUserActivities()
    {
        return $this->hasMany(FitRoadUserActivity::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \backend\commons\models\queries\FitRoadUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\commons\models\queries\FitRoadUserQuery(get_called_class());
    }
    public function getAvatar($size=null){

        $avatar = $this->getFitRoadGalleryImages()->andOnCondition([
            'type'=>FitRoadGalleryImage::IMAGE_TYPE_AVATAR,
            'is_current'=>FitRoadGalleryImage::IS_CURRENT,
            'status'=>FitRoadGalleryImage::STATUS_ACTIVE
        ])->one();

        if($avatar)
            return ApiHelpers::builtUrlImages(Yii::$app->params['avatarsFinder'],
                $avatar->url,$size);
        else
            return null;
    }
}

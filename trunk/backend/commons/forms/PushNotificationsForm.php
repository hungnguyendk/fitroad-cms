<?php
/**
 * User: sangnguyen on  5/11/16 at 23:15
 * File name: PushNotificationsForm.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace backend\commons\forms;


use backend\commons\helpers\UtilHelper;
use yii\base\Model;

class PushNotificationsForm extends Model
{

    /**
     * @inheritdoc
     */
    public $message;

    public function rules()
    {
        return [
            [['message'], 'required'],
        ];
    }

    public function push(){
        if($this->validate()){
                $response = UtilHelper::sendNotification($this->message);
            $res = json_decode($response);
          if(isset($res->message_id)){
              return TRUE;
          }
        }
    }

}
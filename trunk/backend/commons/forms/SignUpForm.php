<?php
/**
 * User: sangnguyen on  11/04/15 at 22:18
 * File name: SignUpForm.php
 * Project name: Fit Road
 * Copyright (c) 2015 by COMPANY
 * All rights reserved
 */

namespace backend\commons\forms;

use backend\commons\models\entities\FitRoadUser ;
use Yii;
use yii\helpers\ArrayHelper;


class SignUpForm extends FitRoadUser{
    CONST SCENARIO_UPDATE       =   'update';
    CONST SCENARIO_REGISTER     =   'register';

    public $user;

    public $password_repeat;
    /**
     * @inheritdoc
     */

    public function rules()
    {
        $parentRules = parent::rules();
        $currentRules =  [
            ['role', 'integer'],
            ['role', 'in','range' => ['0011','4','0010','-1'],'message' => Yii::t('backend/error', 'role not existed')],

            ['phone_number', 'integer'],
            ['phone_number', 'match', 'pattern'=>'/^[0-9]{9,12}$/','message' => Yii::t('backend/error',
                'phone number validate')],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],

            [
                'email', 'unique', 'targetClass' => '\backend\commons\models\entities\FitRoadUser',
                'filter'=> ['not in','id',[$this->id]],
                'message' => Yii::t('backend/error', 'email existed'),
            ],

//            ['password', 'required','on'=>self::SCENARIO_REGISTER],
//            ['password', 'required','on'=>self::SCENARIO_REGISTER],

            [['password','password_repeat'], 'required','on'=>self::SCENARIO_REGISTER],

            [['password_repeat'], 'compare', 'compareAttribute' => 'password'],
        ];
        return ArrayHelper::merge($parentRules,$currentRules);
    }
/*    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = ['username', 'password'];
        $scenarios[self::SCENARIO_REGISTER] = ['username', 'email', 'password'];
        return $scenarios;
    }*/

    public function signup(){
        $this->phone_number     =   $this->phone_number;
        $this->status           =   FitRoadUser::ACTIVE;
        $this->role             =   $this->role;
        $this->save(FALSE);
    }
}
<?php
/**
 * User: sangnguyen
 * Date: 10/11/15
 * Time: 14:46
 * File name: UtilHelper.php
 * Project name: ysd-tee-shirt
 */

namespace backend\commons\helpers;

use backend\commons\models\entities\FitRoadUser;
use common\components\SmartImage;
use Yii;
use yii\helpers\Html;
use api\commons\forms\UploadAttachmentFileForm;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;

class UtilHelper{

    public static function commonIsImageUrl($content)
    {
        $ext=substr($content,strlen($content)-4,4);
        if(strrpos($content,'http://')==0 && ($ext=='.gif' || $ext=='.jpg'|| $ext=='.png')){
            return true;
        }
        return false;
    }

    public static function downloadImageFromLink($path,$url_image,$resize=true){

        if(!self::commonIsImageUrl($url_image)){
            return null;
        }


        $img_file = file_get_contents ( $url_image );

        $filename =Yii::$app->security->generateRandomString() . time() . '.jpg';

        file_put_contents ($path.$filename, $img_file );

        if($resize){
            self :: resizeImg($path,$filename, Yii::$app->params['normalName'].$filename,
                Yii::$app->params['sizeNormalImg'],  Yii::$app->params['sizeNormalImg']);

            self :: resizeImg($path, $filename, Yii::$app->params['thumbName'].$filename,
                Yii::$app->params['sizeThumbImg'],  Yii::$app->params['sizeThumbImg']);
        }

        return  $filename;
    }

    /**
     * @param $path
     * @param $fileName
     * @param string $file
     * @param bool|TRUE $resize
     * @return UploadAttachmentFileForm|bool
     * @throws \yii\base\Exception
     */
    public static function uploadAttachmentFile($path,$fileName=null,$file='files',$resize = true,$compare=FALSE){
        $path .='/';

        $dirImage =  is_dir($path);

        if(!$dirImage ){
            $dirImage = BaseFileHelper::createDirectory($path,0777);
        }

        if(!$dirImage){
           // $this->sendResponse(200,false,$this->builtErrorCode(96));
        }

        $form = new UploadAttachmentFileForm();

        $files   =  UploadedFile::getInstancesByName($file);
        if(is_array($files) && !empty($files)) {
            $form->attachmentFiles = $files;
            $form->path = $path;
            $form->resize = $resize;
            $form->fileName = $fileName;
            $form->compare = $compare;

            if ($form->save()) {
                return $form;
            } else {
                //$this->getFormError($form);
            }
        }else{
            return null;
        }
    }

    /**
     * Sending Push Notification
     */
    public static function sendNotification($message) {

        // Set POST variables

        $url = 'https://gcm-http.googleapis.com/gcm/send';


        $fields = [
            'to'=> '/topics/global',
            'data'=>[
                'message'=>$message
            ]
        ];


        $headers = array(
            'Authorization: key=' .Yii::$app->params['GOOGLE_API_KEY'],
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        return $result;
    }


    public static function getDataUrlByGetMethod($url){

        $curl = curl_init();

        $options = array(
            CURLOPT_URL            => $url,
            CURLOPT_HEADER         => false,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_RETURNTRANSFER => true,
        );

        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);

        if ($error = curl_error($curl))
        {
            throw new \Exception('CURL Error: ' . $error);
        }

        curl_close($curl);

        return $response;

    }

    /**
     *
     * @param string $path
     * @param string $file_name
     * @param string $new_file_name
     * @param string $width
     * @param string $height
     */
    public static function resizeImg($path, $file_name, $new_file_name, $width = false, $height = false) {

        $width = $width && is_numeric ( $width ) ? $width : Yii::$app->params ['mainImg'];

        $height = $height && is_numeric ( $height ) ? $height : Yii::$app->params ['mainImg'];

        $image = new SmartImage( $path . $file_name );

        $image->resize ( $width, $height);

        $image->saveImage ( $path . $new_file_name );
    }

    public static function builtBreadcrumb($level,$self,$option=[]){

        if($level === 1) {
            $self->params['breadcrumbs'][] = ['label' =>$self->title, 'template' => "<li><b>{link}</b></li>\n"];
        }elseif($level === 2){
            $self->params['breadcrumbs'][] = ['label' =>(isset($option['titleLevel1']))?$option['titleLevel1']:null, 'url' =>(isset($option['urlLevel1']))?$option['urlLevel1']:null];
            $self->params['breadcrumbs'][] = ['label' =>(isset($option['objectName']) && !is_null($option['objectName']))?$option['objectName']:$self->title,'template' => "<li><i class='fa fa-angle-right'></i><b>{link}</b></li>\n"];
        }elseif($level ===3 ){
            $self->params['breadcrumbs'][] = ['label' =>(isset($option['titleLevel1']))?$option['titleLevel1']:null, 'url' => (isset($option['urlLevel1']))?$option['urlLevel1']:null];
            $self->params['breadcrumbs'][] = ['label' =>(isset($option['objectName']) && !is_null($option['objectName']))?$option['objectName']:null, 'url' =>(isset($option['urlLevel2']))?$option['urlLevel2']:null,'template' => "<li><i class='fa fa-angle-right'></i>{link}</li>\n"];
            $self->params['breadcrumbs'][] = ['label' =>(isset($option['titleLevel2']))?$option['titleLevel2']:null,'template' => "<li><i class='fa fa-angle-right'></i><b>{link}</b></li>\n"];
        }

    }

    /**
     * @param null string $role : Role name to check
     * @param null AuthItem Entity Model Object $model : Permission model to check with role
     * @return bool
     */
    public static function checkMatrixPermission($role=null,$model=null){
        $access = FALSE;
        if(is_null($role))
            return $access;
        if(is_null($model))
            return $access;

        $permissions = Yii::$app->authManager->getPermissionsByRole($role);
        if(is_array($permissions) && !empty($permissions)){
            foreach($permissions as $_permission){
                $permissionName = strtolower($_permission->name);
                $modelName = strtolower($model->name);
                if($_permission->name === $model->name) {
                    $access = TRUE;
                }
                else{
                    if(\preg_match("/^.$permissionName.[a-zA-Z.]*$/",$modelName) !== 0){
                        $access = TRUE;
                    }
                }
            }
        }
        return $access;
    }

    /**
     * @param $model
     * @return array|null
     */
    public static function generateDataPermissionBelongMoudle($model){
        $option = null;
        $permissionName = strtolower($model->name);
        $modules = \Yii::$app->modules;
        if(is_array($modules) && !empty($modules)){
            foreach($modules as $key=>$_module){
                $moduleName = strtolower($key);
                if($moduleName === $permissionName){
                    return ['data-key'=>'m-'.$moduleName,'id'=>$moduleName];
                }elseif(\preg_match("/^".$moduleName."[-a-zA-Z.]*$/",$permissionName) !== 0){
                    return ['data-key'=>'c-'.$moduleName,'id'=>null];
                }
            }
        }
        return null;
    }
    /**
     * @param null $phone: Phone number is revert
     * @param null $regionCode: Region code of phone number
     * @return mixed|null
     */
    public static function revertPhoneNumber($phone=null,$regionCode=null){
        if(is_null($phone))
            return null;
        if(is_null($regionCode))
            return null;

        return str_replace($regionCode,'',$phone);
    }
    /**
     * @param $roleId
     * @return string
     */
    public static function getRoleName($roleId){
        $role = \backend\modules\authorization\models\entity\AuthItem::findOne(['role_id'=>$roleId]);
        return ($role)?$role->name:"(None)";
    }
    /**
     * @param $modelClass is Entity model
     * @param array $options
     * @return null|string
     */
    public static function summaryChangedAttributes($modelClass,$options=[],$outSideAttribute=[]){
        if(!$modelClass)
            return null;

        $attributes = $modelClass->getChangedAttributes();

        if(empty($attributes))
            return null;

        $header = isset($options['header']) ? $options['header'] : '<p>' . Yii::t('yii', 'Successful updated attributes:') . '</p>';
        $footer = isset($options['footer']) ? $options['footer'] : '';
        $encode = !isset($options['encode']) || $options['encode'] !== false;
        unset($options['header'], $options['footer'], $options['encode']);

        $lines = [];
        if (!is_array($attributes)) {
            $attributes = [$attributes];
        }
       // $attributes = ['avatar'];

        foreach ($attributes as $attribute=>$value) {
            $lines[] = $encode ? Html::encode($modelClass->getAttributeLabel($attribute)) : $modelClass->getAttributeLabel($attribute);
        }

        if(is_array($outSideAttribute) && !empty($outSideAttribute)){
            foreach($outSideAttribute as $attribute=>$value){
                $lines[] = $encode ? Html::encode($attribute) :$attribute;
            }
        }
        if (empty($lines)) {
            $content = "<ul></ul>";
            $options['style'] = isset($options['style']) ? rtrim($options['style'], ';') . '; display:none' : 'display:none';
        } else {
            $content = "<ul><li>" . implode("</li>\n<li>", $lines) . "</li></ul>";
        }
        return Html::tag('div', $header . $content . $footer, $options);
    }

    /**
     * @return array
     */
    public static function getConditionDropListRole(){
        $notInRole = $notInRole=[FitRoadUser::FIX_ROAD_USER_ROOT,FitRoadUser::FIX_ROAD_ADMIN];
        if(!\Yii::$app->user){
            return $notInRole;
        }
        (\Yii::$app->user->can('authorization'))?$notInRole=[FitRoadUser::FIX_ROAD_USER_ROOT]:null;

        return $notInRole;
    }

    /**
     * @param $finder
     * @return string
     */
    public static function builtPathImages($finder){
        return Yii::$app->basePath.Yii::$app->params['pathImages'.$finder];
    }
	
	static function resMulti($name='restaurant_id', $values=array()){		
		$model = UtilHelper::listRestaurants();
		$output = '<select multiple="multiple" class="multi-select" id="restaurant_id" name="restaurant[]" >';
		if(!empty($default)) {
			$output .= '<option value=""></option>';
		}
		if($model){			
			foreach($model AS $item){				
				$selected = "";
				foreach($values as $value){
					if($item['id'] == $value){
						$selected = " selected ";
						break;
					}
				}
				$output .= '<option class="popovers" data-original-title="'.$item['name'].'" data-container="body" data-placement="top" data-trigger="hover" data-content="'.$item['address'].'" value="'.$item['id'].'" '.$selected.'>'.$item['name'].'</option>';
			}
		}						                 					                 
	    $output .= '</select>';	
	    return $output;
	}

    public static function buildClassForCalories($calories){
            switch($calories){
                case $calories > 100:
                    return 'badge-success';
                break;
                case $calories > 300:
                    return 'badge-warning';
                break;
                case $calories > 500:
                    return 'bade-danger';
                break;
                default:  return 'bade-info';

            }
    }

	public function listRestaurants(){
		$connection = \Yii::$app->db;
		$query ="SELECT * from 	fit_road_restaurant where status =1 limit 0,1000";
		$user = $connection->createCommand($query);
		$model = $user->queryAll();
		return $model;
	}

    /**
     * @param $finder
     * @param $fileName
     * @return string
     */
    public  static function builtUrlImages($finder,$fileName,$sizeName = null){

        if(!$fileName){
            return null;
        }

        if($sizeName === null){

            $baseUrl = Yii::$app->params['urlAssetsImagesUploaded'].$finder;

        }
        elseif($sizeName == Yii::$app->params['thumbName']) {

            $baseUrl = Yii::$app->params['urlAssetsImagesUploaded'] . $finder . Yii::$app->params['thumbName'];
        }
        elseif($sizeName == Yii::$app->params['normalName']){

            $baseUrl = Yii::$app->params['urlAssetsImagesUploaded']
                .$finder.Yii::$app->params['normalName'];

        }

        return (string) $baseUrl.$fileName;
    }
    /**
     * trim up to 30 characters
     * @param string $str the string to shorten
     * @param int $length (optional) the max string length to return
     * @return string the shortened string
     */

    public static function shorten($text, $maxchars = 0, $def = null) {

        $newtext = self::safeStrip($text);
        $strlen = strlen($newtext);

        if ($maxchars > 0 && $maxchars < $strlen) {
            $text = substr($newtext, 0, $maxchars);

            $text1 = substr($text, 0, strripos($text, ' ', 0));
            if (strlen($text1) == 0) {
                $text1 = $text;
            }
            if ($def == 'none_ext') {
                $text = $text1;
            } else {
                $text = $text1 . "...";
            }

        } else {
            $text = self::safeStrip($text);
        }

        return $text;
    }

    /**
     * Strips tags without removing possible white space between words.
     *
     * @param string String to strip tags from.
     */
    function safeStrip($text) {

        $text = preg_replace('/</', ' <', $text);
        $text = preg_replace('/>/', '> ', $text);
        $desc = strip_tags($text);
        $desc = preg_replace('/  /', ' ', $desc);

        return $desc;
    }
}
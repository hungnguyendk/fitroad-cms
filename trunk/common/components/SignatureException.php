<?php
/**
 * User: sangnguyen on  3/18/16 at 20:36
 * File name: SignatureException.php
 * Project name: ysd-tee-shirt
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace common\components;

use Yii;
use yii\base\Exception;
use yii\web\BadRequestHttpException;

class SignatureException extends Exception{
    CONST DURATION_DF = 60;

    protected $signature;
    protected $timestamp;
    protected $queryParams=[];

    public function __construct(){
        $this->queryParams = [
            "API_ID" => Yii::$app->params['API_ID'],
            "APP_SECRET" => Yii::$app->params['APP_SECRET'],
            "store_url" => urlencode(Yii::$app->getUrlManager()->getHostInfo()),
            "token" => Yii::$app->request->csrfToken,
            "timestamp"=> DateHelper::local_to_gmt()
        ];
    }


    public function validSignature($signature,$timestamp,$csrf=null){
        $this->signature = $signature;
        $this->timestamp = $timestamp;

        $this->queryParams['token'] =   $csrf;

        $this->queryParams['timestamp'] =   $timestamp;

        foreach($this->queryParams as $key=>$value){
            $queryParams[urldecode($key)] = urldecode($value);
        }

        $this->expiryChecking();

        $this->verifyChecking();

        return TRUE;

    }
    private function verifyChecking(){
        if($this->generateSignature($this->queryParams) === $this->signature){
            return true;
        }else{
            Yii::warning("Bad request.");

            throw new BadRequestHttpException();
        }
    }

    private function expiryChecking($duration = self::DURATION_DF){

         if(DateHelper::local_to_gmt() - (int)$this->timestamp < $duration){
             return TRUE;
         }else{
             Yii::warning("Bad request.");
             throw new BadRequestHttpException();
         }
    }

    public function generateSignature($queryParams=null,$getCsrf=true){

        if($getCsrf === FALSE){
            unset($this->queryParams['token']);
        }

        if($queryParams !== null){
            $this->queryParams = $queryParams;
        }

        $params = array();
        foreach($this->queryParams as $key=>$value){
            $params[] = "$key=$value";
        }

        sort($params);
        $sortedParams = implode($params);
       // die(var_dump($sortedParams));
        return $this->signature = sha1($this->queryParams['APP_SECRET'] . $sortedParams);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: MyWorld
 * Date: 8/27/2015
 * Time: 12:16 AM
 */

namespace common\components;
use Yii;

class DateHelper
{
    public static function convert_timestamp_to_date($timestamp = null, $format_string = 'Y-m-d H:i:s') {
        if($timestamp) {
            return date($format_string, $timestamp);
        }
        return false;
    }

    public static function now($time_reference = 'gmt', $format_date = false, $format_string = 'Y-m-d H:i:s')
    {

        if (strtolower($time_reference) == 'gmt')
        {
            $now = time();
            $system_time = mktime(gmdate("H", $now), gmdate("i", $now), gmdate("s", $now), gmdate("m", $now), gmdate("d", $now), gmdate("Y", $now));

            if (strlen($system_time) < 10)
            {
                $system_time = time();
                Yii::error('The Date class could not set a proper GMT timestamp so the local time() value was used.');
            }

            if($format_date) {
                return gmdate($format_string, $system_time);
            } else {
                return $system_time;
            }

        }
        else
        {
            if($format_date) {
                $time = time();
                return date($format_string, $time);
            } else {
                return time();
            }
        }
    }

    public static function mdate($datestr = '', $time = '')
    {
        if ($datestr == '')
            return '';

        if ($time == '')
            $time = DateHelper::now();

        $datestr = str_replace('%\\', '', preg_replace("/([a-z]+?){1}/i", "\\\\\\1", $datestr));
        return date($datestr, $time);
    }


    public static function standard_date($fmt = 'DATE_RFC822', $time = '')
    {
        $formats = array(
            'DATE_ATOM'		=>	'%Y-%m-%dT%H:%i:%s%Q',
            'DATE_COOKIE'	=>	'%l, %d-%M-%y %H:%i:%s UTC',
            'DATE_ISO8601'	=>	'%Y-%m-%dT%H:%i:%s%Q',
            'DATE_RFC822'	=>	'%D, %d %M %y %H:%i:%s %O',
            'DATE_RFC850'	=>	'%l, %d-%M-%y %H:%i:%s UTC',
            'DATE_RFC1036'	=>	'%D, %d %M %y %H:%i:%s %O',
            'DATE_RFC1123'	=>	'%D, %d %M %Y %H:%i:%s %O',
            'DATE_RSS'		=>	'%D, %d %M %Y %H:%i:%s %O',
            'DATE_W3C'		=>	'%Y-%m-%dT%H:%i:%s%Q'
        );

        if ( ! isset($formats[$fmt]))
        {
            return FALSE;
        }

        return DateHelper::mdate($formats[$fmt], $time);
    }

    public static function days_in_month($month = 0, $year = '')
    {
        if ($month < 1 OR $month > 12)
        {
            return 0;
        }

        if ( ! is_numeric($year) OR strlen($year) != 4)
        {
            $year = date('Y');
        }

        if ($month == 2)
        {
            if ($year % 400 == 0 OR ($year % 4 == 0 AND $year % 100 != 0))
            {
                return 29;
            }
        }

        $days_in_month	= array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        return $days_in_month[$month - 1];
    }

    public static function local_to_gmt($time = '')
    {
        date_default_timezone_set('UTC');
        if ($time == '')
            $time = time();

        /*var_dump(gmdate("H", $time));
        var_dump(gmdate("i", $time));
        var_dump(gmdate("s", $time));
        var_dump(gmdate("m", $time));
        var_dump(gmdate("d", $time));
        die(var_dump(gmdate("Y", $time)));*/
        return mktime( gmdate("H", $time), gmdate("i", $time), gmdate("s", $time), gmdate("m", $time), gmdate("d", $time), gmdate("Y", $time));
    }

    public static function gmt_to_local($time = '', $timezone = 'UTC', $dst = FALSE)
    {
        if ($time == '')
        {
            return DateHelper::now();
        }

        $timezone = DateHelper::timezones($timezone);
        $time += $timezone * 3600;

        if ($dst == TRUE)
        {
            $time += 3600;
        }

        return $time;
    }


    /**
     * Converts a MySQL Timestamp to Unix
     *
     * @access	public
     * @param	integer Unix timestamp
     * @return	integer
     */
    public static function mysql_to_unix($time = '')
    {
        // We'll remove certain characters for backward compatibility
        // since the formatting changed with MySQL 4.1
        // YYYY-MM-DD HH:MM:SS

        $time = str_replace('-', '', $time);
        $time = str_replace(':', '', $time);
        $time = str_replace(' ', '', $time);

        // YYYYMMDDHHMMSS
        return  mktime(
            substr($time, 8, 2),
            substr($time, 10, 2),
            substr($time, 12, 2),
            substr($time, 4, 2),
            substr($time, 6, 2),
            substr($time, 0, 4)
        );
    }


    /**
     * Unix to "Human"
     *
     * Formats Unix timestamp to the following prototype: 2006-08-21 11:35 PM
     *
     * @access	public
     * @param	integer Unix timestamp
     * @param	bool	whether to show seconds
     * @param	string	format: us or euro
     * @return	string
     */
    public static function unix_to_human($time = '', $seconds = FALSE, $fmt = 'us')
    {
        $r  = date('Y', $time).'-'.date('m', $time).'-'.date('d', $time).' ';

        if ($fmt == 'us')
        {
            $r .= date('h', $time).':'.date('i', $time);
        }
        else
        {
            $r .= date('H', $time).':'.date('i', $time);
        }

        if ($seconds)
        {
            $r .= ':'.date('s', $time);
        }

        if ($fmt == 'us')
        {
            $r .= ' '.date('A', $time);
        }

        return $r;
    }


    /**
     * Convert "human" date to GMT
     *
     * Reverses the above process
     *
     * @access	public
     * @param	string	format: us or euro
     * @return	integer
     */
    public static function human_to_unix($datestr = '')
    {
        if ($datestr == '')
        {
            return FALSE;
        }

        $datestr = trim($datestr);
        $datestr = preg_replace("/\040+/", ' ', $datestr);

        if ( ! preg_match('/^[0-9]{2,4}\-[0-9]{1,2}\-[0-9]{1,2}\s[0-9]{1,2}:[0-9]{1,2}(?::[0-9]{1,2})?(?:\s[AP]M)?$/i', $datestr))
        {
            return FALSE;
        }

        $split = explode(' ', $datestr);

        $ex = explode("-", $split['0']);

        $year  = (strlen($ex['0']) == 2) ? '20'.$ex['0'] : $ex['0'];
        $month = (strlen($ex['1']) == 1) ? '0'.$ex['1']  : $ex['1'];
        $day   = (strlen($ex['2']) == 1) ? '0'.$ex['2']  : $ex['2'];

        $ex = explode(":", $split['1']);

        $hour = (strlen($ex['0']) == 1) ? '0'.$ex['0'] : $ex['0'];
        $min  = (strlen($ex['1']) == 1) ? '0'.$ex['1'] : $ex['1'];

        if (isset($ex['2']) && preg_match('/[0-9]{1,2}/', $ex['2']))
        {
            $sec  = (strlen($ex['2']) == 1) ? '0'.$ex['2'] : $ex['2'];
        }
        else
        {
            // Unless specified, seconds get set to zero.
            $sec = '00';
        }

        if (isset($split['2']))
        {
            $ampm = strtolower($split['2']);

            if (substr($ampm, 0, 1) == 'p' AND $hour < 12)
                $hour = $hour + 12;

            if (substr($ampm, 0, 1) == 'a' AND $hour == 12)
                $hour =  '00';

            if (strlen($hour) == 1)
                $hour = '0'.$hour;
        }

        return mktime($hour, $min, $sec, $month, $day, $year);
    }


    public static function countdown_date($date,$now = false,$select = false)
    {
        $output_hour = '';
        $output_day = '';
        $output_minute = '';

        $CI 	=& get_instance();
        $now = ! empty ( $now ) ? $now : $CI->db->query ( "SELECT UNIX_TIMESTAMP() as time " )->row()->time;

        $hour = date('H', strtotime($date));
        $minute= date('i', strtotime($date));
        $month= date('m', strtotime($date));
        $day = date('d', strtotime($date));
        $year = date('Y', strtotime($date));

        ($hour == 0)?$hour= 23:$hour;
        ($minute == 0)?$minute = 59:$minute;

        $the_countdown_date = mktime($hour, $minute, 0, $month, $day, $year, -1);


        $today = strtotime($now);

        $difference = $the_countdown_date - $today;
        if ($difference < 0) $difference = 0;

        $days_left = floor($difference/60/60/24);
        $hours_left = floor(($difference - $days_left*60*60*24)/60/60);
        $minutes_left = floor(($difference - $days_left*60*60*24 - $hours_left*60*60)/60);

        $output_hour = ($hours_left > 1)?$hours_left.lang('hours').' ':$hours_left.lang('hour');
        $output_day = ($days_left > 1)?$days_left.lang('days').' - ':$days_left.lang('day').' - ';
        $output_minute =  ($minutes_left > 1)?'- '.$minutes_left.lang('minutes'):'- '.$minutes_left.lang('minute').' ';

        $output_hour = ($hours_left == 0)?'':$output_hour;
        $output_day = ($days_left == 0)?'':$output_day;
        $output_minute = ($minutes_left == 0)?'':$output_minute;

        if(!empty($select))
        {
            switch ($select)
            {
                case 'day':
                    return $days_left;
                    break;
                case 'hour':
                    return $hours_left;
                    break;
                case 'minute':
                    return $minutes_left;
                    break;
            }
        }else
            return $output_day.$output_hour.$output_minute;
    }

    public static function checkDate($timestamp, $format  = false) {
        //$timestamp = "2014.09.02T13:34";

        $today = new \DateTime(); // This object represents current date/time
        $today->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

        if($format) {
            $match_date = \DateTime::createFromFormat( $format, $timestamp );
        } else {
            $match_date = \DateTime::createFromFormat( "Y-m-d", $timestamp );
        }

        if($match_date) {
            $match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

            $diff = $today->diff( $match_date );
            $diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval

            return $diffDays;
        }

        return FALSE;
    }

    public static function isToday($date, $format = "Y-m-d") //check if it's today
    {
        $check = DateHelper::checkDate($date, $format);

        if($check === FALSE && $check == 0) {
            return true;
        }

        return false;
    }

    public static function isYesterday($date, $format = "Y-m-d") //check if it's yesterday
    {
        $check = DateHelper::checkDate($date, $format);

        if($check === FALSE && $check == -1) {
            return true;
        }

        return false;
    }

    public static function isPastDay($date, $format = "Y-m-d") //check if it's yesterday
    {
        $check = DateHelper::checkDate($date, $format);//die(var_dump( $check ));

        if($check === FALSE || $check < 0) {
            return true;
        }

        return false;
    }

    public static function convert_time_to_second($str_time) {
        $hours = 0;
        $minutes = 0;
        $seconds = 0;

        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

        $time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes * 60;

        return $time_seconds;
    }

    public static function relative_time($date, $postfix = ' ago', $fallback = 'F Y')
    {
        $diff = time() - strtotime($date);
        if ($diff < 60) {
            return $diff . ' second'. ($diff != 1 ? 's' : '') . $postfix;
        }
        $diff = round($diff/60);
        if ($diff < 60) {
            return $diff . ' minute'. ($diff != 1 ? 's' : '') . $postfix;
        }
        $diff = round($diff/60);
        if ($diff < 24) {
            return $diff . ' hour'. ($diff != 1 ? 's' : '') . $postfix;
        }
        $diff = round($diff/24);
        if ($diff < 7) {
            return $diff . ' day'. ($diff != 1 ? 's' : '') . $postfix;
        }
        $diff = round($diff/7);
        if ($diff < 4) {
            return $diff . ' week'. ($diff != 1 ? 's' : '') . $postfix;
        }
        $diff = round($diff/4);
        if ($diff < 12) {
            return $diff . ' month'. ($diff != 1 ? 's' : '') . $postfix;
        }
        return date($fallback, strtotime($date));
    }


    /**
     * Timezones
     *
     * Returns an array of timezones.  This is a helper function
     * for various other ones in this library
     *
     * @access	public
     * @param	string	timezone
     * @return	string
     */
    public static function timezones($tz = '')
    {
        // Note: Don't change the order of these even though
        // some items appear to be in the wrong order

        $zones = array(
            'UM12'		=> -12,
            'UM11'		=> -11,
            'UM10'		=> -10,
            'UM95'		=> -9.5,
            'UM9'		=> -9,
            'UM8'		=> -8,
            'UM7'		=> -7,
            'UM6'		=> -6,
            'UM5'		=> -5,
            'UM45'		=> -4.5,
            'UM4'		=> -4,
            'UM35'		=> -3.5,
            'UM3'		=> -3,
            'UM2'		=> -2,
            'UM1'		=> -1,
            'UTC'		=> 0,
            'UP1'		=> +1,
            'UP2'		=> +2,
            'UP3'		=> +3,
            'UP35'		=> +3.5,
            'UP4'		=> +4,
            'UP45'		=> +4.5,
            'UP5'		=> +5,
            'UP55'		=> +5.5,
            'UP575'		=> +5.75,
            'UP6'		=> +6,
            'UP65'		=> +6.5,
            'UP7'		=> +7,
            'UP8'		=> +8,
            'UP875'		=> +8.75,
            'UP9'		=> +9,
            'UP95'		=> +9.5,
            'UP10'		=> +10,
            'UP105'		=> +10.5,
            'UP11'		=> +11,
            'UP115'		=> +11.5,
            'UP12'		=> +12,
            'UP1275'	=> +12.75,
            'UP13'		=> +13,
            'UP14'		=> +14
        );

        if ($tz == '')
        {
            return $zones;
        }

        if ($tz == 'GMT')
            $tz = 'UTC';

        return ( ! isset($zones[$tz])) ? 0 : $zones[$tz];
    }

    public static function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public static function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {

        $step = !empty($step) ? $step : '+1 day';

        $dates = array();
        $current = strtotime( $first );
        $last = strtotime( $last );

        while( $current <= $last ) {

            $dates[] = date( $format, $current );
            $current = strtotime( $step, $current );
        }

        return $dates;
    }
}
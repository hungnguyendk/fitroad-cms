<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api'); // add api alias
Yii::setAlias('@publicImagesAvatars', dirname(dirname(__DIR__)) . '/public/upload/images/avatars/');
Yii::setAlias('@publicImagesFoods', dirname(dirname(__DIR__)) . '/public/upload/images/foods/');
Yii::setAlias('@publicImagesActivities', dirname(dirname(__DIR__)) . '/public/upload/images/activities/');
Yii::setAlias('@publicImagesRestaurants', dirname(dirname(__DIR__)) . '/public/upload/images/restaurants/');

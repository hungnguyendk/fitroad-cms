<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,


    'sizeThumbImg'			               =>	200,
    'sizeNormalImg'		                   =>  400,
    'thumbName'                  => 'thumb-',
    'normalName'                 => 'normal-',
    'avatarsFinder' =>'avatars/',
    'activitiesFinder' =>'activities/',
    'foodsFinder' =>'foods/',
    'restaurantsFinder'=>'restaurants/',
];

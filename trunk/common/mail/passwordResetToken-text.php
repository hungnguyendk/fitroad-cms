<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = $linkRestPassword;
?>
Hello <?= trim($user->first_name.' '.$user->last_name)?>,

Follow the link below to reset your password:

<?= $resetLink ?>
